package model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import utils.ObjectReflect;
import utils.Routines;

/**
 * Created by okarpov on 1/30/2017.
 */
public class HighScoreRow extends ObjectReflect{

    static String TAG = "HighScoreRow";

    public HighScoreRow()
    {
        _User = "";
        _Score = 0;
    }

    public HighScoreRow(String user, int score)
    {
        _User = user;
        _Score = score;
    }

    public String getUser()
    {
        return _User;
    }

    public int getScore()
    {
        return _Score;
    }

    private String _User;
    private int _Score;

    public long SaveToDB(SQLiteDatabase db)
    {
        print(TAG);

        try {
            ContentValues values = new ContentValues();
            values.put("user", _User);
            values.put("score", _Score);

            return db.insert("HighScoreTable", null, values);
        }
        catch (Exception e)
        {
            Log.i(TAG, "e:" + e);
        }

        return -1;
    }

    public void ReadFromDB(Cursor cursor)
    {
        _User = cursor.getString(cursor.getColumnIndexOrThrow("user"));
        _Score = cursor.getInt(cursor.getColumnIndexOrThrow("score"));

        print(TAG);
    }

    public int updateScore(SQLiteDatabase db)
    {
        print(TAG);

        try {

            ContentValues values = new ContentValues();
            values.put("score", _Score);

            String selection = "user LIKE ?";
            String[] selectionArgs = {"" + _User};

            return db.update(
                    "HighScoreTable",
                    values,
                    selection,
                    selectionArgs);
        }
        catch (Exception e)
        {
            Log.i(TAG, "e:" + e);
        }

        return -1;
    }

    public void DeleteScore(final SQLiteDatabase db)
    {
        print(TAG);

        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                String selection = "_User LIKE ?";
                String[] selectionArgs = { "" + _User };
                db.delete("HighScoreTable", selection, selectionArgs);
            }
        });
    }
}
