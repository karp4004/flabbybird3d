package model;

import android.util.Log;
import utils.Routines;

/**
 * Created by okarpov on 12/15/2016.
 */
public class ModelResorces {

    static String TAG = "ModelResorces";

    public enum eResourcesState
    {
        eNotLoaded,
        eLoading,
        eLoaded
    };

    eResourcesState mResourcesLoaded = eResourcesState.eNotLoaded;

    public interface ResourcesEvent
    {
        void onEvent();
    }

    ResourcesEvent mResourcesNotLoadedEvent;
    ResourcesEvent mResourcesLoadingEvent;
    ResourcesEvent mResourcesLoadedEvent;

    public void resourcesLoaded()
    {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                mResourcesLoaded = eResourcesState.eLoaded;

                mResourcesLoadedEvent.onEvent();
            }
        });
    }

    public void resourcesLoading()
    {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                mResourcesLoaded = eResourcesState.eLoading;

                mResourcesLoadingEvent.onEvent();
            }
        });
    }

    public void loadResources(final ResourcesEvent notloaded, final ResourcesEvent loading, final ResourcesEvent loaded)
    {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {

                mResourcesNotLoadedEvent = notloaded;
                mResourcesLoadingEvent = loading;
                mResourcesLoadedEvent = loaded;

                Log.i(TAG, "loadResources:" + mResourcesLoaded);

                if(mResourcesLoaded == eResourcesState.eNotLoaded)
                {
                    mResourcesNotLoadedEvent.onEvent();
                }
                else if(mResourcesLoaded == eResourcesState.eLoading)
                {
                    mResourcesLoadingEvent.onEvent();
                }
                else if(mResourcesLoaded == eResourcesState.eLoaded)
                {
                    mResourcesLoadedEvent.onEvent();
                }
            }
        });
    }
}
