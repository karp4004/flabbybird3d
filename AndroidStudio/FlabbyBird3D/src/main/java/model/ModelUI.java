package model;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import utils.Routines;

/**
 * Created by okarpov on 7/29/2016.
 */
public class ModelUI {

    public enum eViewState {

        eSplash,
        eMain,
        eMenu,
        eExit,
        eCount,
        DS_ERROR;
    }

    public class ModelUIState
    {
        public ModelUIState()
        {
           mViewStack.add(eViewState.eExit);
           mViewStack.add(eViewState.eSplash);
        }

        public ArrayList<eViewState> mViewStack = new ArrayList<>();
        public ModelUI.eViewState mCurrent = eViewState.eSplash;
    }

    String TAG = "ModelUI";

    interface IterateCB
    {
        void onIterateInterface(ViewInterface i);
    }

    public interface ViewInterface
    {
        void onViewStateChanged(eViewState s);
    }

    boolean inited = false;

    HashMap<String, ViewInterface> mInterfaces = new HashMap<>();

    static ModelUIState mModelUIState;

    public ModelUI.eViewState getCurrent()
    {
        return mModelUIState.mCurrent;
    }

    public ModelUI()
    {
        if(mModelUIState == null)
        {
            mModelUIState = new ModelUIState();
        }
    }

    public void addInterface(final String name, final ViewInterface i)
    {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                mInterfaces.put(name, i);
            }
        });
    }

    public void resetViewStack(eViewState st)
    {
        mModelUIState.mViewStack.clear();
        mModelUIState.mViewStack.add(eViewState.eExit);
        mModelUIState.mViewStack.add(st);
        mModelUIState.mCurrent = st;
    }

    void interateInterfaces(final IterateCB cb)
    {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                for (ViewInterface i:
                        mInterfaces.values()) {
                    if(cb != null)
                    {
                        cb.onIterateInterface(i);
                    }
                }
            }
        });
    }

    public void updateUI()
    {
        if(mModelUIState.mCurrent == eViewState.eExit)
        {
            resetViewStack(eViewState.eSplash);
        }

        interateInterfaces(new IterateCB() {
            @Override
            public void onIterateInterface(ViewInterface i) {
                i.onViewStateChanged(mModelUIState.mCurrent);
            }
        });
    }

    public void setViewState(final eViewState s)
    {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                printViewStack();

                eViewState st = null;
                if (mModelUIState.mViewStack.size() > 0) {
                    st = mModelUIState.mViewStack.get(mModelUIState.mViewStack.size() - 1);
                }

                if (st != s && s != eViewState.eSplash) {
                    mModelUIState.mViewStack.add(s);
                }

                mModelUIState.mCurrent = s;

                interateInterfaces(new IterateCB() {
                    @Override
                    public void onIterateInterface(ViewInterface i) {
                        i.onViewStateChanged(mModelUIState.mCurrent);
                    }
                });

                printViewStack();
            }
        });
    }

    void printViewStack()
    {
        Log.i(TAG, "stack:" + mModelUIState.mViewStack.size());

        for (eViewState st:
             mModelUIState.mViewStack) {
            Log.i(TAG, "st:" + st);
        }
    }

    public void prevViewState()
    {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                printViewStack();

                eViewState st = null;

                if(mModelUIState.mViewStack.size() > 0) {

                    if(mModelUIState.mViewStack.size() > 1) {
                        st = mModelUIState.mViewStack.get(mModelUIState.mViewStack.size() - 2);
                    }

                    mModelUIState.mViewStack.remove(mModelUIState.mViewStack.size() - 1);

                    Log.e(TAG, "mViewStack.size():" + mModelUIState.mViewStack.size());

                    if(mModelUIState.mViewStack.size() > 0) {
                        st = mModelUIState.mViewStack.get(mModelUIState.mViewStack.size() - 1);
                    }

                    Log.e(TAG, "st:" + st);

                    if(st == eViewState.eSplash)
                    {
                        st = eViewState.eExit;
                    }

                    mModelUIState.mCurrent = st;

                    interateInterfaces(new IterateCB() {
                        @Override
                        public void onIterateInterface(ViewInterface i) {
                            i.onViewStateChanged(mModelUIState.mCurrent);
                        }
                    });
                }
            }
        });
    }
}
