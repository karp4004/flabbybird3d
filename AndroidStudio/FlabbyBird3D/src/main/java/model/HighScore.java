package model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import utils.Routines;

/**
 * Created by okarpov on 1/19/2017.
 */
public class HighScore extends SQLiteOpenHelper {

    static String TAG = "HighScore";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE HighScoreTable (user TEXT" +
                    ", score INT);";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS HighScoreTable;";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "HighScore.db";

    public HighScore(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void exportDatabse(String path)
    {
        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String backupDBPath = sd.getPath() + "//Download//HighScore.db";
                File currentDB = new File(path);
                File backupDB = new File(backupDBPath);

                Log.i(TAG, "currentDB:" + currentDB + " " + currentDB.exists());
                Log.i(TAG, "backupDB:" + backupDB + " " + backupDB.exists());

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();

                    Log.i(TAG, "src:" + src);
                    Log.i(TAG, "dst:" + dst);

                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            Log.i(TAG, "e:" + e);
        }
    }

    public void onCreate(final SQLiteDatabase db) {

        Log.i(TAG, "onCreate:" + DATABASE_NAME);

        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {

                Log.i(TAG, "onCreate:" + SQL_CREATE_ENTRIES);

                db.execSQL(SQL_CREATE_ENTRIES);
            }
        });
    }

    public void onUpgrade(final SQLiteDatabase db, int oldVersion, int newVersion) {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {

                Log.i(TAG, "onUpgrade:" + SQL_DELETE_ENTRIES);

                db.execSQL(SQL_DELETE_ENTRIES);
                onCreate(db);
            }
        });
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.i(TAG, "onDowngrade:" + SQL_DELETE_ENTRIES);

        onUpgrade(db, oldVersion, newVersion);
    }


    public void deleteScore(final HighScoreRow score)
    {
        score.print(TAG);

        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                SQLiteDatabase db = getWritableDatabase();
                score.DeleteScore(db);
            }
        });
    }

    public long saveScore(HighScoreRow score)
    {
        try {
            SQLiteDatabase db = getWritableDatabase();
            return score.SaveToDB(db);
        }
        catch (Exception e)
        {
            Log.i(TAG, "e:" + e);
        }

        return -1;
    }

    public ArrayList<HighScoreRow> readScores()
    {
        Log.i(TAG, "readScores");

        final ArrayList<HighScoreRow> itemIds = new ArrayList<>();

        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {
                SQLiteDatabase db = getReadableDatabase();

                exportDatabse(db.getPath());

                String[] projection = {
                        "user",
                        "score"
                };

                Cursor cursor = db.query(
                        "HighScoreTable",
                        projection,
                        null,
                        null,
                        null,
                        null,
                        "score DESC"
                );

                while(cursor.moveToNext()) {

                    HighScoreRow score = new HighScoreRow();
                    score.ReadFromDB(cursor);

                    Log.i(TAG, "score:" + score.getScore());

                    itemIds.add(score);
                }
                cursor.close();
            }
        });

        return itemIds;
    }

    public HighScoreRow readMaxScore()
    {
        HighScoreRow ret = new HighScoreRow();
        ArrayList<HighScoreRow> scores = readScores();
        for (HighScoreRow r:
             scores) {
            if(r.getScore() > ret.getScore())
            {
                ret = r;
            }
        }

        return ret;
    }

    public int updateScore(HighScoreRow score)
    {
        score.print(TAG);

        try {
            SQLiteDatabase db = getReadableDatabase();
            return score.updateScore(db);
        }
        catch (Exception e)
        {
            Log.i(TAG, "e:" + e);
        }

        return -1;
    }
}
