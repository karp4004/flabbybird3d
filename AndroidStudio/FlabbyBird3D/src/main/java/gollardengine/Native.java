package gollardengine;

import android.content.res.AssetManager;
import android.util.Log;

import utils.Routines;

public class Native {

	static String TAG = "Native";
	
	static {
        System.loadLibrary("gelibrary");
    }

	public interface ResourcesLoadedEvent
	{
		void onResourcesLoaded();
		void onResourcesLoadedException(Exception e);
	}

	public interface ICollisionBird
	{
		void onCollisionBird();
	}

	public interface ISceneCreated
	{
		void onSceneCreated();
	}

	private static boolean _SceneLoaded = false;

	static ResourcesLoadedEvent mResourcesLoadedEvent;
	public static boolean mResourcesLoaded = false;

	static ICollisionBird _CollisionBird;
	static ISceneCreated _SceneCreated;

	public static void setResourcesLoadedEvent(ResourcesLoadedEvent e)
	{
		mResourcesLoadedEvent = e;
	}
	public static void setCollisionBird(ICollisionBird e)
	{
		_CollisionBird = e;
	}
	public static void setSceneCreated(ISceneCreated e)
	{
		_SceneCreated = e;
	}

   /**
    * @param width the current view width
    * @param height the current view height
    */
    public static native int loadResources(AssetManager am);
    public static native int createScene();
    public static native int releaseScene();
    public static native int onSurfaceCreated();
    public static native int onSurfaceChanged(int width, int height);
    public static native int onDrawFrame();
	public static native void onTouchScreen();
	public static native float getScores();
	public static native void onDestroy();

	private static void ResourcesLoaded() {
		Log.i(TAG, "resourcesLoaded");
		
		mResourcesLoaded = true;
		
		if(mResourcesLoadedEvent != null)
		{
			try
			{
				mResourcesLoadedEvent.onResourcesLoaded();
			}
			catch(Exception e)
			{
				mResourcesLoadedEvent.onResourcesLoadedException(e);
			}
		}
	}

	private static void CollisionBird() {
		Log.i(TAG, "collisionBird");

		Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
			@Override
			public void onMethod() {
				_CollisionBird.onCollisionBird();
			}
		});
	}

	private static void SceneCreated() {
		Log.i(TAG, "collisionBird");

		Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
			@Override
			public void onMethod() {
				_SceneCreated.onSceneCreated();
			}
		});
	}
}
