package controller;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import fb3d.MainActivity;
import fb3d.test.R;

/**
 * Created by okarpov on 1/25/2017.
 */
public class ControllerView {

    static String TAG = "ControllerView";

    public ControllerView(MainActivity main, int rid)
    {
        Log.i(TAG, "rid:" + rid);

        final LayoutInflater inflater = (LayoutInflater) main.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final FrameLayout content = (FrameLayout) main.findViewById(R.id.content_frame);
        content.removeAllViews();
        final View v = inflater.inflate(main.getResources().getLayout(rid), null);
        content.addView(v);

        Log.i(TAG, "rid:" + rid);
    }
}
