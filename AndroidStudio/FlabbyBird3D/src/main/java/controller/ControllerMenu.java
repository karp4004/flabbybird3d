package controller;

import android.animation.Animator;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import fb3d.MainActivity;
import fb3d.test.R;
import gollardengine.Native;
import model.ModelResorces;
import model.ModelUI;
import utils.AsyncTask;
import utils.Routines;

/**
 * Created by admin on 30.01.2017.
 */

public class ControllerMenu extends ControllerView {

    public ControllerMenu(final MainActivity a, final ModelUI _modelUI) {
        super(a, R.layout.view_menu);

        final ViewGroup content = (ViewGroup)a.findViewById(R.id.content_frame);

        final Animator.AnimatorListener def = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };

        Routines.baseAnimation(content.findViewById(R.id.btnStart), "alpha", 1, 500, def);
        Routines.baseAnimation(content.findViewById(R.id.btnStart), "scaleX", 1, 500, def);
        Routines.baseAnimation(content.findViewById(R.id.btnStart), "scaleY", 1, 500, def);

        Routines.setBtnClick(content, R.id.btnStart, null, null, new Routines.ClickEvent() {
            @Override
            public void onClick() {
                Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
                    @Override
                    public void onMethod() {

                        new AsyncTask().executeTask(new AsyncTask.TaskEvent() {
                            @Override
                            public void onTask() {
                                Native.createScene();
                            }
                        });
                    }
                });
            }
        });

        Native.setSceneCreated(new Native.ISceneCreated() {
            @Override
            public void onSceneCreated() {

                Log.i(TAG, "onSceneCreated");

                a.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Routines.baseAnimation(content.findViewById(R.id.btnStart), "alpha", 0, 500, new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                _modelUI.setViewState(ModelUI.eViewState.eMain);
                            }

                            @Override
                            public void onAnimationCancel (Animator animation){

                            }

                            @Override
                            public void onAnimationRepeat (Animator animation){

                            }
                        });

                        Routines.baseAnimation(content.findViewById(R.id.btnStart), "scaleX", 3, 500, def);
                        Routines.baseAnimation(content.findViewById(R.id.btnStart), "scaleY", 3, 500, def);

                        //Routines.sequnceAnimation3(final View v, final String[] param, final float[] end, final int dur, final Animator.AnimatorListener list)
                    }
                });
            }
        });

        Native.releaseScene();
    }
}
