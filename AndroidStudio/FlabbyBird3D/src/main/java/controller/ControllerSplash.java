package controller;

import android.os.CountDownTimer;
import android.view.View;

import fb3d.MainActivity;
import fb3d.test.R;
import gollardengine.Native;
import model.ModelResorces;
import model.ModelUI;
import utils.AsyncTask;
import utils.Routines;

/**
 * Created by admin on 30.01.2017.
 */

public class ControllerSplash extends ControllerView {

    public void startLoadResources(final MainActivity a, final ModelResorces _modelResorces)
    {
        Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
            @Override
            public void onMethod() {

                new AsyncTask().executeTask(new AsyncTask.TaskEvent() {
                    @Override
                    public void onTask() {
                        Native.loadResources(a.getAssets());
                    }
                });

                _modelResorces.resourcesLoading();
            }
        });
    }

    public ControllerSplash(final MainActivity a, final ModelUI _modelUI, final ModelResorces _modelResorces) {
        super(a, R.layout.view_splash);

        new CountDownTimer(2000, 2000)
        {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Native.setResourcesLoadedEvent(new Native.ResourcesLoadedEvent(){
                    @Override
                    public void onResourcesLoaded() {
                        _modelResorces.resourcesLoaded();
                    }

                    @Override
                    public void onResourcesLoadedException(Exception e) {
                    }
                });

                _modelResorces.loadResources(
                    new ModelResorces.ResourcesEvent() {
                        @Override
                        public void onEvent() {
                            startLoadResources(a, _modelResorces);
                        }
                    },

                    null,
                    new ModelResorces.ResourcesEvent() {
                        @Override
                        public void onEvent() {
                            a.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    _modelUI.setViewState(ModelUI.eViewState.eMenu);
                                }
                            });
                        }
                    });
            }
        }.start();
    }
}
