package controller;

import android.animation.Animator;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.Random;

import model.HighScore;
import model.HighScoreRow;
import model.ModelResorces;
import model.ModelUI;
import views.GollardEngineView;
import gollardengine.Native;
import fb3d.MainActivity;
import fb3d.test.R;
import utils.Routines;

/**
 * Created by okarpov on 12/14/2016.
 */
public class ControllerMain extends ControllerView {

    static String TAG = "ControllerMain";

    static HighScore _HighScore;

    GollardEngineView mView;

    boolean _Finished = false;

    public ControllerMain(final MainActivity a, final ModelUI _modelUI)
    {
        super(a, R.layout.view_main);

        _HighScore = new HighScore(a);

        final ViewGroup content = (ViewGroup)a.findViewById(R.id.content_frame);

        Routines.setViewVisible(content, R.id.layFlash, View.GONE);

        mView = (GollardEngineView)Routines.addChildView(content, R.id.idSurface, new GollardEngineView(a));

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Native.onTouchScreen();
            }
        });

        Routines.setViewVisible(content, R.id.layResult, View.INVISIBLE);

        new CountDownTimer(1000, 1000) {
            @Override
            public void onFinish() {

                Routines.setTextView(content, R.id.txtScore, "Score: " + (int)Native.getScores());

                start();
            }

            @Override
            public void onTick(long millisUntilFinished) {

            }
        }.start();

        Native.setCollisionBird(new Native.ICollisionBird() {
            @Override
            public void onCollisionBird() {

                Log.i(TAG, "_Finished:" + _Finished);

                if(!_Finished) {
                    a.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Routines.setViewVisible(content, R.id.layFlash, View.VISIBLE);
                            Routines.baseAnimation(content.findViewById(R.id.layFlash), "alpha", 0, 200, new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });

                            String user = "user" + (int)(Math.random()*10000);
                            _HighScore.saveScore(new HighScoreRow(user, (int)Native.getScores()));

                            HighScoreRow maxscore = _HighScore.readMaxScore();

                            Routines.setViewVisible(content, R.id.layResult, View.VISIBLE);
                            Routines.setTextView(content, R.id.resultScore, "Score: " + "" + user + " - " + (int)Native.getScores());
                            Routines.setTextView(content, R.id.highScore, "HighScore: " + "" + maxscore.getUser() + " - " + maxscore.getScore());
                        }
                    });

                    _Finished = true;
                }
            }
        });

        Routines.setBtnClick(content, R.id.btnMenu, null, null, new Routines.ClickEvent() {
            @Override
            public void onClick() {
                _modelUI.resetViewStack(ModelUI.eViewState.eMenu);
                _modelUI.updateUI();
            }
        });
    }
}
