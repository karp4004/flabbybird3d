package controller;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import model.ModelResorces;
import model.ModelUI;
import fb3d.MainActivity;
import fb3d.test.R;

/**
 * Created by okarpov on 10/28/2016.
 */
public class ControllerUI {

    String TAG = "ControllerUI";

    boolean inited = false;

    public ControllerUI(final MainActivity main, final ModelUI modelUI, final ModelResorces modelResorces)
    {
        if(!inited) {

            modelUI.addInterface(TAG, new ModelUI.ViewInterface() {

                @Override
                public void onViewStateChanged(ModelUI.eViewState s) {

                    Log.i(TAG, "onViewStateChanged:" + s);

                    if (s == ModelUI.eViewState.eMain) {
                        new ControllerMain(main, modelUI);
                    }
                    else if (s == ModelUI.eViewState.eSplash) {
                        new ControllerSplash(main, modelUI, modelResorces);
                    }
                    else if (s == ModelUI.eViewState.eMenu) {
                        new ControllerMenu(main, modelUI);
                    }
                }
            });

            inited = true;
        }

        modelUI.updateUI();
    }
}
