package utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by okarpov on 11/7/2016.
 */
public class DateFromCalendar {
    public static Date getDate(int y, int m, int d)
    {
        Calendar start = Calendar.getInstance();
        start.set(y, m, d);

        return start.getTime();
    }
}
