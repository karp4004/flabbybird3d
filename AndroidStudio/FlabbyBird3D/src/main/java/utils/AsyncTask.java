package utils;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by okarpov on 11/8/2016.
 */
public class AsyncTask {

    static String TAG = "AsyncTask";

    public interface TaskEvent
    {
        void onTask();
    }

    protected Executor mTaskExecutor;

    public AsyncTask()
    {
        mTaskExecutor = Executors.newFixedThreadPool(10);
    }

    public void executeTask(final TaskEvent e)
    {
        mTaskExecutor.execute(new Runnable() {
            @Override
            public void run() {

                Routines.ExceptionMethod(TAG, new Routines.IExceptionMethod() {
                    @Override
                    public void onMethod() {
                        e.onTask();
                    }
                });
            }
        });
    }
}
