package utils;

import android.util.Log;

import java.lang.reflect.Field;

/**
 * Created by okarpov on 11/7/2016.
 */
public class ObjectReflect extends Object {
    public void print(String tag)
    {
        try {
            for (Field f :
                    getClass().getDeclaredFields()) {
                f.setAccessible(true);
                Log.i(tag, "" + f.getName() + ":" + f.get(this));
            }
        }
        catch (Exception e)
        {
            Log.i(tag, "e:" + e);
        }
    }

    public Class getSerialClass()
    {
        return ObjectReflect.class;
    }
}
