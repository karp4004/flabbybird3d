package utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.Calendar;
import java.util.Date;

import model.ModelUI;

/**
 * Created by karp4004 on 14.11.2016.
 */
public class Routines {

    static String TAG = "Routines";

    public interface IDatePeriod
    {
        void onDatePeriod(Date start, Date end);
    }

    public interface ICheckPhone
    {
        void onChecked(String tel);
        void onNotChecked();
    }

    public interface IExceptionMethod
    {
        void onMethod();
    }

    public interface DelayedEvent
    {
        void onDelayedEvent();
    }

    public interface ClickEvent
    {
        void onClick();
    }

    public interface CheckEvent
    {
        void onCheckEvent(boolean ch);
    }

    public interface TextChangedEvent
    {
        void onTextChanged(View v, CharSequence s, int i, int i1, int i2);
    }

    public static Bitmap BITMAP_RESIZER(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;

    }

    public static Bitmap createBitmapFromLayoutWithText(View view) {

        //Pre-measure the view so that height and width don't remain null.
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        //Assign a size and position to the view and all of its descendants
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        //Create the bitmap
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmap);

        //Render this view (and all of its children) to the given Canvas
        view.draw(c);
        return bitmap;
    }

    public static Bitmap loadBitmapFromView(View v, int w, int h) {
        Bitmap b = Bitmap.createBitmap( w, h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, w, h);
        v.draw(c);
        return b;
    }

    public static Bitmap generateIcon(Resources res, int id, int sz)
    {
        Bitmap ret = BitmapFactory.decodeResource(res, id);
        ret = BITMAP_RESIZER(ret, (int) res.getDimension(sz),
                (int) res.getDimension(sz));

        return ret;
    }

    public static void getDatePeriod(IDatePeriod i, int shift)
    {
        Calendar cal = Calendar.getInstance();

        Date end = cal.getTime();

        cal.add(Calendar.DAY_OF_MONTH, shift);

        Date start = cal.getTime();

        if(i != null)
        {
            i.onDatePeriod(start, end);
        }
    }

    public static int getJsonElementInt(String name, JsonObject o)
    {
        JsonElement e = o.get(name);
        if(e != null) {
            return e.getAsInt();
        }

        return 0;
    }

    public static float getJsonElementFloat(String name, JsonObject o)
    {
        JsonElement e = o.get(name);
        if(e != null) {
            return e.getAsFloat();
        }

        return 0;
    }

    public static void checkPhone(String c, String s, String t, ICheckPhone e)
    {
        if(e != null) {

            Log.i(TAG, "c:" + c + " s:" + s + " t:" + t);

            if (s.length() == 3 && t.length() == 7 && c.length() > 0) {

                e.onChecked(c + s + t);
            } else {
                e.onNotChecked();
            }
        }
    }

    public static void ExceptionMethod(String tag, IExceptionMethod m)
    {
        try {
            if (m != null)
            {
                m.onMethod();
            }
            else
            {
                throw new Exception(" no mothod");
            }
        }
        catch (Exception e)
        {
            Log.i(tag, "e:" + e);
        }
    }

    public static void setSetting(String s, String val, SharedPreferences p)
    {
        if(p != null) {
            SharedPreferences.Editor editor = p.edit();
            editor.putString(s, val);
            editor.commit();
        }
    }

    public static String getSetting(String s, SharedPreferences p)
    {
        if(p != null) {
            return p.getString(s, "");
        }

        return "";
    }

    public static String getSetting(String s, String dv, SharedPreferences p)
    {
        if(p != null) {
            return p.getString(s, dv);
        }

        return "";
    }

    public static void setTextView(View v, int rid, String txt)
    {
        if(v != null) {
            TextView tv = (TextView) v.findViewById(rid);
            if (tv != null) {
                tv.setText(txt);
            }
        }
    }

    public static String getTextView(View v, int rid)
    {
        if(v != null) {
            TextView tv = (TextView) v.findViewById(rid);
            if (tv != null) {
                return tv.getText().toString();
            }
        }

        return "";
    }

    public static void setBGColor(View v, int rid, int c)
    {
        if(v != null) {
            View tv = v.findViewById(rid);
            if (tv != null) {
                tv.setBackgroundColor(c);
            }
        }
    }

    public static void setTextColor(final View v, final int rid, final int c)
    {
        Routines.ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                if(v != null) {
                    TextView tv = (TextView)v.findViewById(rid);
                    if (tv != null) {
                        tv.setTextColor(c);
                    }
                }
            }
        });
    }

    public static String getEditTextView(View v, int rid)
    {
        if(v != null) {
            EditText tv = (EditText) v.findViewById(rid);
            if (tv != null) {
                return tv.getText().toString();
            }
        }

        return "";
    }

    public static void setBtnText(View v, int rid, String txt)
    {
        Button tv = (Button)v.findViewById(rid);
        if(tv != null)
        {
            tv.setText(txt);
        }
    }

    public static String getBtnText(View v, int rid)
    {
        Button tv = (Button)v.findViewById(rid);
        if(tv != null)
        {
            return tv.getText().toString();
        }

        return "";
    }

    public static void delayedEvent(final DelayedEvent ev)
    {
        Handler handler = new Handler(Looper.getMainLooper());
        Runnable runnable = new Runnable() {
            public void run() {
                if (ev != null) {
                    ev.onDelayedEvent();
                }
            }

        };

        handler.postDelayed(runnable, 10000);
    }

    public static void setBtnEnabled(View parent, int btn, boolean e)
    {
        if (parent != null) {
            View vtmp = parent.findViewById(btn);
            if (vtmp != null) {
                vtmp.setEnabled(e);
            }
        }
    }

    public static boolean getBtnEnabled(View parent, int btn)
    {
        if (parent != null) {
            View vtmp = parent.findViewById(btn);
            if (vtmp != null) {
                return vtmp.isEnabled();
            }
        }

        return false;
    }

    public static void setBtnClick(View v, int id, final ModelUI.eViewState s, final ModelUI ui, final ClickEvent event)
    {
        View b = v.findViewById(id);

        Log.i("setBtnClick", "v:" + v + "id:" + id);

        if (b != null) {
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.i("onClick", "view:" + view + " s:" + s);

                    if(event != null) {
                        event.onClick();
                    }

                    if(ui != null) {
                        if (s != null) {
                            ui.setViewState(s);
                        }
                    }
                }
            });
        }
    }

    public static void checkPinCode(final View v, int id, final TextChangedEvent ev)
    {
        final EditText codeText = (EditText)v.findViewById(id);

        Log.i(TAG, "codeText:" + codeText);

        codeText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                Log.i(TAG, "s:" + s + " ev:" + ev);

                if (ev != null) {
                    ev.onTextChanged(codeText, s, i, i1, i2);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    public static void setViewVisible(View p, int id, int vis)
    {
        View v = p.findViewById(id);
        if(v != null)
        {
            v.setVisibility(vis);
        }
    }

    public static void setViewVisible(final Activity a, final int id, final int vis)
    {
        Routines.ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                a.findViewById(id).setVisibility(vis);
            }
        });
    }

    public static void setChecked(final View p, final int rid, final boolean ch)
    {
        ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                ((CheckBox) p.findViewById(rid)).setChecked(ch);
            }
        });
    }

    public static void setCheckedChanged(final View p, final int rid, final CheckEvent ev)
    {
        ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                ((CheckBox)p.findViewById(rid)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        ev.onCheckEvent(b);
                    }
                });
            }
        });
    }

    public static void setHorizontalWeight(final View p, final int rid, final int w)
    {
        Routines.ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                View v = p.findViewById(rid);
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.getLayoutParams();
                lp.weight = w;
                v.setLayoutParams(lp);
            }
        });
    }

    public static void setHorizontalWidth(final View p, final int rid, final int w)
    {
        Routines.ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                View v = p.findViewById(rid);
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)v.getLayoutParams();
                lp.width = w;
                v.setLayoutParams(lp);
            }
        });
    }

    public static void setFrameLeft(final View p, final int rid, final int l)
    {
        Routines.ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                View v = p.findViewById(rid);
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams)v.getLayoutParams();
                lp.leftMargin = l;
                v.setLayoutParams(lp);
            }
        });
    }

    public static void setLinearLeft(final View p, final int rid, final int l)
    {
        Routines.ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                View v = p.findViewById(rid);
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)v.getLayoutParams();
                lp.leftMargin = l;
                v.setLayoutParams(lp);
            }
        });
    }

    public static int getViewLeft(final View p, final int rid)
    {
        if(p != null)
        {
            View v = p.findViewById(rid);
            if(v != null) {
                return v.getLeft();
            }
        }

        return 0;
    }

    public static int getViewWidth(final View p, final int rid)
    {
        if(p != null)
        {
            View v = p.findViewById(rid);
            if(v != null) {
                return v.getWidth();
            }
        }

        return 0;
    }

    public static void setViewWidth(final View p, final int rid, int w)
    {
        if(p != null)
        {
            View v = p.findViewById(rid);
            if(v != null) {
                ViewGroup.LayoutParams lp = v.getLayoutParams();
                lp.width = w;
                v.setLayoutParams(lp);
            }
        }
    }

    public static View addChildView(final View p, final int rid, final View ch)
    {
        Routines.ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                ((ViewGroup)p.findViewById(rid)).addView(ch);
            }
        });

        return ch;
    }

    public static void setTouchListener(final View p, final int rid, final View.OnTouchListener l)
    {
        Routines.ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                p.findViewById(rid).setOnTouchListener(l);
            }
        });
    }

    public static void baseAnimation(final View v, final String param, final float end, final int dur, final Animator.AnimatorListener list)
    {
        ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                final ObjectAnimator mover = ObjectAnimator.ofFloat(v,
                        param, end);
                mover.setDuration(dur);

                AnimatorSet set = new AnimatorSet();
                set.play(mover);
                set.addListener(list);
                set.start();
            }
        });
    }

    public static void sequnceAnimation3(final View v, final String[] param, final float[] end, final int dur, final Animator.AnimatorListener list)
    {
        ExceptionMethod(TAG, new IExceptionMethod() {
            @Override
            public void onMethod() {
                final ObjectAnimator mover1 = ObjectAnimator.ofFloat(v,
                        param[0], end[0]);
                mover1.setDuration(dur);

                final ObjectAnimator mover2 = ObjectAnimator.ofFloat(v,
                        param[1], end[1]);
                mover2.setDuration(dur);

                final ObjectAnimator mover3 = ObjectAnimator.ofFloat(v,
                        param[2], end[2]);
                mover3.setDuration(dur);

                AnimatorSet set = new AnimatorSet();
                set.play(mover1).with(mover2).with(mover3);
                set.addListener(list);
                set.start();
            }
        });
    }
}
