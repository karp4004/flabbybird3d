package fb3d;

import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import controller.ControllerUI;
import gollardengine.Native;
import fb3d.test.R;
import model.ModelResorces;
import model.ModelUI;

public class MainActivity extends AppCompatActivity {

    static String TAG = "MainActivity";

    ModelUI mModelUI = new ModelUI();
    ModelResorces mModelResorces = new ModelResorces();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        new ControllerUI(this, mModelUI, mModelResorces);
    }

    @Override public void onBackPressed()
    {
        mModelUI.prevViewState();

        if(mModelUI.getCurrent() == ModelUI.eViewState.eExit)
        {
            Native.releaseScene();
            Native.onDestroy();
            super.onBackPressed();
        }
    }
}
