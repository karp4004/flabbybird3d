#include "WinFileManager.h"

#include <io.h>

#include "Core/FileDir.h"
#include "Core/File.h"
#include "WinFileDir.h"
#include "WinFile.h"

#define DEBUG 1
#define TRACE 1
#define LOG_COLOR "#FFFF00"
#include "Core/Logger.h"

void WinFileManager::setPath(string uri)
{
	mUri = uri;
}

shared_ptr<FileDir> WinFileManager::OpenDir(string dir)
{
	shared_ptr<WinFileDir> ret(new WinFileDir(mUri + dir));

	return ret;
}

shared_ptr<File> WinFileManager::OpenFile(string uri, FileOperation op)
{
	shared_ptr<WinFile> ret(new WinFile(mUri + uri, op));

	return ret;
}

int WinFileManager::OpenFileDescriptor(string uri, int &start, int &length)
{
	return 0;
}
