#ifndef AndroidLoggerH
#define AndroidLoggerH

#include "Platform/Logger.h"

class AndroidLogger: public Logger
{
private:
	AndroidLogger(int level):Logger(level) {}

public:
	virtual int LogInfo(const char *tag, const char *fmt, ...);
	virtual int LogDebug(const char *tag, const char *fmt, ...);
	virtual int LogError(const char *tag, const char *fmt, ...);
};

#endif