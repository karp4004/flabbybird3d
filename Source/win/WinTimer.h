/*
 * WinTimer.h
 *
 *  Created on: 31.08.2014
 *      Author: karp4004
 */

#ifndef WINTIMER_H_
#define WINTIMER_H_

#include <windows.h>
#include "Core/Timer.h"
#include <string>

using namespace std;

class WinTimer: public Timer
{
private:
	WinTimer();

public:
	static WinTimer& instance()
	{
		static WinTimer INSTANCE;
		return INSTANCE;
	}

public:
	virtual int Update();
	virtual int Reset();

private:
	double PCFreq;
	__int64 CounterStart;
};



#endif /* WINTIMER_H_ */
