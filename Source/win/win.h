#include "Core/ActivityObject.h"
#include "Core/OpenGL.h"
#include "Scene/MeshNode.h"
#include "Scene/Camera.h"
#include "Scene/LightNode.h"
#include "CSInterop.h"

#ifdef __cplusplus
extern "C" {
#endif
	DLLEXPORT int STDCALL LoadResources(const char* path);
	DLLEXPORT int STDCALL CreateScene();
	DLLEXPORT int STDCALL CreateContext(HWND hwnd);
	DLLEXPORT int STDCALL OnDrawContext(HWND hwnd);
	DLLEXPORT int STDCALL ReleaseContext(HWND hwnd);
#ifdef __cplusplus
}
#endif

