#ifndef Win_FILE_H
#define Win_FILE_H

#include "Core/File.h"

#include <string>
#include <stdio.h>

using namespace std;

class WinFile: public File
{
public:
	WinFile(string name, FileOperation op);
	virtual ~WinFile();

	virtual int Length();
	virtual int Read(char* buf, int len);
	virtual int Write(char* buf, int len);

	int Log(string module, string name){return 0;};

private:
	FILE* file;
	long mLength;
};

#endif
