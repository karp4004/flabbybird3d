#include "WinLogger.h"

#include <stdio.h>
#include <stdarg.h>

int AndroidLogger::LogInfo(const char *tag, const char *format, ...)
{
	int ret = -1;

	if (_Level | kINFO)
	{
		va_list arg;

		va_start(arg, format);
		ret = vfprintf(stderr, format, arg);
		va_end(arg);
	}

	return ret;
}

int AndroidLogger::LogDebug(const char *tag, const char *format, ...)
{
	int ret = -1;

	if (_Level | kDEBUG)
	{
		va_list arg;

		va_start(arg, format);
		ret = vfprintf(stderr, format, arg);
		va_end(arg);
	}

	return ret;
}

int AndroidLogger::LogError(const char *tag, const char *format, ...)
{
	int ret = -1;

	if (_Level | kERROR)
	{
		va_list arg;

		va_start(arg, format);
		ret = vfprintf(stderr, format, arg);
		va_end(arg);
	}

	return ret;
}
