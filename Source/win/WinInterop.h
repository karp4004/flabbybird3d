#ifndef WinInteropH
#define WinInteropH

#include "Platform/Interop.h"

class WinInterop: public Interop
{
private:
	WinInterop() {}

public:
	static WinInterop& instance()
	{
		static WinInterop INSTANCE;
		return INSTANCE;
	}

	virtual void ResourcesLoaded();
	virtual void CollisionBird();
	virtual void SceneCreated();
};

#endif