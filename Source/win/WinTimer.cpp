#include "WinTimer.h"

WinTimer::WinTimer():
Timer()
{
	Reset();
}

int WinTimer::Update()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    double fdelta = double(li.QuadPart-CounterStart)/PCFreq;
    CounterStart = li.QuadPart;

	SetDelta(fdelta);
	CountElapsed(fdelta);

	return 0;
}

int WinTimer::Reset()
{
	Timer::Reset();

    LARGE_INTEGER li;
    QueryPerformanceFrequency(&li);

    PCFreq = double(li.QuadPart);

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;

    return 0;
}





