#include "WinFileDir.h"

WinFileDir::WinFileDir(string name):
FileDir()
,handle(0)
{
	name += "/*.*";

	handle = _findfirst(name.c_str(), &dir);

	_findnext(handle, &dir);
}

const char* WinFileDir::GetNextFileName()
{
	if(!_findnext(handle, &dir))
	{
		if(!(dir.attrib & _A_SUBDIR))
		{
			return dir.name;
		}
	}

	return 0;
}
