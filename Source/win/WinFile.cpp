#include "WinFile.h"
#include "Core/ActivityObject.h"

WinFile::WinFile(string name, FileOperation op):
File(op)
,mLength(0)
{
	Platform::instance().Logger().LogInfo(__FILE__, "WinFile:%s", name.c_str());

	EXCEPTION_TRY

		if (op == kFileRead)
		{
			errno_t err = fopen_s(&file, name.c_str(), "rb");
			if (file)
			{
				fseek(file, 0, SEEK_END);   // non-portable
				mLength = ftell(file);
				fseek(file, 0, SEEK_SET);
			}
			else
			{
				Platform::instance().Logger().LogError(__FILE__, "fopen_s fail:%d", err);
			}
		}
		else if (op == kFileWrite)
		{
			errno_t err = fopen_s(&file, name.c_str(), "wb");
		}

	EXCEPTION_CATCH
}

WinFile::~WinFile()
{
	fclose(file);
}

int WinFile::Length()
{
	return mLength;
}

int WinFile:: Read(char* buf, int len)
{
	Platform::instance().Logger().LogError(__FILE__, "len:%d", len);
	return fread(buf, 1, len, file);
}

int WinFile::Write(char* buf, int len)
{
	return fwrite(buf, 1, len, file);
}
