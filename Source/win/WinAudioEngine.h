#ifndef WinAudioEngineH
#define WinAudioEngineH

#include <string>

#include "Sound/AudioEngine.h"
#include "Core/SharedPtr.h"

using namespace std;

class WinAudioEngine: public AudioEngine
{
private:
	WinAudioEngine() {}

public:
	static WinAudioEngine& instance()
	{
		static WinAudioEngine INSTANCE;
		return INSTANCE;
	}

public:
	virtual ~WinAudioEngine();

	int CreateEngine();
	int createBufferQueueAudioPlayer();
	shared_ptr<AudioPlayer> createAssetAudioPlayer(string filename, bool loop);
};

#endif
