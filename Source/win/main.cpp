#include "win.h"
#include "CSInterop.h"
#include "Core/Logger.h"
#include "Core/CommandObject.h"
#include "Core/OpenGL.h"

#include <iostream>
#include <map>

using namespace std;

#define INFO 1
#define DEBUG 1
#define TRACE 0
#define LOG_COLOR "#00ff00"

PFNGLCREATESHADERPROC glCreateShader;
PFNGLSHADERSOURCEPROC glShaderSource;
PFNGLCOMPILESHADERPROC glCompileShader;
PFNGLGETSHADERIVPROC glGetShaderiv;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
PFNGLDELETESHADERPROC glDeleteShader;
PFNGLCREATEPROGRAMPROC glCreateProgram;
PFNGLATTACHSHADERPROC glAttachShader;
PFNGLLINKPROGRAMPROC glLinkProgram;
PFNGLGETPROGRAMIVPROC glGetProgramiv;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
PFNGLDELETEPROGRAMPROC glDeleteProgram;
PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
PFNGLUSEPROGRAMPROC glUseProgram;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
PFNGLUNIFORM3FVPROC glUniform3fv;
PFNGLUNIFORM1FVPROC glUniform1fv;
PFNGLACTIVETEXTUREARBPROC glActiveTexture;
PFNGLUNIFORM1IPROC glUniform1i;
PFNGLCOMPRESSEDTEXIMAGE2DPROC glCompressedTexImage2D;
PFNGLBINDBUFFERPROC glBindBuffer;
PFNGLBUFFERDATAPROC glBufferData;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray;
PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays;
PFNGLDELETEBUFFERSPROC glDeleteBuffers;
PFNGLGENBUFFERSPROC glGenBuffers;
PFNGLGENERATEMIPMAPPROC glGenerateMipmap;
PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB;
PFNGLUNIFORM4FVPROC glUniform4fv;

int LoadProcs()
{
	glCreateShader = (PFNGLCREATESHADERPROC)wglGetProcAddress("glCreateShader");
	glShaderSource = (PFNGLSHADERSOURCEPROC)wglGetProcAddress("glShaderSource");
	glCompileShader = (PFNGLCOMPILESHADERPROC)wglGetProcAddress("glCompileShader");
	glGetShaderiv = (PFNGLGETSHADERIVPROC)wglGetProcAddress("glGetShaderiv");
	glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)wglGetProcAddress("glGetShaderInfoLog");
	glDeleteShader = (PFNGLDELETESHADERPROC)wglGetProcAddress("glDeleteShader");
	glCreateProgram = (PFNGLCREATEPROGRAMPROC)wglGetProcAddress("glCreateProgram");
	glAttachShader = (PFNGLATTACHSHADERPROC)wglGetProcAddress("glAttachShader");
	glLinkProgram = (PFNGLLINKPROGRAMPROC)wglGetProcAddress("glLinkProgram");
	glGetProgramiv = (PFNGLGETPROGRAMIVPROC)wglGetProcAddress("glGetProgramiv");
	glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)wglGetProcAddress("glGetProgramInfoLog");
	glDeleteProgram = (PFNGLDELETEPROGRAMPROC)wglGetProcAddress("glDeleteProgram");
	glGetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC)wglGetProcAddress("glGetAttribLocation");
	glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)wglGetProcAddress("glGetUniformLocation");
	glUseProgram = (PFNGLUSEPROGRAMPROC)wglGetProcAddress("glUseProgram");
	glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC)wglGetProcAddress("glVertexAttribPointer");
	glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress("glEnableVertexAttribArray");
	glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC)wglGetProcAddress("glUniformMatrix4fv");
	glUniform3fv = (PFNGLUNIFORM3FVPROC)wglGetProcAddress("glUniform3fv");
	glUniform1fv = (PFNGLUNIFORM1FVPROC)wglGetProcAddress("glUniform1fv");
	glActiveTexture = (PFNGLACTIVETEXTUREARBPROC)wglGetProcAddress("glActiveTexture");
	glUniform1i = (PFNGLUNIFORM1IPROC)wglGetProcAddress("glUniform1i");
	glCompressedTexImage2D = (PFNGLCOMPRESSEDTEXIMAGE2DPROC)wglGetProcAddress("glCompressedTexImage2D");
	glBindBuffer = (PFNGLBINDBUFFERPROC)wglGetProcAddress("glBindBuffer");
	glBufferData = (PFNGLBUFFERDATAPROC)wglGetProcAddress("glBufferData");
	glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)wglGetProcAddress("glGenVertexArraysOES");
	glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)wglGetProcAddress("glBindVertexArrayOES");
	glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC)wglGetProcAddress("glDeleteVertexArraysOES");
	glDeleteBuffers = (PFNGLDELETEBUFFERSPROC)wglGetProcAddress("glDeleteBuffers");
	glGenBuffers = (PFNGLGENBUFFERSPROC)wglGetProcAddress("glGenBuffers");
	glGenerateMipmap = (PFNGLGENERATEMIPMAPPROC)wglGetProcAddress("glGenerateMipmap");
	wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
	glUniform4fv = (PFNGLUNIFORM4FVPROC)wglGetProcAddress("glUniform4fv");

	LOGD("glCreateShader:%p", glCreateShader);
	LOGD("glShaderSource:%p", glShaderSource);
	LOGD("glCompileShader:%p", glCompileShader);
	LOGD("glGetShaderiv:%p", glGetShaderiv);
	LOGD("glGetShaderInfoLog:%p", glGetShaderInfoLog);
	LOGD("glDeleteShader:%p", glDeleteShader);
	LOGD("glCreateProgram:%p", glCreateProgram);
	LOGD("glAttachShader:%p", glAttachShader);
	LOGD("glLinkProgram:%p", glLinkProgram);
	LOGD("glGetProgramiv:%p", glGetProgramiv);
	LOGD("glGetProgramInfoLog:%p", glGetProgramInfoLog);
	LOGD("glDeleteProgram:%p", glDeleteProgram);
	LOGD("glGetAttribLocation:%p", glGetAttribLocation);
	LOGD("glGetUniformLocation:%p", glGetUniformLocation);
	LOGD("glUseProgram:%p", glUseProgram);
	LOGD("glVertexAttribPointer:%p", glVertexAttribPointer);
	LOGD("glEnableVertexAttribArray:%p", glEnableVertexAttribArray);
	LOGD("glUniformMatrix4fv:%p", glUniformMatrix4fv);
	LOGD("glUniform3fv:%p", glUniform3fv);
	LOGD("glUniform1fv:%p", glUniform1fv);
	LOGD("glActiveTexture:%p", glActiveTexture);
	LOGD("glUniform1i:%p", glUniform1i);
	LOGD("glCompressedTexImage2D:%p", glCompressedTexImage2D);
	LOGD("glBindBuffer:%p", glBindBuffer);
	LOGD("glBufferData:%p", glBufferData);
	LOGD("glGenVertexArrays:%p", glGenVertexArrays);
	LOGD("glBindVertexArray:%p", glBindVertexArray);
	LOGD("glDeleteVertexArrays:%p", glDeleteVertexArrays);
	LOGD("glGenerateMipmap:%p", glGenerateMipmap);
	LOGD("wglCreateContextAttribsARB:%p", wglCreateContextAttribsARB);
	LOGD("glUniform4fv:%p", glUniform4fv);

	return 0;
}

HGLRC hrc; // Rendering context
HDC hdc; // Device context
HWND hWnd;

bool running = true; // Whether or not the application is currently running

map<unsigned char, unsigned char> keys;

HINSTANCE hInstance; // The HINSTANCE of this application
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM); // Standard window callback

int STDCALL OnKeyEscape(KeyEvent e)
{
	LOGT("", "");

	running = false;

	return 0;
}

typedef int (STDCALL *KeyCommand)(KeyEvent e);
KeyEvent mouseState;

KeyCommand keyHandlers[] =
{
		0,//0
		0,//1
		0,//2
		0,//3
		0,//4
		0,//5
		0,//6
		0,//7
		0,//8
		0,//9
		0,//10
		0,//11
		0,//12
		0,//13
		0,//14
		0,//15
		0,//16
		0,//17
		0,//18
		0,//19
		0,//20
		0,//21
		0,//22
		0,//23
		0,//24
		0,//25
		0,//26
		OnKeyEscape,//27
		0,//28
		0,//29
		0,//30
		0,//31
		0,//32
		0,//33
		0,//34
		0,//35
		0,//36
		OnKeyLeft,//37
		OnKeyUp,
		OnKeyRight,
		OnKeyDown,
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,OnKeyLeft,0,0,OnKeyRight,0,0,
		0,0,0,0,0,0,0,0,0,0,
		0,0,OnKeyDown,0,0,0,OnKeyUp,0,0,0,
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,
};

/**
	WndProc is a standard method used in Win32 programming for handling Window messages. Here we
	handle our window resizing and tell our OpenGLContext the new window size.
*/
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	LOGT("", "");

	switch (message) {
		case WM_SIZE: // If our window is resizing
		{
			OnSurfaceChanged(LOWORD(lParam), HIWORD(lParam));
			break;
		}

		case WM_DESTROY:
		{
			PostQuitMessage(0);
			break;
		}
		case WM_KEYDOWN:
		{
			LOGD("wParam:%d", wParam);

			keys[wParam] = wParam;

			if(keyHandlers[wParam])
			keyHandlers[wParam](kKeyDown);
			break;
		}
		case WM_KEYUP:
		{
			LOGD("wParam:%d", wParam);

			keys.erase(wParam);

			if(keyHandlers[wParam])
			keyHandlers[wParam](kKeyUp);
			break;
		}
		case WM_LBUTTONDOWN:
		{
			LOGD("wParam:%d", wParam);

			OnTouchDown();

			break;
		}
		case WM_LBUTTONUP:
		{
			LOGD("wParam:%d", wParam);

			OnTouchUp();

			break;
		}
		case WM_MOUSEMOVE:
		{
			OnMouseMove();
		}
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void setLogik()
{
	PhysicScene* s;

	{
		MoveableDescriptor d;
		d.name = "Scene1";
		s = AddPhysicScene(d);
	}

	{
		CameraDescriptor d;
		d.mMoveableDescriptor.name = "cam1";

		float p[] = { -0.0f, 30.0f, 50.0f };
		memcpy(d.mMoveableDescriptor.position, p, sizeof(p));

		float t[] = { 0.0f, 0.0f, 0.0f };
		memcpy(d.mTarget, t, sizeof(t));

		float u[] = { 0.0f, 1.0f, 0.0f };
		memcpy(d.mUp, u, sizeof(u));

		d.mFarPlane = 1000.0f;
		AddCamera(s, d);
	}

	{
		MoveableDescriptor d;

		{
			d.name = "mLight2";
			float p[] = { -4.0f, 100.0f, -6.0f };
			memcpy(d.position, p, sizeof(p));
			AddLight(s, d);
		}

		{
			d.name = "mLight";
			float p[] = { 0.0f, 100.0f, -6.0f };
			memcpy(d.position, p, sizeof(p));
			AddLight(s, d);
		}
	}

	{
		CarDescriptor d;

		d.mMeshDescriptor.mMoveableDescriptor.name = "player_veh";
		float p[] = { 0, 10, 0 };
		memcpy(d.mMeshDescriptor.mMoveableDescriptor.position, p, sizeof(p));
		float sc[] = { 1.0f, 1.0f, 1.0f };
		memcpy(d.mMeshDescriptor.mMoveableDescriptor.scale, sc, sizeof(sc));

		d.mMeshDescriptor.file_name = "Jeep_rigged.glrd";
		d.mMeshDescriptor.shader = "perpixel_light";

		float w1[] = { 0.0f, -0.0f, -0.1f };
		memcpy(d.w1, w1, sizeof(w1));
		float w2[] = { 0.0f, -0.0f, -0.1f };
		memcpy(d.w2, w2, sizeof(w2));
		float w3[] = { 0.0f, -0.0f, 0.3f };
		memcpy(d.w3, w3, sizeof(w3));
		float w4[] = { 0.0f, -0.0f, 0.3f };
		memcpy(d.w4, w4, sizeof(w4));

		d.wheel_mesh = "lv.glrd";

		CreateCar(s, d);
	}


	{
		MeshDescriptor d;
		d.mMoveableDescriptor.name = "anim";
		float p[] = { 10.0f, 0.0f, 0.0f };
		memcpy(d.mMoveableDescriptor.position, p, sizeof(p));
		float sc[] = { 1.0f, 1.0f, 1.0f };
		memcpy(d.mMoveableDescriptor.scale, sc, sizeof(sc));

		d.file_name = "nightwinganim.glrd";
		d.shader = "perpixel_light";

		AddMesh(s, d);
	}

	{
		RigidNodeDescriptor d;

		d.mMeshDescriptor.mMoveableDescriptor.name = "floor";
		float p[] = { 0.0f, 0.0f, 0.0f };
		memcpy(d.mMeshDescriptor.mMoveableDescriptor.position, p, sizeof(p));
		float sc[] = { 1.0f, 1.0f, 1.0f };
		memcpy(d.mMeshDescriptor.mMoveableDescriptor.scale, sc, sizeof(sc));

		d.mMeshDescriptor.file_name = "table3.glrd";
		d.mMeshDescriptor.shader = "perpixel_light";

		d.mMass = 0.0f;

		float in[] = { 0.0f, 0.0f, 0.0f };
		memcpy(d.mInertia, in, sizeof(in));
		float o[] = { 1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, -8.5f, 0.0f, 1.0f };
		memcpy(d.mOffset, o, sizeof(o));
		float hull[] = { 70.0f, 20.0f, 40.0f };
		memcpy(d.mHull, hull, sizeof(hull));

		AddRigidMesh(s, d);
	}

	{
		RigidNodeDescriptor d;

		d.mMeshDescriptor.mMoveableDescriptor.name = "anim1";
		float p[] = { -10.0f, 10.0f, 0.0f };
		memcpy(d.mMeshDescriptor.mMoveableDescriptor.position, p, sizeof(p));
		float sc[] = { 1.0f, 1.0f, 1.0f };
		memcpy(d.mMeshDescriptor.mMoveableDescriptor.scale, sc, sizeof(sc));

		d.mMeshDescriptor.file_name = "bob.glrd";
		d.mMeshDescriptor.shader = "skeletal";

		d.mMass = 1.0f;

		float in[] = { 1.0f, 1.0f, 1.0f };
		memcpy(d.mInertia, in, sizeof(in));
		float o[] = { 1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f };
		memcpy(d.mOffset, o, sizeof(o));
		float hull[] = { 0.1f, 0.3f, 0.1f };
		memcpy(d.mHull, hull, sizeof(hull));

		AddRigidMesh(s, d);
	}
}

/**
	createWindow is going to create our window using Windows API calls. It is then going to
	create our OpenGL context on the window and then show our window, making it visible.
*/
bool createWindow(LPCSTR title, int width, int height) {
	LOGT("", "");


	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

	hInstance = GetModuleHandle(NULL);

	WNDCLASS windowClass;
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = (WNDPROC) WndProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = NULL;
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = title;

	if (!RegisterClass(&windowClass)) {
		return false;
	}

	HWND hWnd = CreateWindowEx(dwExStyle, title, title, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, width, height, NULL, NULL, hInstance, NULL);

	SetCursorHWND(hWnd);

	hdc = GetDC(hWnd); // Get the device context for our window

	PIXELFORMATDESCRIPTOR pfd; // Create a new PIXELFORMATDESCRIPTOR (PFD)
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR)); // Clear our  PFD
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR); // Set the size of the PFD to the size of the class
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW; // Enable double buffering, opengl support and drawing to a window
	pfd.iPixelType = PFD_TYPE_RGBA; // Set our application to use RGBA pixels
	pfd.cColorBits = 32; // Give us 32 bits of color information (the higher, the more colors)
	pfd.cDepthBits = 32; // Give us 32 bits of depth information (the higher, the more depth levels)
	pfd.iLayerType = PFD_MAIN_PLANE; // Set the layer of the PFD

	int nPixelFormat = ChoosePixelFormat(hdc, &pfd); // Check if our PFD is valid and get a pixel format back
	if (nPixelFormat == 0) // If it fails
			return false;

	bool bResult = SetPixelFormat(hdc, nPixelFormat, &pfd); // Try and set the pixel format based on our PFD
	if (!bResult) // If it fails
		return false;

	HGLRC tempOpenGLContext = wglCreateContext(hdc); // Create an OpenGL 2.1 context for our device context
	wglMakeCurrent(hdc, tempOpenGLContext); // Make the OpenGL 2.1 context current and active

	int attributes[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3, // Set the MAJOR version of OpenGL to 3
		WGL_CONTEXT_MINOR_VERSION_ARB, 2, // Set the MINOR version of OpenGL to 2
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, // Set our OpenGL context to be forward compatible
		0
	};

	if (0) { // If the OpenGL 3.x context creation extension is available
		hrc = wglCreateContextAttribsARB(hdc, NULL, attributes); // Create and OpenGL 3.x context based on the given attributes
		wglMakeCurrent(NULL, NULL); // Remove the temporary context from being active
		wglDeleteContext(tempOpenGLContext); // Delete the temporary OpenGL 2.1 context
		wglMakeCurrent(hdc, hrc); // Make our OpenGL 3.0 context current
	}
	else {
		hrc = tempOpenGLContext; // If we didn't have support for OpenGL 3.x and up, use the OpenGL 2.1 context
	}

	int glVersion[2] = {-1, -1}; // Set some default values for the version
	glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]); // Get back the OpenGL MAJOR version we are using
	glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]); // Get back the OpenGL MAJOR version we are using

	std::cout << "Using OpenGL: " << glVersion[0] << "." << glVersion[1] << std::endl; // Output which version of OpenGL we are using

	OnLoadResources("../../../../VisualStudio2015/android/assets/");
	OnCreate();

	setLogik();

	OnSurfaceCreated();

	ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);

	return true;
}

/**
	WinMain is the main entry point for Windows based applications as opposed to 'main' for console
	applications. Here we will make the calls to create our window, setup our scene and then
	perform our 'infinite' loop which processes messages and renders.
*/
int main(int argc, char** argv)
{
	LOGT("", "");

	MSG msg;

	/**
		The following 6 lines of code do conversion between char arrays and LPCWSTR variables
		which are used in the Windows API.
	*/
	char *orig = "OpenGL 3 Project"; // Our windows title

	createWindow(orig, 1600, 900); // Create our OpenGL window

	/**
		This is our main loop, it continues for as long as running is true
	*/
	while (running)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) { // If we have a message to process, process it
			if (msg.message == WM_QUIT) {
				running = false; // Set running to false if we have a message to quit
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else { // If we don't have a message to process

			OnTouchMove();

			map<unsigned char, unsigned char>::iterator it = keys.begin();
			while(it != keys.end())
			{
				if(keyHandlers[it->first])
				keyHandlers[it->first](kKeyPress);

				it++;
			}

		    OnDrawFrame();

		    SwapBuffers(hdc); // Swap buffers so we can see our rendering
		}
	}

    wglMakeCurrent(hdc, 0); // Remove the rendering context from our device context
    wglDeleteContext(hrc); // Delete our rendering context

    ReleaseDC(hWnd, hdc); // Release the device context from our window

	return (int) msg.wParam;
}
