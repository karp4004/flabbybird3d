#ifndef Win_FILE_MANAGER_H
#define Win_FILE_MANAGER_H

#include "Core/SharedPtr.h"
#include "Core/FileManager.h"

#include <string>

using namespace std;

class FileDir;
class File;

class WinFileManager: public FileManager
{
private:
	WinFileManager() {}

public:
	static WinFileManager& instance()
	{
		static WinFileManager INSTANCE;
		return INSTANCE;
	}

public:
	void setPath(string uri);
	virtual shared_ptr<FileDir> OpenDir(string name);
	virtual shared_ptr<File> OpenFile(string uri, FileOperation op);
	virtual int OpenFileDescriptor(string uri, int &start, int &length);

	int Log(string module, string name){return 0;};

private:
	string mUri;
};

#endif
