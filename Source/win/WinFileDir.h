#ifndef Win_FILE_DIR
#define Win_FILE_DIR

#include "Core/FileDir.h"

#include <string>
#include <io.h>

using namespace std;

class WinFileDir: public FileDir
{
public:
	WinFileDir(string name);

	virtual const char* GetNextFileName();

	int Log(string module, string name){return 0;};

private:
	_finddata_t dir;
	intptr_t handle;
};

#endif
