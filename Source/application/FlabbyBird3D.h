#ifndef CustomApplicationH
#define CustomApplicationH

#include "ApplicationFrame.h"
#include "Bird.h"
#include "Pipe.h"
#include "PipeController.h"

using namespace std;

class FlabbyBird3D: public ApplicationFrame
{
private:
	FlabbyBird3D() { _Bird = NULL; }

public:
	static FlabbyBird3D& instance()
	{
		static FlabbyBird3D INSTANCE;
		return INSTANCE;
	}

	virtual int onCreateScene();
	virtual void onTouchScreen();
	float getScores() { if (_Bird != NULL) return _Bird->getScore(); return 0.f; }
	shared_ptr<Pipe> generatePipe(float offsetx, float offsety);
	shared_ptr<Bird> generateBird(shared_ptr<PipeController> c);
	shared_ptr<PipeController> generatePipeController();
	void releasePipe(shared_ptr<Pipe> p);

private:
	shared_ptr<Bird> _Bird;
};

#endif
