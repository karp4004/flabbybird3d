#include "PipeController.h"
#include "Platform/Platform.h"
#include "FlabbyBird3D.h"

const float tube_hole = 8.f;
const float tube_interval = 8.f*2.6f;

PipeController::PipeController()
{
	_offset = tube_interval *2;
}

void PipeController::generatePipe()
{
	EXCEPTION_TRY

	_PipeList.push_back(FlabbyBird3D::instance().generatePipe(_offset, tube_hole));
	_offset += tube_interval;

	Platform::instance().Logger().LogInfo(__FILE__, "releasePipe:%d", _PipeList.size());

	EXCEPTION_CATCH
}

shared_ptr<Pipe> PipeController::getTargetPipe(float border)
{
	deque<shared_ptr<Pipe>>::iterator it = _PipeList.begin();
	while (it != _PipeList.end())
	{
		if (it->get()->getBorder() > border)
		{
			return (*it);
		}

		it++;
	}
}

int PipeController::onDrawFrame(double deltaTime)
{
	if(_PipeList.size() < 1)
	{
		generatePipe();
	}

	shared_ptr<Pipe> lastPipe = _PipeList.back();
	if (lastPipe != NULL)
	{
		if (!lastPipe->checkOutRight())
		{
			generatePipe();
		}
	}

	shared_ptr<Pipe> firstPipe = _PipeList.front();
	if (firstPipe != NULL)
	{
		if (firstPipe->checkOutLeft())
		{
			EXCEPTION_TRY

			FlabbyBird3D::instance().releasePipe(firstPipe);
			_PipeList.pop_front();

			EXCEPTION_CATCH

			Platform::instance().Logger().LogInfo(__FILE__, "releasePipe:%d", _PipeList.size());
		}
	}

	return 0;
}

int PipeController::onReleaseScene()
{
	EXCEPTION_TRY
	_PipeList.clear();
	EXCEPTION_CATCH

	return 0;
}
