#ifndef BirdH
#define BirdH

#include "Core/SharedPtr.h"

#include "Scene/MeshNode.h"
#include "Sound/AudioEngine.h"

#include "PipeController.h"

using namespace std;

class Bird: public MeshNode
{
public:
	Bird(shared_ptr<Mesh> mesh, shared_ptr<Shader> shader, shared_ptr<PipeController> ctrl, Matrix4 origin = matrixIdentity());
	virtual int onSurfaceCreated(); 
	virtual int onDrawFrame(double deltaTime);
	virtual int onReleaseScene();
	void Fly();
	float CmaeraDistane();
	void GrabCameraAndLight(shared_ptr<LightNode> light, shared_ptr<Camera> cam);
	float getScore() { return _Score; }
	bool checkFloorCeilColision();

private:
	shared_ptr<LightNode> _Light;
	shared_ptr<Camera> _Camera;

	float _fallSpeed;
	float _fallAcc;
	float startTimeout;

	float _Score;

	shared_ptr<AudioPlayer> _Sound;
	shared_ptr<AudioPlayer> _SoundCollide;
	shared_ptr<PipeController> _Controller;
};

#endif
