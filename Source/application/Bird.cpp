#include "Bird.h"

#include "ApplicationFrame.h"

#include "Scene/Camera.h"
#include "Scene/LightNode.h"
#include "Scene/MeshNode.h"
#include "Resource/Texture.h"
#include "Resource/MeshPrimitive.h"
#include "Platform/Platform.h"

const float speed_scale = 12.f;
const float camera_offset = 6.f;

Bird::Bird(shared_ptr<Mesh> mesh, shared_ptr<Shader> shader, shared_ptr<PipeController> ctrl, Matrix4 origin):
MeshNode(mesh, shader, origin)
, startTimeout(0.f)
, _Controller(ctrl)
, _Light(NULL)
, _Camera(NULL)
, _Sound(NULL)
{
	_fallSpeed = 0;
	_fallAcc = -9.8 * speed_scale;
	_Score = 0.f;
}

float Bird::CmaeraDistane()
{ 
	return GetDimensions().x * camera_offset;
}

int Bird::onSurfaceCreated()
{
	_Sound = Platform::instance().AudioEngine().createAssetAudioPlayer("bird.mp3", true);
	if (_Sound)
	{
		int millibel = -500;
		_Sound->setVolume(millibel);
		_Sound->setPlayingAssetAudioPlayer(true);
	}

	_SoundCollide = Platform::instance().AudioEngine().createAssetAudioPlayer("birds001.wav", false);
	if (_Sound)
	{
		int millibel = -500;
		_SoundCollide->setVolume(millibel);
	}

	return MeshNode::onSurfaceCreated();
}

bool Bird::checkFloorCeilColision()
{
	Vector3 pos = GetPosition();
	Matrix4 m;
	Vector3 scrBottom = Platform::instance().Renderer().Project(pos, m);

	pos.y += GetDimensions().y * GetScale().y;
	Vector3 scrTop = Platform::instance().Renderer().Project(pos, m);

	int* vp = Platform::instance().Renderer().getViewport();
	if (vp != NULL)
	{
		//LOGI("scrTop:%f\%f\%f", scrTop.x, scrTop.y, scrTop.z)
		//LOGI("scrBottom:%f\%f\%f", scrBottom.x, scrBottom.y, scrBottom.z)
		//LOGI("vp:%f\%f", vp[1], vp[3])

		if (scrBottom.y >= vp[3] || scrTop.y <= vp[1])
		{
			return true;
		}
	}

	return false;
}

int Bird::onDrawFrame(double deltaTime)
{
	startTimeout += deltaTime;

	Vector3 pos = GetPosition();
	pos.y += GetDimensions().y * GetScale().y / 2;

	if (_Light != NULL)
	{
		Vector3 p = GV(pos.x, 0, CmaeraDistane());
		_Light->SetPosition(p);
	}

	if (_Camera != NULL)
	{
		Vector3 p = GV(-pos.x, 0, -CmaeraDistane());
		_Camera->SetPosition(p);
	}

	bool collision = false;
	shared_ptr<Pipe> target = _Controller->getTargetPipe(pos.x - GetDimensions().z / 2);
	if (target != NULL)
	{
		//LOGI("target:%f\%f\%f", target->getBorder(), pos.x, GetDimensions().z)

		Vector2 center = GV(pos.x, pos.y);
		collision = target->intersectCircle(center, GetDimensions().y * GetScale().y / 2);
	}

	collision = collision || checkFloorCeilColision();

	if (!collision)
	{
		if (startTimeout > 2.0f)
		{
			_fallSpeed = _fallSpeed + _fallAcc*deltaTime;
		}

		Vector3 m = GV(deltaTime * speed_scale, _fallSpeed*deltaTime, 0);
		Move(m);

		_Score += deltaTime;
	}
	else
	{
		if (_SoundCollide != NULL)
		{
			_SoundCollide->setPlayingAssetAudioPlayer(true);
		}

		Platform::instance().Interop().CollisionBird();
	}

	return MeshNode::onDrawFrame(deltaTime);
}

int Bird::onReleaseScene()
{
	if (_Sound != NULL)
	{
		_Sound->setPlayingAssetAudioPlayer(false);
	}

	if (_SoundCollide != NULL)
	{
		_SoundCollide->setPlayingAssetAudioPlayer(false);
	}

	return 0;
}

void Bird::Fly()
{
	if (startTimeout > 2.0f)
	{
		_fallSpeed = -_fallAcc / 3;
	}
}

void Bird::GrabCameraAndLight(shared_ptr<LightNode> light, shared_ptr<Camera> cam)
{
	_Light = light;
	_Camera = cam;

	_Camera->LookAt(GV(0.f, 0.f, CmaeraDistane()),
		GV(0, 0, 0),
		GV(0, 1, 0));
}