#include "Pipe.h"
#include "Platform/Platform.h"

Pipe::Pipe(float offsetx, float offsety)
{
	float drand = rand() % 20 + 1 - 10;

	Matrix4 t = matrixTranslate(offsetx, offsety + drand, 0);
	Vector3 s = GV(1, 1, 1);
	_top = addMesh("tube.glrd", t, "perpixel_light", s);
	_top->setCullingType(kScreenCull);

	t = matrixTranslate(offsetx, -_top->GetDimensions().y - offsety + drand, 0);
	s = GV(1, 1, 1);
	_bottom = addMesh("tube.glrd", t, "perpixel_light", s);
	_bottom->setCullingType(kScreenCull);

	if (_top != NULL)
	{
		_Border._left = _top->GetPosition().x - _top->GetDimensions().x / 2;
		_Border._right = _top->GetPosition().x + _top->GetDimensions().x / 2;
	}
	else if (_bottom != NULL)
	{
		_Border._left = _bottom->GetPosition().x - _bottom->GetDimensions().x / 2;
		_Border._right = _bottom->GetPosition().x + _bottom->GetDimensions().x / 2;
	}

	_Border._top = offsety + drand;
	_Border._bottom = -offsety + drand;
}

bool Pipe::intersectCircle(Vector2& c, float r)
{
	int idx = 0;
	if (c.y <= _Border._top && c.y >= _Border._bottom)
	{
		if (c.x >= _Border._left
			&& c.x <= _Border._right)
		{
			float val = c.y + r;
			if(val >= _Border._top)
			{
				return true;
			}

			val = c.y - r;
			if (val <= _Border._bottom)
			{
				return true;
			}
		}

		Vector2 v = GV(_Border._left, _Border._top);
		float vd = VD(c, v);
		if (vd <= r)
		{
			return true;
		}

		v = GV(_Border._right, _Border._top);
		if (VD(c, v) <= r)
		{
			return true;
		}

		v = GV(_Border._left, _Border._bottom);
		if (VD(c, v) <= r)
		{
			return true;
		}

		v = GV(_Border._right, _Border._bottom);
		if (VD(c, v) <= r)
		{
			return true;
		}
	}
	else
	{
		float v = c.x + r;
		if (v >= _Border._left
			&& v <= _Border._right)
		{
			return true;
		}

		v = c.x - r;
		if (v >= _Border._left
			&& v <= _Border._right)
		{
			return true;
		}
	}

	return false;
}

int Pipe::onReleaseScene()
{
	if (_top != NULL)
	{
		removeNode(_top);
	}
	
	if(_bottom != NULL)
	{
		removeNode(_bottom);
	}

	return 0;
}

float Pipe::getBorder()
{
	return _Border._right;
}

bool Pipe::checkOutLeft()
{
	if (_top != NULL)
	{
		return _top->checkOutLeft();
	}
	else if (_bottom != NULL)
	{
		return _bottom->checkOutLeft();
	}

	return false;
}

bool Pipe::checkOutRight()
{
	if (_top != NULL)
	{
		return _top->checkOutRight();
	}
	else if (_bottom != NULL)
	{
		return _bottom->checkOutRight();
	}

	return false;
}