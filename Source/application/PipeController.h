#ifndef PipeControllerH
#define PipeControllerH

#include "Core/SharedPtr.h"

#include "Scene/MeshNode.h"
#include "Pipe.h"

#include <deque>

using namespace std;

class PipeController: public SceneNode
{
public:
	PipeController();
	virtual int onDrawFrame(double deltaTime);
	shared_ptr<Pipe> getTargetPipe(float border);
	virtual int onReleaseScene();

private:
	void generatePipe();

	deque<shared_ptr<Pipe>> _PipeList;
	float _offset;
};

#endif
