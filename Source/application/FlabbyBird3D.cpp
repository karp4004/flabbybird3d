#include "FlabbyBird3D.h"

#include "Scene/Camera.h"
#include "Scene/MeshNode.h"

#include "Bird.h"
#include "Pipe.h"
#include "Core/FileManager.h"
#include "Platform/Platform.h"

int FlabbyBird3D::onCreateScene()
{
	Platform::instance().Logger().LogInfo(__FILE__, "onCreateScene:%d", 1);

	ApplicationFrame::onCreateScene();

	if (_Root != NULL)
	{
		Matrix4 l = GV(-4.0f, 100.0f, -6.0f);
		shared_ptr<LightNode> _Light = _Root->addLight(1, l);

		Vector3 e = GV(0.f, 0.f, 0);
		Vector3 t = GV(0, 0, 0);
		Vector3 u = GV(0, 1, 0);
		shared_ptr<Camera> _Camera = _Root->addCamera(e, t, u, 1000.0f);
		
		if (_Camera != NULL && _Light != NULL)
		{
			_Camera->setCameraMode(kCamFirst);

			shared_ptr<PipeController> ctrl = generatePipeController();
			if (ctrl != NULL)
			{
				_Bird = generateBird(ctrl);
				if (_Bird != NULL)
				{
					Vector3 r = GV(0, -90, 0);
					_Bird->Rotate(r);
					_Bird->GrabCameraAndLight(_Light, _Camera);
				}
			}
		}
	}

	Platform::instance().Interop().SceneCreated();

	return 0;
}

void FlabbyBird3D::onTouchScreen()
{
	if (_Bird != NULL)
	{
		_Bird->Fly();
	}
}

shared_ptr<Pipe> FlabbyBird3D::generatePipe(float offsetx, float offsety)
{
	EXCEPTION_TRY

	shared_ptr<Pipe> ret(new Pipe(offsetx, offsety));
	if (_Root != NULL)
	{
		_Root->AddChild(ret);

		Platform::instance().Logger().LogInfo(__FILE__, "_Root:%d", _Root->ChildCount());

		return ret;
	}

	EXCEPTION_CATCH

	return NULL;
}

shared_ptr<Bird> FlabbyBird3D::generateBird(shared_ptr<PipeController> c)
{
	EXCEPTION_TRY

	shared_ptr<Mesh> mesh = Platform::instance().ResourceProvider().GetMesh("boy.glrd");
	if (mesh)
	{
		shared_ptr<Shader> sh = Platform::instance().ResourceProvider().GetShader("skeletal");
		if (sh != NULL)
		{
			shared_ptr<Bird> meshNode(new Bird(mesh, sh, c, matrixTranslate(0, 0, 0)));
			Vector3 s = GV(1, 0.2, 1);
			meshNode->Scale(s);

			if (_Root != NULL)
			{
				_Root->AddChild(meshNode);
				return meshNode;
			}
		}
	}
	else
	{
		Platform::instance().Logger().LogInfo(__FILE__, "%s:no mesh", "boy.glrd");
	}

	EXCEPTION_CATCH

	return NULL;
}

shared_ptr<PipeController> FlabbyBird3D::generatePipeController()
{
	EXCEPTION_TRY
	
	shared_ptr<PipeController> ret(new PipeController());
		
	if (_Root != NULL)
	{
		_Root->AddChild(ret);
		return ret;
	}
	
	EXCEPTION_CATCH

	return NULL;
}

void FlabbyBird3D::releasePipe(shared_ptr<Pipe> p)
{
	if (_Root != NULL)
	{
		_Root->removeNode(p);

		Platform::instance().Logger().LogInfo(__FILE__, "_Root:%d", _Root->ChildCount());
	}
}