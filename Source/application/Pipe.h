#ifndef PipeH
#define PipeH

#include "Core/SharedPtr.h"

#include "Scene/MeshNode.h"

using namespace std;

class Pipe: public SceneNode
{
public:
	Pipe(float offsetx, float offsety);
	virtual int onReleaseScene();
	float getBorder();
	bool intersectCircle(Vector2& c, float r);
	bool checkOutLeft();
	bool checkOutRight();

private:
	shared_ptr<MeshNode> _top;
	shared_ptr<MeshNode> _bottom;

	struct Border
	{
		float _left, _right, _top, _bottom;
	};

	Border _Border;
};

#endif
