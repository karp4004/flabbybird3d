#ifndef QuaternionH
#define QuaternionH

#include "Math/Matrix4.h"

class Quaternion
{
public:
	Quaternion();
	Quaternion(float x, float y, float z, float w);
	int ToRotationMatrix(Matrix4& m);

	bool operator== (const Quaternion& o) const
	{
		return x == o.x && y == o.y && z == o.z && w == o.w;
	}

	bool operator!= (const Quaternion& o) const
	{
		return !(*this == o);
	}

	float w,x,y,z;
};


#endif
