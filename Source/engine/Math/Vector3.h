#ifndef Vector3H
#define Vector3H

#include <string>
#include <math.h>
#include <memory>

using namespace std;

class Vector4;

struct Vector3
{
	float x,y,z;

	static Vector3 GV(float x, float y, float z)
	{
		Vector3 res;

		res.x = x;
		res.y = y;
		res.z = z;

		return res;
	}

	bool operator== (const Vector3& other) const {
		return x == other.x && y == other.y && z == other.z;
	}

	bool operator!= (const Vector3& other) const {
		return x != other.x || y != other.y || z != other.z;
	}

	Vector3 operator*(const Vector3& other) const {
		return GV(x*other.x, y*other.y, z*other.z);
	}

	Vector3 getRounded(int step);
};


Vector3 NewVec3(Vector4& v4);

float VectorLength(Vector3& vec);
float VL(Vector3& vec);
Vector3 GV(float x, float y, float z);
Vector3 operator/(Vector3 vec1, float scalar);
Vector3 operator*(Vector3 vec1, float scalar);
Vector3 operator-(Vector3 vec1, Vector3 vec2);
Vector3 operator-(Vector3 vec1);
Vector3 operator+(Vector3 vec1, Vector3 vec2);
Vector3 normalize(Vector3& vec);
float dot(Vector3& vec1, Vector3& vec2);
Vector3 cross(Vector3& vec1, Vector3& vec2);
float VA(Vector3& vec1, Vector3& vec2);

#endif
