#ifndef BBoxH
#define BBoxH

#include "Math/Vector3.h"

using namespace std;

class BBox
{
public:
	BBox();
	BBox(float w,float h,float l);
	int BBoxMerge(BBox& second);

	float width;
	float height;
	float length;
};

#endif
