#include "math.h"
#include "Vector3.h"
#include "Math/Vector4.h"
#include "Math/GEMath.h"
#include "Platform/Platform.h"

Vector3 NewVec3(Vector4& v4)
{
	Vector3 v3;
	v3.x = v4.x;
	v3.y = v4.y;
	v3.z = v4.z;

	return v3;
}

Vector3 GV(float x, float y, float z)
{
	Vector3 res;

	res.x = x;
	res.y = y;
	res.z = z;

	return res;
}

float VectorLength(Vector3& vec)
{
	float length = vec.x*vec.x + vec.y*vec.y + vec.z*vec.z;
	length = sqrt(length);

	return length;
}

float VL(Vector3& vec)
{
	return VectorLength(vec);
}

Vector3 operator/(Vector3 vec1, float scalar)
{
	Vector3 res;

	res.x = vec1.x/scalar;
	res.y = vec1.y/scalar;
	res.z = vec1.z/scalar;

	return res;
}

Vector3 operator*(Vector3 vec1, float scalar)
{
	Vector3 res;

	res.x = vec1.x*scalar;
	res.y = vec1.y*scalar;
	res.z = vec1.z*scalar;

	return res;
}

Vector3 operator-(Vector3 vec1)
{
	vec1.x = -vec1.x;
	vec1.y = -vec1.y;
	vec1.z = -vec1.z;

	return vec1;
}

Vector3 operator-(Vector3 vec1, Vector3 vec2)
{
	Vector3 res;

	res.x = vec1.x - vec2.x;
	res.y = vec1.y - vec2.y;
	res.z = vec1.z - vec2.z;

	return res;
}

Vector3 operator+(Vector3 vec1, Vector3 vec2)
{
	Vector3 res;

	res.x = vec1.x + vec2.x;
	res.y = vec1.y + vec2.y;
	res.z = vec1.z + vec2.z;

	return res;
}

Vector3 normalize(Vector3& vec)
{
	float length = VectorLength(vec);
	vec = vec / length;
	return vec;
}

float dot(Vector3& vec1, Vector3& vec2)
{
	float res;
	res = vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z;
	return res;
}

Vector3 cross(Vector3& vec1, Vector3& vec2)
{
	Vector3 res;

	res.x = vec1.y*vec2.z - vec1.z*vec2.y;
	res.y = vec1.z*vec2.x - vec1.x*vec2.z;
	res.z = vec1.x*vec2.y - vec1.y*vec2.x;

	return res;
}

float VA(Vector3& vec1, Vector3& vec2)
{
	float a_mag = VL(vec1);
	float b_mag = VL(vec2);
	float ab_dot = dot(vec1, vec2);
	float c = ab_dot / (a_mag * b_mag);

	// clamp d to from going beyond +/- 1 as acos(+1/-1) results in infinity
//	if (c > 1.0f) {
//	    c = 1.0;
//	} else if (c < -1.0) {
//	    c = -1.0;
//	}
//
//	c = acos(c);
//	c *= k180PI;


	c = atan2(vec1.x*vec2.z - vec2.x*vec1.z, vec1.x*vec2.x + vec1.z*vec2.z);
//	c = atan2(VL(cross(vec1, vec2)), dot(vec1, vec2));

		c *= k180PI;

	return c;
}

Vector3 Vector3::getRounded(int step)
{
	int xi = (int)roundf(x);
	int yi = (int)roundf(y);
	int zi = (int)roundf(z);

	xi = (xi / step) * step;
	zi = (zi / step) * step;

	return GV(xi, yi, zi);
}
