#include "Math/BSphere.h"

#include "Platform/Platform.h"

BSphere::BSphere()
:radius(0.)
,center(GV(0.,0.,0.))
{
}

BSphere::BSphere(Vector3& c,float r)
:center(c)
,radius(r)
{
}

int BSphere::BSphereMerge(BSphere& second)
{
	if(radius>0)
	{
		Vector3 distance = second.center - center;
		Vector3 dHalf = distance/2;
		center = center + dHalf;
		radius = VectorLength(distance);
	}
	else
	{
		*this = second;
	}

	return 0;
}
