#ifndef Vector4H
#define Vector4H

class Vector3;

class Vector4
{
public:
	Vector4();
	Vector4(Vector3& v3);

	float x,y,z,w;
};

Vector4 GV(float x, float y, float z, float w);

#endif
