#ifndef BSphereH
#define BSphereH

#include "Vector3.h"

using namespace std;

class BSphere
{
public:
	BSphere();
	BSphere(Vector3& c,float r);
	int BSphereMerge(BSphere& second);

	Vector3 center;
	float radius;
};


#endif
