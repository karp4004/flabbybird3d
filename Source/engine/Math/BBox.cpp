#include "Math/BBox.h"

#include <limits>

#include "Platform/Platform.h"

BBox::BBox():
width(0)
,height(0)
,length(0)
{
}

BBox::BBox(float w,float h,float l):
width(w)
,height(h)
,length(l)
{
}

int BBox::BBoxMerge(BBox& second)
{
	if(width < second.width)
	{
		width = second.width;
	}

	if(height < second.height)
	{
		height = second.height;
	}

	if(length < second.length)
	{
		length = second.length;
	}

	return 0;
}
