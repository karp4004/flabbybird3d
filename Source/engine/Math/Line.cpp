#include "Line.h"

#include <float.h>
#include <math.h>

Line3d::Line3d()
{
}

Line3d::Line3d(	Vector3& _p1, Vector3& _p2):
p1(_p1)
,p2(_p2)
{
}

float dist3D_Line_to_Line( Line3d& L1, Line3d& L2)
{
    Vector3   u = L1.p2 - L1.p1;
    Vector3   v = L2.p2 - L2.p1;
    Vector3   w = L1.p1 - L2.p1;
    float    a = dot(u,u);         // always >= 0
    float    b = dot(u,v);
    float    c = dot(v,v);         // always >= 0
    float    d = dot(u,w);
    float    e = dot(v,w);
    float    D = a*c - b*b;        // always >= 0
    float    sc, tc;

    // compute the line parameters of the two closest points
    if (D < FLT_MIN) {          // the lines are almost parallel
        sc = 0.0;
        tc = (b>c ? d/b : e/c);    // use the largest denominator
    }
    else {
        sc = (b*e - c*d) / D;
        tc = (a*e - b*d) / D;
    }

    // get the difference of the two closest points
    Vector3   dP = w + (u * sc) - (v * tc);  // =  L1(sc) - L2(tc)

    return VectorLength(dP);   // return the closest distance
}

float
dist3D_Segment_to_Segment( Line3d& S1, Line3d& S2)
{
    Vector3   u = S1.p2 - S1.p1;
    Vector3   v = S2.p2 - S2.p1;
    Vector3   w = S1.p1 - S2.p1;
    float    a = dot(u,u);         // always >= 0
    float    b = dot(u,v);
    float    c = dot(v,v);         // always >= 0
    float    d = dot(u,w);
    float    e = dot(v,w);
    float    D = a*c - b*b;        // always >= 0
    float    sc, sN, sD = D;       // sc = sN / sD, default sD = D >= 0
    float    tc, tN, tD = D;       // tc = tN / tD, default tD = D >= 0

    // compute the line parameters of the two closest points
    if (D < FLT_MIN) { // the lines are almost parallel
        sN = 0.0;         // force using point P0 on segment S1
        sD = 1.0;         // to prevent possible division by 0.0 later
        tN = e;
        tD = c;
    }
    else {                 // get the closest points on the infinite lines
        sN = (b*e - c*d);
        tN = (a*e - b*d);
        if (sN < 0.0) {        // sc < 0 => the s=0 edge is visible
            sN = 0.0;
            tN = e;
            tD = c;
        }
        else if (sN > sD) {  // sc > 1  => the s=1 edge is visible
            sN = sD;
            tN = e + b;
            tD = c;
        }
    }

    if (tN < 0.0) {            // tc < 0 => the t=0 edge is visible
        tN = 0.0;
        // recompute sc for this edge
        if (-d < 0.0)
            sN = 0.0;
        else if (-d > a)
            sN = sD;
        else {
            sN = -d;
            sD = a;
        }
    }
    else if (tN > tD) {      // tc > 1  => the t=1 edge is visible
        tN = tD;
        // recompute sc for this edge
        if ((-d + b) < 0.0)
            sN = 0;
        else if ((-d + b) > a)
            sN = sD;
        else {
            sN = (-d +  b);
            sD = a;
        }
    }
    // finally do the division to get sc and tc
    sc = (fabs(sN) < FLT_MIN ? 0.0 : sN / sD);
    tc = (fabs(tN) < FLT_MIN ? 0.0 : tN / tD);

    // get the difference of the two closest points
    Vector3   dP = w + (u * sc) - (v * tc);  // =  S1(sc) - S2(tc)

    return VectorLength(dP);   // return the closest distance
}
