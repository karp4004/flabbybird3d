#include "Matrix4.h"
#include "GEMath.h"

#include "stdlib.h"
#include "math.h"

#include "Core/File.h"
#include "Core/FileStream.h"
#include "Quaternion.h"

Vector4 VM(Matrix4 &m, Vector4 &v)
{
	return vectorMultiply(m, v);
}

int Matrix4::LoadFromMap(shared_ptr<FileStream> f)
{
	f->ReadMemory((char*)&v, sizeof(v));

	return 0;
}

Matrix4::Matrix4()
{
    v[0] = v[5] = v[10] = v[15] = 1.0;
    v[1] = v[2] = v[3] = v[4] = 0.0;
    v[6] = v[7] = v[8] = v[9] = 0.0;
    v[11] = v[12] = v[13] = v[14] = 0.0;
}

Matrix4::Matrix4(Vector3 translate)
{
    v[0] = v[5] = v[10] = v[15] = 1.0;
    v[1] = v[2] = v[3] = v[4] = 0.0;
    v[6] = v[7] = v[8] = v[9] = 0.0;
    v[11] = v[12] = v[13] = v[14] = 0.0;

	*this = translateM(*this, translate.x, translate.y, translate.z);
}

Matrix4 transpose(Matrix4& m)
{
	Matrix4 result;

	result.v[0] = m.v[0];
	result.v[1] = m.v[4];
	result.v[2] = m.v[8];
	result.v[3] = m.v[12];
	result.v[4] = m.v[1];
	result.v[5] = m.v[5];
	result.v[6] = m.v[9];
	result.v[7] = m.v[13];
	result.v[8] = m.v[2];
	result.v[9] = m.v[6];
	result.v[10] = m.v[10];
	result.v[11] = m.v[14];
	result.v[12] = m.v[3];
	result.v[13] = m.v[7];
	result.v[14] = m.v[11];
	result.v[15] = m.v[15];

	return result;
}

Matrix4 inverse(Matrix4& m)
{
	Matrix4 result;
	return result;
}

Matrix4& translateM(
		Matrix4& m,
		float x, float y, float z)
{
	for (int i=0 ; i<4 ; i++) {
		int mi = i;
		m.v[12 + mi] += m.v[mi] * x + m.v[4 + mi] * y + m.v[8 + mi] * z;
	}

	return m;
}

Matrix4 matrixIdentity()
{
	Matrix4 m;
    m.v[0] = m.v[5] = m.v[10] = m.v[15] = 1.0;
    m.v[1] = m.v[2] = m.v[3] = m.v[4] = 0.0;
    m.v[6] = m.v[7] = m.v[8] = m.v[9] = 0.0;
    m.v[11] = m.v[12] = m.v[13] = m.v[14] = 0.0;

    return m;
}

Matrix4 matrixMove(float x, float y, float z)
{
	Matrix4 m = matrixIdentity();

	m.v[12] = x;
	m.v[13] = y;
	m.v[14] = z;

	return m;
}

Matrix4 matrixTranslate(float x, float y, float z)
{
	Matrix4 m = matrixIdentity();

	translateM(m, x,y,z);

    return m;
}

Matrix4 matrixScale(float sx, float sy, float sz)
{
	Matrix4 m = matrixIdentity();

    // Scale slots.
    m.v[0] = sx;
    m.v[5] = sy;
    m.v[10] = sz;

    return m;
}

Matrix4 matrixRotateX(float degrees)
{
    float radians = degreesToRadians(degrees);

    Matrix4 m = matrixIdentity();

    // Rotate X formula.
    m.v[5] = cosf(radians);
    m.v[6] = -sinf(radians);
    m.v[9] = -m.v[6];
    m.v[10] = m.v[5];

    return m;
}

Matrix4 matrixRotateY(float degrees)
{
    float radians = degreesToRadians(degrees);

    Matrix4 m = matrixIdentity();

    // Rotate Y formula.
    m.v[0] = cosf(radians);
    m.v[2] = sinf(radians);
    m.v[8] = -m.v[2];
    m.v[10] = m.v[0];

    return m;
}

Matrix4 matrixRotateZ(float degrees)
{
    float radians = degreesToRadians(degrees);

    Matrix4 m = matrixIdentity();

    // Rotate Z formula.
    m.v[0] = cosf(radians);
    m.v[1] = sinf(radians);
    m.v[4] = -m.v[1];
    m.v[5] = m.v[0];

    return m;
}

Matrix4 matrixMultiply(Matrix4& m, Matrix4& m2)
{
	Matrix4 result;
    // Fisrt Column
    result.v[0] = m.v[0]*m2.v[0] + m.v[4]*m2.v[1] + m.v[8]*m2.v[2] + m.v[12]*m2.v[3];
    result.v[1] = m.v[1]*m2.v[0] + m.v[5]*m2.v[1] + m.v[9]*m2.v[2] + m.v[13]*m2.v[3];
    result.v[2] = m.v[2]*m2.v[0] + m.v[6]*m2.v[1] + m.v[10]*m2.v[2] + m.v[14]*m2.v[3];
    result.v[3] = m.v[3]*m2.v[0] + m.v[7]*m2.v[1] + m.v[11]*m2.v[2] + m.v[15]*m2.v[3];

    // Second Column
    result.v[4] = m.v[0]*m2.v[4] + m.v[4]*m2.v[5] + m.v[8]*m2.v[6] + m.v[12]*m2.v[7];
    result.v[5] = m.v[1]*m2.v[4] + m.v[5]*m2.v[5] + m.v[9]*m2.v[6] + m.v[13]*m2.v[7];
    result.v[6] = m.v[2]*m2.v[4] + m.v[6]*m2.v[5] + m.v[10]*m2.v[6] + m.v[14]*m2.v[7];
    result.v[7] = m.v[3]*m2.v[4] + m.v[7]*m2.v[5] + m.v[11]*m2.v[6] + m.v[15]*m2.v[7];

    // Third Column
    result.v[8] = m.v[0]*m2.v[8] + m.v[4]*m2.v[9] + m.v[8]*m2.v[10] + m.v[12]*m2.v[11];
    result.v[9] = m.v[1]*m2.v[8] + m.v[5]*m2.v[9] + m.v[9]*m2.v[10] + m.v[13]*m2.v[11];
    result.v[10] = m.v[2]*m2.v[8] + m.v[6]*m2.v[9] + m.v[10]*m2.v[10] + m.v[14]*m2.v[11];
    result.v[11] = m.v[3]*m2.v[8] + m.v[7]*m2.v[9] + m.v[11]*m2.v[10] + m.v[15]*m2.v[11];

    // Fourth Column
    result.v[12] = m.v[0]*m2.v[12] + m.v[4]*m2.v[13] + m.v[8]*m2.v[14] + m.v[12]*m2.v[15];
    result.v[13] = m.v[1]*m2.v[12] + m.v[5]*m2.v[13] + m.v[9]*m2.v[14] + m.v[13]*m2.v[15];
    result.v[14] = m.v[2]*m2.v[12] + m.v[6]*m2.v[13] + m.v[10]*m2.v[14] + m.v[14]*m2.v[15];
    result.v[15] = m.v[3]*m2.v[12] + m.v[7]*m2.v[13] + m.v[11]*m2.v[14] + m.v[15]*m2.v[15];

    return result;
}

Matrix4 MM(Matrix4& m, Matrix4& m2)
{
	return matrixMultiply(m, m2);
}

Matrix4 MI()
{
	return matrixIdentity();
}

Vector4 vectorMultiply(Matrix4& m, Vector4& v)
{
	Vector4 result;

	result.x = m.v[0]*v.x + m.v[4]*v.y + m.v[8]*v.z + m.v[12]*v.w;
	result.y = m.v[1]*v.x + m.v[5]*v.y + m.v[9]*v.z + m.v[13]*v.w;
	result.z = m.v[2]*v.x + m.v[6]*v.y + m.v[10]*v.z + m.v[14]*v.w;
	result.w = m.v[3]*v.x + m.v[7]*v.y + m.v[11]*v.z + m.v[15]*v.w;

	return result;
}

Matrix4 generateMatrix(Vector3& pos, Vector3& dir, Vector3& up)
{
	Vector3 side = cross(dir, up);
	side = normalize(side);

	up = cross(side, dir);
	up = normalize(up);

	Matrix4 m;

	m.v[0] = side.x;
	m.v[1] = up.x;
	m.v[2] = dir.x;
	m.v[3] = 0.0f;

	m.v[4] = side.y;
	m.v[5] = up.y;
	m.v[6] = dir.y;
	m.v[7] = 0.0f;

	m.v[8] = side.z;
	m.v[9] = up.z;
	m.v[10] = dir.z;
	m.v[11] = 0.0f;

	m.v[12] = pos.x;
	m.v[13] = pos.y;
	m.v[14] =  pos.z;
	m.v[15] = 1.0f;

	return m;//translateM(m, pos.x, pos.y, pos.z);
}

Matrix4 invertMatrix(Matrix4& m)
{
	// Invert a 4 x 4 matrix using Cramer's Rule

	// transpose matrix
	float src0 = m.v[0];
	float src4 = m.v[1];
	float src8 = m.v[2];
	float src12 = m.v[3];

	float src1 = m.v[4];
	float src5 = m.v[5];
	float src9 = m.v[6];
	float src13 = m.v[7];

	float src2 = m.v[8];
	float src6 = m.v[9];
	float src10 = m.v[10];
	float src14 = m.v[11];

	float src3 = m.v[12];
	float src7 = m.v[13];
	float src11 = m.v[14];
	float src15 = m.v[15];

	// calculate pairs for first 8 elements (cofactors)
	float atmp0 = src10 * src15;
	float atmp1 = src11 * src14;
	float atmp2 = src9 * src15;
	float atmp3 = src11 * src13;
	float atmp4 = src9 * src14;
	float atmp5 = src10 * src13;
	float atmp6 = src8 * src15;
	float atmp7 = src11 * src12;
	float atmp8 = src8 * src14;
	float atmp9 = src10 * src12;
	float atmp10 = src8 * src13;
	float atmp11 = src9 * src12;

	// calculate first 8 elements (cofactors)
	float dst0 = (atmp0 * src5 + atmp3 * src6 + atmp4 * src7)
			- (atmp1 * src5 + atmp2 * src6 + atmp5 * src7);
	float dst1 = (atmp1 * src4 + atmp6 * src6 + atmp9 * src7)
			- (atmp0 * src4 + atmp7 * src6 + atmp8 * src7);
	float dst2 = (atmp2 * src4 + atmp7 * src5 + atmp10 * src7)
			- (atmp3 * src4 + atmp6 * src5 + atmp11 * src7);
	float dst3 = (atmp5 * src4 + atmp8 * src5 + atmp11 * src6)
			- (atmp4 * src4 + atmp9 * src5 + atmp10 * src6);
	float dst4 = (atmp1 * src1 + atmp2 * src2 + atmp5 * src3)
			- (atmp0 * src1 + atmp3 * src2 + atmp4 * src3);
	float dst5 = (atmp0 * src0 + atmp7 * src2 + atmp8 * src3)
			- (atmp1 * src0 + atmp6 * src2 + atmp9 * src3);
	float dst6 = (atmp3 * src0 + atmp6 * src1 + atmp11 * src3)
			- (atmp2 * src0 + atmp7 * src1 + atmp10 * src3);
	float dst7 = (atmp4 * src0 + atmp9 * src1 + atmp10 * src2)
			- (atmp5 * src0 + atmp8 * src1 + atmp11 * src2);

	// calculate pairs for second 8 elements (cofactors)
	float btmp0 = src2 * src7;
	float btmp1 = src3 * src6;
	float btmp2 = src1 * src7;
	float btmp3 = src3 * src5;
	float btmp4 = src1 * src6;
	float btmp5 = src2 * src5;
	float btmp6 = src0 * src7;
	float btmp7 = src3 * src4;
	float btmp8 = src0 * src6;
	float btmp9 = src2 * src4;
	float btmp10 = src0 * src5;
	float btmp11 = src1 * src4;

	// calculate second 8 elements (cofactors)
	float dst8 = (btmp0 * src13 + btmp3 * src14 + btmp4 * src15)
			- (btmp1 * src13 + btmp2 * src14 + btmp5 * src15);
	float dst9 = (btmp1 * src12 + btmp6 * src14 + btmp9 * src15)
			- (btmp0 * src12 + btmp7 * src14 + btmp8 * src15);
	float dst10 = (btmp2 * src12 + btmp7 * src13 + btmp10 * src15)
			- (btmp3 * src12 + btmp6 * src13 + btmp11 * src15);
	float dst11 = (btmp5 * src12 + btmp8 * src13 + btmp11 * src14)
			- (btmp4 * src12 + btmp9 * src13 + btmp10 * src14);
	float dst12 = (btmp2 * src10 + btmp5 * src11 + btmp1 * src9)
			- (btmp4 * src11 + btmp0 * src9 + btmp3 * src10);
	float dst13 = (btmp8 * src11 + btmp0 * src8 + btmp7 * src10)
			- (btmp6 * src10 + btmp9 * src11 + btmp1 * src8);
	float dst14 = (btmp6 * src9 + btmp11 * src11 + btmp3 * src8)
			- (btmp10 * src11 + btmp2 * src8 + btmp7 * src9);
	float dst15 = (btmp10 * src10 + btmp4 * src8 + btmp9 * src9)
			- (btmp8 * src9 + btmp11 * src10 + btmp5 * src8);

	// calculate determinant
	float det = src0 * dst0 + src1 * dst1 + src2 * dst2 + src3
			* dst3;

	if (det == 0.0f) {
		return m;
	}

	Matrix4 mInv;
	// calculate matrix inverse
	float invdet = 1.0f / det;
	mInv.v[0] = dst0 * invdet;
	mInv.v[1] = dst1 * invdet;
	mInv.v[2] = dst2 * invdet;
	mInv.v[3] = dst3 * invdet;

	mInv.v[4] = dst4 * invdet;
	mInv.v[5] = dst5 * invdet;
	mInv.v[6] = dst6 * invdet;
	mInv.v[7] = dst7 * invdet;

	mInv.v[8] = dst8 * invdet;
	mInv.v[9] = dst9 * invdet;
	mInv.v[10] = dst10 * invdet;
	mInv.v[11] = dst11 * invdet;

	mInv.v[12] = dst12 * invdet;
	mInv.v[13] = dst13 * invdet;
	mInv.v[14] = dst14 * invdet;
	mInv.v[15] = dst15 * invdet;

	return mInv;
}

Matrix4 Matrix4x4Compose(const Vector3& scaling, Quaternion* rotation, const Vector3& position) {
	Matrix4 r;

	Matrix4 m;
	rotation->ToRotationMatrix(m);

	r.v[0] = m.v[0] * scaling.x;
	r.v[1] = m.v[1] * scaling.x;
	r.v[2] = m.v[2] * scaling.x;
	r.v[3] = position.x;

	r.v[4] = m.v[4] * scaling.y;
	r.v[5] = m.v[5] * scaling.y;
	r.v[6] = m.v[6] * scaling.y;
	r.v[7] = position.y;

	r.v[8] = m.v[8] * scaling.z;
	r.v[9] = m.v[9] * scaling.z;
	r.v[10] = m.v[10] * scaling.z;
	r.v[11] = position.z;

	r.v[12] = 0.;
	r.v[13] = 0.;
	r.v[14] = 0.;
	r.v[15] = 1.0;

	return r;
}
