#ifndef Matrix4H
#define Matrix4H

#include "Vector3.h"
#include "Vector4.h"
#include "Core/SharedPtr.h"

using namespace std;

class File;
class FileStream;
class Quaternion;

class Matrix4
{
public:
	Matrix4();
	Matrix4(Vector3 translate);

	int LoadFromMap(shared_ptr<FileStream> f);

	float v[16];
};

Matrix4 matrixIdentity();
Matrix4 MI();
Matrix4 matrixTranslate(float x, float y, float z);
Matrix4 matrixMove(float x, float y, float z);
Matrix4 matrixScale(float sx, float sy, float sz);
Matrix4 matrixRotateX(float degrees);
Matrix4 matrixRotateY(float degrees);
Matrix4 matrixRotateZ(float degrees);
Matrix4 matrixMultiply(Matrix4& m, Matrix4& m2);
Matrix4 MM(Matrix4& m, Matrix4& m2);
Vector4 vectorMultiply(Matrix4 &m, Vector4 &v);
Vector4 VM(Matrix4 &m, Vector4 &v);
Matrix4 generateMatrix(Vector3& pos, Vector3& dir, Vector3& up);
Matrix4& translateM(Matrix4& m, float x, float y, float z);
Matrix4 transpose(Matrix4& m);
Matrix4 inverse(Matrix4& m);
Matrix4 Matrix4x4Compose(const Vector3& scaling, Quaternion* rotation, const Vector3& position);

#endif
