#include "GEMath.h"
#include <algorithm>

#include "Platform/Platform.h"

Matrix4 frustumM(float left, float right, float bottom, float top, float near, float far)
{
	float r_width  = 1.0f / (right - left);
	float r_height = 1.0f / (top - bottom);
	float r_depth  = 1.0f / (near - far);
	float x = 2.0f * (near * r_width);
	float y = 2.0f * (near * r_height);
	float A = 2.0f * ((right + left) * r_width);
	float B = (top + bottom) * r_height;
	float C = (far + near) * r_depth;
	float D = 2.0f * (far * near * r_depth);

	Matrix4 m;

	m.v[0] = x;
	m.v[5] = y;
	m.v[8] = A;
	m.v[9] = B;
	m.v[10] = C;
	m.v[14] = D;
	m.v[11] = -1.0f;
	m.v[1] = 0.0f;
	m.v[2] = 0.0f;
	m.v[3] = 0.0f;
	m.v[4] = 0.0f;
	m.v[6] = 0.0f;
	m.v[7] = 0.0f;
	m.v[12] = 0.0f;
	m.v[13] = 0.0f;
	m.v[15] = 0.0f;

	return m;
}

Matrix4 getPerspective(float fovy, float aspect, float nearZ, float farZ)
{
   float frustumW, frustumH;

   frustumH = tanf( fovy / 360.0f * PI ) * nearZ;
   frustumW = frustumH * aspect;

   return frustumM( -frustumW, frustumW, -frustumH, frustumH, nearZ, farZ );
}

Matrix4 getOrtho(float left, float right, float bottom, float top, float near, float far)
{
    float       deltaX = right - left;
    float       deltaY = top - bottom;
    float       deltaZ = far - near;

    Matrix4    ortho = matrixIdentity();

    if ( (deltaX != 0.0f) || (deltaY != 0.0f) || (deltaZ != 0.0f) )
    {
        ortho.v[0] = 2.0f / deltaX;
        ortho.v[12] = -(right + left) / deltaX;
        ortho.v[5] = 2.0f / deltaY;
        ortho.v[13] = -(top + bottom) / deltaY;
        ortho.v[10] = -2.0f / deltaZ;
        ortho.v[14] = -(near + far) / deltaZ;
    }

    return ortho;
}

Matrix4 getLookAtM(Vector3& eye, Vector3& center, Vector3& up)
{
	Vector3 forward = center - eye;

	forward = normalize(forward);
	Vector3 side = cross(forward, up);
	side = normalize(side);
	up = cross(side, forward);

	Matrix4 m;
	m.v[0] = side.x;
	m.v[1] = up.x;
	m.v[2] = -forward.x;
	m.v[3] = 0.0f;

	m.v[4] = side.y;
	m.v[5] = up.y;
	m.v[6] = -forward.y;
	m.v[7] = 0.0f;

	m.v[8] = side.z;
	m.v[9] = up.z;
	m.v[10] = -forward.z;
	m.v[11] = 0.0f;

	m.v[12] = 0.0f;
	m.v[13] = 0.0f;
	m.v[14] = 0.0f;
	m.v[15] = 1.0f;

	m = translateM(m, -eye.x, -eye.y, -eye.z);

	return m;
}

Matrix4 getLookAtM2(Vector3& eye, Vector3& center, Vector3& up)
{
	Vector3 forward, side;
   Matrix4 matrix2, resultMatrix[16];
   //------------------
   forward.x = center.x - eye.x;
   forward.y = center.y  - eye.y;
   forward.z = center.z  - eye.z;
   forward = normalize(forward);
   //------------------
   //Side = forward x up
   side = cross(forward, up);
   side = normalize(side);
   //------------------
   //Recompute up as: up = side x forward
   up = cross(side, forward);
   //------------------
   matrix2.v[0] = side.x;
   matrix2.v[4] = side.y;
   matrix2.v[8] = side.z;
   matrix2.v[12] = 0.0;
   //------------------
   matrix2.v[1] = up.x;
   matrix2.v[5] = up.y;
   matrix2.v[9] = up.z;
   matrix2.v[13] = 0.0;
   //------------------
   matrix2.v[2] = -forward.x;
   matrix2.v[6] = -forward.y;
   matrix2.v[10] = -forward.z;
   matrix2.v[14] = 0.0;
   //------------------
   matrix2.v[3] = matrix2.v[7] = matrix2.v[11] = 0.0;
   matrix2.v[15] = 1.0;


   Matrix4 m = matrixIdentity();
   m = matrixMultiply(m, matrix2);

   m = translateM(m, -eye.x, -eye.y, -eye.z);

	return m;
}

BSphere BSphereFromBBox(BBox& bbox)
{
	BSphere ret;
	ret.radius = std::max(std::max(bbox.width, bbox.height), bbox.length)/2.;
	return ret;
}

bool intersectPlane(Plane& plane, Ray& ray, Vector3& out)
{
	plane.normal = normalize(plane.normal);

	Vector3 inclined = GV(0, 0, 0) - ray.Start;
	float d = dot(plane.normal, inclined);
	Vector3 rayvec = ray.End - ray.Start;
	float e = dot(plane.normal, rayvec);

	if (e != 0)
	{
		out = ray.Start + rayvec * d / e;

		return true;
	}

	return false;
}

void multMatrixVecd(const float matrix[16], const float in[4],
	float out[4])
{
	int i;

	for (i = 0; i<4; i++) {
		out[i] =
			in[0] * matrix[0 * 4 + i] +
			in[1] * matrix[1 * 4 + i] +
			in[2] * matrix[2 * 4 + i] +
			in[3] * matrix[3 * 4 + i];
	}
}

void multMatricesd(const float a[16], const float b[16],
	float r[16])
{
	int i, j;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			r[i * 4 + j] =
				a[i * 4 + 0] * b[0 * 4 + j] +
				a[i * 4 + 1] * b[1 * 4 + j] +
				a[i * 4 + 2] * b[2 * 4 + j] +
				a[i * 4 + 3] * b[3 * 4 + j];
		}
	}
}

int invertMatrixd(const float m[16], float invOut[16])
{
	float inv[16], det;
	int i;

	inv[0] = m[5] * m[10] * m[15] - m[5] * m[11] * m[14] - m[9] * m[6] * m[15]
		+ m[9] * m[7] * m[14] + m[13] * m[6] * m[11] - m[13] * m[7] * m[10];
	inv[4] = -m[4] * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6] * m[15]
		- m[8] * m[7] * m[14] - m[12] * m[6] * m[11] + m[12] * m[7] * m[10];
	inv[8] = m[4] * m[9] * m[15] - m[4] * m[11] * m[13] - m[8] * m[5] * m[15]
		+ m[8] * m[7] * m[13] + m[12] * m[5] * m[11] - m[12] * m[7] * m[9];
	inv[12] = -m[4] * m[9] * m[14] + m[4] * m[10] * m[13] + m[8] * m[5] * m[14]
		- m[8] * m[6] * m[13] - m[12] * m[5] * m[10] + m[12] * m[6] * m[9];
	inv[1] = -m[1] * m[10] * m[15] + m[1] * m[11] * m[14] + m[9] * m[2] * m[15]
		- m[9] * m[3] * m[14] - m[13] * m[2] * m[11] + m[13] * m[3] * m[10];
	inv[5] = m[0] * m[10] * m[15] - m[0] * m[11] * m[14] - m[8] * m[2] * m[15]
		+ m[8] * m[3] * m[14] + m[12] * m[2] * m[11] - m[12] * m[3] * m[10];
	inv[9] = -m[0] * m[9] * m[15] + m[0] * m[11] * m[13] + m[8] * m[1] * m[15]
		- m[8] * m[3] * m[13] - m[12] * m[1] * m[11] + m[12] * m[3] * m[9];
	inv[13] = m[0] * m[9] * m[14] - m[0] * m[10] * m[13] - m[8] * m[1] * m[14]
		+ m[8] * m[2] * m[13] + m[12] * m[1] * m[10] - m[12] * m[2] * m[9];
	inv[2] = m[1] * m[6] * m[15] - m[1] * m[7] * m[14] - m[5] * m[2] * m[15]
		+ m[5] * m[3] * m[14] + m[13] * m[2] * m[7] - m[13] * m[3] * m[6];
	inv[6] = -m[0] * m[6] * m[15] + m[0] * m[7] * m[14] + m[4] * m[2] * m[15]
		- m[4] * m[3] * m[14] - m[12] * m[2] * m[7] + m[12] * m[3] * m[6];
	inv[10] = m[0] * m[5] * m[15] - m[0] * m[7] * m[13] - m[4] * m[1] * m[15]
		+ m[4] * m[3] * m[13] + m[12] * m[1] * m[7] - m[12] * m[3] * m[5];
	inv[14] = -m[0] * m[5] * m[14] + m[0] * m[6] * m[13] + m[4] * m[1] * m[14]
		- m[4] * m[2] * m[13] - m[12] * m[1] * m[6] + m[12] * m[2] * m[5];
	inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2] * m[11]
		- m[5] * m[3] * m[10] - m[9] * m[2] * m[7] + m[9] * m[3] * m[6];
	inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2] * m[11]
		+ m[4] * m[3] * m[10] + m[8] * m[2] * m[7] - m[8] * m[3] * m[6];
	inv[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1] * m[11]
		- m[4] * m[3] * m[9] - m[8] * m[1] * m[7] + m[8] * m[3] * m[5];
	inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1] * m[10]
		+ m[4] * m[2] * m[9] + m[8] * m[1] * m[6] - m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];
	if (det == 0)
		return false;

	det = 1.0 / det;

	for (i = 0; i < 16; i++)
		invOut[i] = inv[i] * det;

	return true;
}

int
project(float objx, float objy, float objz,
	const float modelMatrix[16],
	const float projMatrix[16],
	const int viewport[4],
	float *winx, float *winy, float *winz)
{
	float in[4];
	float out[4];

	in[0] = objx;
	in[1] = objy;
	in[2] = objz;
	in[3] = 1.0;
	multMatrixVecd(modelMatrix, in, out);
	multMatrixVecd(projMatrix, out, in);
	if (in[3] == 0.0) return(false);
	in[0] /= in[3];
	in[1] /= in[3];
	in[2] /= in[3];
	/* Map x, y and z to range 0-1 */
	in[0] = in[0] * 0.5 + 0.5;
	in[1] = in[1] * 0.5 + 0.5;
	in[2] = in[2] * 0.5 + 0.5;

	/* Map x,y to viewport */
	in[0] = in[0] * viewport[2] + viewport[0];
	in[1] = in[1] * viewport[3] + viewport[1];

	*winx = in[0];
	*winy = in[1];
	*winz = in[2];
	return(true);
}

int
unproject(float winx, float winy, float winz,
	const float modelMatrix[16],
	const float projMatrix[16],
	const int viewport[4],
	float *objx, float *objy, float *objz)
{
	float finalMatrix[16];
	float in[4];
	float out[4];

	multMatricesd(modelMatrix, projMatrix, finalMatrix);
	if (!invertMatrixd(finalMatrix, finalMatrix)) return(false);

	in[0] = winx;
	in[1] = winy;
	in[2] = winz;
	in[3] = 1.0;

	/* Map x and y from window coordinates */
	in[0] = (in[0] - viewport[0]) / viewport[2];
	in[1] = (in[1] - viewport[1]) / viewport[3];

	/* Map to range -1 to 1 */
	in[0] = in[0] * 2 - 1;
	in[1] = in[1] * 2 - 1;
	in[2] = in[2] * 2 - 1;

	multMatrixVecd(finalMatrix, in, out);
	if (out[3] == 0.0) return(false);
	out[0] /= out[3];
	out[1] /= out[3];
	out[2] /= out[3];
	*objx = out[0];
	*objy = out[1];
	*objz = out[2];
	return(true);
}