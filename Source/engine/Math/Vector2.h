#ifndef Vector2H
#define Vector2H

#include <string>

using namespace std;

struct Vector2
{
	float x,y;
};

Vector2 GV(float x, float y);
float VL(Vector2& vec);
Vector2 operator-(Vector2& vec1, Vector2& vec2);
Vector2 operator+(Vector2& vec1, Vector2& vec2);
Vector2 normalize(Vector2& vec);
float VectorLength(Vector2& vec);
Vector2 operator/(Vector2& vec1, float scalar);
Vector2 operator*(Vector2& vec1, float scalar);
float VD(Vector2& vec1, Vector2& vec2);

#endif
