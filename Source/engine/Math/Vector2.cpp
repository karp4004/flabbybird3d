#include "Vector2.h"

#include "math.h"

#include "Platform/Platform.h"

float VL(Vector2& vec)
{
	return VectorLength(vec);
}

Vector2 GV(float x, float y)
{
	Vector2 ret;
	ret.x = x;
	ret.y = y;
	return ret;
}

Vector2 operator-(Vector2& vec1, Vector2& vec2)
{
	Vector2 res;

	res.x = vec1.x - vec2.x;
	res.y = vec1.y - vec2.y;

	return res;
}

Vector2 operator+(Vector2& vec1, Vector2& vec2)
{
	Vector2 res;

	res.x = vec1.x + vec2.x;
	res.y = vec1.y + vec2.y;

	return res;
}

Vector2 normalize(Vector2& vec)
{
	float length = VectorLength(vec);
	vec = vec / length;
	return vec;
}

float VectorLength(Vector2& vec)
{
	float length = vec.x*vec.x + vec.y*vec.y;
	length = sqrt(length);

	return length;
}

Vector2 operator/(Vector2& vec1, float scalar)
{
	Vector2 res;

	res.x = vec1.x/scalar;
	res.y = vec1.y/scalar;

	return res;
}

Vector2 operator*(Vector2& vec1, float scalar)
{
	Vector2 res;

	res.x = vec1.x*scalar;
	res.y = vec1.y*scalar;

	return res;
}

float VD(Vector2& vec1, Vector2& vec2) {
	Vector2 ret = vec1 - vec2;
	return VL(ret);
}
