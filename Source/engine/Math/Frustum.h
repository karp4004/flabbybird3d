#ifndef FrustumH
#define FrustumH

#include <string>

using namespace std;

struct FPlane
{
	float A,B,C,D;
};

struct Frustum
{
	FPlane right;
	FPlane left;
	FPlane bottom;
	FPlane top;
	FPlane mNear;
	FPlane mFar;
};


#endif
