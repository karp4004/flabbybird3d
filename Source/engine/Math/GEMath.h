#ifndef GEMathH
#define GEMathH

#include "math.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Vector4.h"
#include "Matrix4.h"
#include "BBox.h"
#include "BSphere.h"
#include "Math/Plane.h"
#include "Math/Ray.h"

#define PI 3.1415926535897932384626433832795f
// Pre-calculated value of PI / 180.
#define kPI180   0.017453

// Pre-calculated value of 180 / PI.
#define k180PI  57.295780

// Converts degrees to radians.
#define degreesToRadians(x) (x * kPI180)

// Converts radians to degrees.
#define radiansToDegrees(x) (x * k180PI)

Matrix4 getLookAtM2(Vector3& eye, Vector3& center, Vector3& up);
Matrix4 getLookAtM(Vector3& eye, Vector3& center, Vector3& up);
Matrix4 getPerspective(float fovy, float aspect, float nearZ, float farZ);
Matrix4 getOrtho(float left, float right, float bottom, float top, float nearZ, float farZ);
Matrix4 frustumM(float left, float right, float bottom, float top, float near, float far);
BSphere BSphereFromBBox(BBox& bbox);
bool intersectPlane(Plane& plane, Ray& ray, Vector3& out);
void multMatrixVecd(const float matrix[16], const float in[4],
	float out[4]);

void multMatricesd(const float a[16], const float b[16],
	float r[16]);

int invertMatrixd(const float m[16], float invOut[16]);

int
project(float objx, float objy, float objz,
	const float modelMatrix[16],
	const float projMatrix[16],
	const int viewport[4],
	float *winx, float *winy, float *winz);

int
unproject(float winx, float winy, float winz,
	const float modelMatrix[16],
	const float projMatrix[16],
	const int viewport[4],
	float *objx, float *objy, float *objz);

#endif
