#ifndef LINE_H
#define LINE_H

#include "Math/Vector3.h"

struct Line3d
{
	Line3d();
	Line3d(	Vector3& p1, Vector3& p2);

	Vector3 p1;
	Vector3 p2;
};

float dist3D_Line_to_Line( Line3d& L1, Line3d& L2);
float dist3D_Segment_to_Segment( Line3d& S1, Line3d& S2);

#endif
