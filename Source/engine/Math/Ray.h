#ifndef RAY_H
#define RAY_H

#include "Math/Vector3.h"

struct Ray
{
	Ray(Vector3& p1, Vector3& p2);

	Vector3 Start;
	Vector3 End;
};

#endif
