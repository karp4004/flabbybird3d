#ifndef Plane_H
#define Plane_H

#include "Math/Vector3.h"

struct Plane
{
	Plane(	Vector3& n, Vector3& b);

	Vector3 normal;
	Vector3 normalBase;
};

#endif
