#include "Math/Quaternion.h"
#include "Platform/Platform.h"

Quaternion::Quaternion()
:x(0.)
,y(0.)
,z(0.)
,w(1.)
{
}

Quaternion::Quaternion(float p1, float p2, float p3, float p4)
:x(p1)
, y(p2)
, z(p3)
, w(p4)
{
}

int Quaternion::ToRotationMatrix(Matrix4& m)
{
	m.v[0] = 1.0 - 2.0 * (y * y + z * z);
	m.v[1] = 2.0 * (x * y - z * w);
	m.v[2] = 2.0 * (x * z + y * w);
	m.v[4] = 2.0 * (x * y + z * w);
	m.v[5] = 1.0 - 2.0 * (x * x + z * z);
	m.v[6] = 2.0 * (y * z - x * w);
	m.v[8] = 2.0 * (x * z - y * w);
	m.v[9] = 2.0 * (y * z + x * w);
	m.v[10] = 1.0 - 2.0 * (x * x + y * y);

	return 0;
}
