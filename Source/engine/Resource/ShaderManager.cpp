#include "ShaderManager.h"

#include "Resource/Shader.h"
#include "Platform/Platform.h"

ShaderManager::ShaderManager()
:ResourceManager()
{
}

int ShaderManager::LoadFromStream(shared_ptr<FileStream> fs, string shaderFile)
{
	EXCEPTION_TRY

	if(GetResource(shaderFile))
	{
		return -1;
	}

	int dot = shaderFile.find(".");

	if(dot != string::npos)
	{
		string shaderName = shaderFile.substr(0, dot);

		shared_ptr<Shader> shader = static_pointer_cast<Shader>(GetResource(shaderName));
		if(!shader)
		{
			shader.reset(Platform::instance().RenderFactory().CreateShader(shaderName));
			AddResource(shader);
		}

		if(shaderFile.substr(dot, shaderFile.size() - dot) == ".vertex")
		{
			shader->SetVertexSourceCode(fs->ReadString());
		}
		else if(shaderFile.substr(dot, shaderFile.size() - dot) == ".fragment")
		{
			shader->SetFragmentSourceCode(fs->ReadString());
		}
	}

	EXCEPTION_CATCH

	return 0;
}
