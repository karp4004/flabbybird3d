#include "TextureManager.h"
#include "Platform\Platform.h"

#include "Math/GEMath.h"
#include "Scene/Vertex.h"

TextureManager::TextureManager()
:ResourceManager()
{
}

shared_ptr<Texture> TextureManager::parseTexture(shared_ptr<FileStream> fs, const char* textureName)
{
	shared_ptr<Texture> deffuse;

	PixelFormat bpp = (PixelFormat)fs->ReadInt();
	int width = fs->ReadInt();
	int height = fs->ReadInt();
	PixelFormat format = (PixelFormat)fs->ReadInt();
	int data_size = fs->ReadInt();
	vector<unsigned char> data;
	data.resize(data_size);
	fs->ReadMemory(&data[0], data_size);

	return shared_ptr<Texture>(Platform::instance().RenderFactory().CreateTexture(textureName, bpp, width, height, format, data, GE_TEXTURE0, GE_TEXTURE2D));
}

int TextureManager::LoadFromStream(shared_ptr<FileStream> fs, string textureName)
{
	if(GetResource(textureName))
	{
		return -1;
	}

	shared_ptr<Texture> tex = parseTexture(fs, textureName.c_str());
	if(tex)
	{
		AddResource(tex);
	}

	return 0;
}
