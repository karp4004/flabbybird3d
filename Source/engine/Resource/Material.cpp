#include "Material.h"

Material::Material(string name, shared_ptr<Texture> deffuse, float alpha, aiTextureTypeDef t)
:Resource(name)
,mDeffuse(deffuse)
,mAlpha(alpha)
,type(t)
{
}

int Material::Allocate()
{
	if(mDeffuse != NULL)
	{
		mDeffuse->Allocate();
	}

	return 0;
}

int Material::Bind(unsigned int* deffuse, unsigned int* alpha)
{
	if(mDeffuse != NULL)
	{
		if(type == aiTextureTypeDef_DIFFUSE)
		{
			mDeffuse->Bind(deffuse, 0);
		}
	}

	return 0;
}

aiTextureTypeDef Material::getType()
{
	return type;
}