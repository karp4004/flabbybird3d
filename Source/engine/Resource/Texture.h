#ifndef TextureH
#define TextureH

#include "Core/Resource.h"
#include <vector>

using namespace std;

enum PixelFormat
{
	GE_RGB,
	GE_RGBA,
	GE_ALPHA,

	GE_COMPRESSED,

	//gles
	GE_ETC1_RGB8_OES,

	//gl
	GE_COMPRESSED_RGB_FXT1_3DFX,

	//shared
	GE_PALETTE4_RGB8_OES,
};

enum TextureUnit
{
	GE_TEXTURE0,
	GE_TEXTURE1,
	GE_TEXTURE2,
	GE_TEXTURE3
};

enum TextureCap
{
	GE_TEXTURE2D
};

class Texture: public Resource
{
public:
	Texture(string name, int width, int height, vector<unsigned char>& data);

	virtual int Bind(unsigned int* indx, int glsl_index) = 0;

	virtual int SetAlpha(float alpha);

	int GetWidth();
	int GetHeight();
	int GetDataSize();

	virtual int TexImage2D(int width, int height, unsigned char* data) = 0;

protected:
	int mWidth;
	int mHeight;
	int mDataSize;
	vector<unsigned char> mData;

	float mAlpha;
};

#endif
