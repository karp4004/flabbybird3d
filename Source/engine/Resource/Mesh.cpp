#include "Mesh.h"
#include "Platform/Platform.h"

#include "Scene/MeshNode.h"

#include "Math/Matrix4.h"

#include <algorithm>

Mesh::Mesh(string name)
:Resource(name)
,max_left(0.)
,min_right(0.)
,max_up(0.)
,min_down(0.)
,max_far(0.)
,min_near(0.)
{
}

int Mesh::Allocate()
{
	map<int, shared_ptr<Material> >::iterator mMaterialList_it = mMaterialList.begin();
	while(mMaterialList_it != mMaterialList.end())
	{
		mMaterialList_it->second->Allocate();
		mMaterialList_it++;
	}

	vector<shared_ptr<MeshPrimitive> >::iterator mPrimitiveList_it = mPrimitiveList.begin();
	while(mPrimitiveList_it != mPrimitiveList.end())
	{
		(*mPrimitiveList_it)->Allocate();
		mPrimitiveList_it++;
	}

	return 0;
}

int Mesh::AddMaterial(int index, shared_ptr<Material> m)
{
	mMaterialList[index] = m;
	return 0;
}

int Mesh::AddPrimitive(shared_ptr<MeshPrimitive> p)
{
	EXCEPTION_TRY
	mPrimitiveList.push_back(p);
	EXCEPTION_CATCH

	return 0;
}

BBox& Mesh::GetBBox()
{
	return mBBox;
}

BSphere& Mesh::GetBSphere()
{
	return mBSphere;
}

shared_ptr<Material> Mesh::GetMaterial(int index)
{
	map<int, shared_ptr<Material> >::iterator mMaterialList_it = mMaterialList.find(index);
	if(mMaterialList_it != mMaterialList.end())
	{
		return mMaterialList_it->second;
	}

	return 0;
}

int Mesh::OnAddResource()
{
	vector<shared_ptr<MeshPrimitive> >::iterator mPrimitiveList_it = mPrimitiveList.begin();
	while(mPrimitiveList_it != mPrimitiveList.end())
	{
		max_left = std::max(max_left, (*mPrimitiveList_it)->mAABB.max_left);
		min_right = std::min(min_right, (*mPrimitiveList_it)->mAABB.min_right);
		max_up = std::max(max_up, (*mPrimitiveList_it)->mAABB.max_up);
		min_down = std::min(min_down, (*mPrimitiveList_it)->mAABB.min_down);
		max_far = std::max(max_far, (*mPrimitiveList_it)->mAABB.max_far);
		min_near = std::min(min_near, (*mPrimitiveList_it)->mAABB.min_near);

		mPrimitiveList_it++;
	}

	mBBox.width = max_left - min_right;
	mBBox.height = max_up - min_down;
	mBBox.length = max_far - min_near;

	mBSphere = BSphereFromBBox(mBBox);

	return 0;
}
