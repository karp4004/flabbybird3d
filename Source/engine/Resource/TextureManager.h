#ifndef TextureManagerH
#define TextureManagerH

#include <list>
#include <map>
#include <string>

#include "Core/SharedPtr.h"
#include "Core/FileStream.h"

#include "Resource/ResourceManager.h"
#include "Resource/Texture.h"

using namespace std;

class TextureManager: public ResourceManager
{
public:
	TextureManager();
	virtual int LoadFromStream(shared_ptr<FileStream> fs, string textureName);

private:
    shared_ptr<Texture> parseTexture(shared_ptr<FileStream> fs, const char* meshName);
};

#endif
