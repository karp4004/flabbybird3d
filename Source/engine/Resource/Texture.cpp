#include "Texture.h"
#include <sstream>

Texture::Texture(string name, int width, int height, vector<unsigned char>& data)
:Resource(name)
,mWidth(width)
,mHeight(height)
,mAlpha(1.)
{
	mData = data;
}

int Texture::SetAlpha(float alpha)
{
	mAlpha = alpha;
	return 0;
}

int Texture::GetWidth()
{
	return mWidth;
}

int Texture::GetHeight()
{
	return mHeight;
}

int Texture::GetDataSize()
{
	return mDataSize;
}
