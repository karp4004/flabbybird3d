#ifndef ShaderManagerH
#define ShaderManagerH

#include <list>
#include <map>
#include <string>

#include "Core/SharedPtr.h"
#include "Core/FileStream.h"

#include "Resource/ResourceManager.h"

using namespace std;

class ShaderManager: public ResourceManager
{
public:
	ShaderManager();
	int LoadFromStream(shared_ptr<FileStream> fs, string meshName);
};

#endif
