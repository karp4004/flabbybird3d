#include "ResourceProvider.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>

#include "Core/FileStream.h"
#include "Resource/MeshManager.h"
#include "Resource/TextureManager.h"
#include "Core/SharedPtr.h"
#include "Core/FileManager.h"
#include "Platform/Platform.h"

int ResourceProvider::LoadResources()
{
	EXCEPTION_TRY

	Platform::instance().Logger().LogInfo(__FILE__, "LoadResources:%s", "");

	map<string, ResourceManager*> folders_managers;
	folders_managers["meshes"] = &mMeshManager;
	folders_managers["shaders"] = &mShaderManager;
	folders_managers["textures"] = &mTextureManager;

	Platform::instance().Logger().LogInfo(__FILE__, "folders_managers:%d", folders_managers.size());

	map<string, ResourceManager*>::iterator folders_managers_it = folders_managers.begin();
	while(folders_managers_it != folders_managers.end())
	{
		shared_ptr<FileDir> dir = Platform::instance().FileManager().OpenDir(folders_managers_it->first);
		while(1)
		{
			const char* filename = dir->GetNextFileName();

			Platform::instance().Logger().LogInfo(__FILE__, "filename:%s", filename);

			if(filename)
			{
				string uri = folders_managers_it->first + "/" + filename;
				shared_ptr<File> f = Platform::instance().FileManager().OpenFile(uri, kFileRead);
				if(f.get())
				{
					shared_ptr<FileStream> fs(new FileStream(f.get()));
					if (fs->GetBufferLength() > 0)
					{
						folders_managers_it->second->LoadFromStream(fs, filename);
					}
					else
					{
						Platform::instance().Logger().LogError(__FILE__, "%s", "empty file");
					}
				}

			}
			else
			{
				break;
			}
		}

		folders_managers_it++;
	}

	Platform::instance().Interop().ResourcesLoaded();

	EXCEPTION_CATCH

	return 0;
}

int ResourceProvider::onSurfaceCreated()
{
	mMeshManager.Allocate();
	mShaderManager.Allocate();
	mTextureManager.Allocate();

	return 0;
}

shared_ptr<Mesh> ResourceProvider::GetMesh(string name)
{
	return static_pointer_cast<Mesh>(mMeshManager.GetResource(name));
}

shared_ptr<Shader> ResourceProvider::GetShader(string name)
{
	return static_pointer_cast<Shader>(mShaderManager.GetResource(name));
}

shared_ptr<Texture> ResourceProvider::GetTexture(string name)
{
	return static_pointer_cast<Texture>(mTextureManager.GetResource(name));
}

shared_ptr<Material> ResourceProvider::GetDefaultMaterial()
{
	return static_pointer_cast<Material>(mMeshManager.GetResource("defaultMaterial"));
}

map<string, shared_ptr<Resource>>* ResourceProvider::getMeshes()
{
	return mMeshManager.getResources();
}