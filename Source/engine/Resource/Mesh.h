#ifndef MeshH
#define MeshH

#include <vector>
#include <list>
#include <map>
#include "Core/SharedPtr.h"
#include "MeshPrimitive.h"
#include <string>

#include "Core/Resource.h"
#include "Resource/Shader.h"
#include "Resource/Material.h"
#include "Math/Quaternion.h"

using namespace std;

class MeshNode;
class Mesh: public Resource
{
public:
	Mesh(string name);

	int AddMaterial(int index, shared_ptr<Material> m);
	int AddPrimitive(shared_ptr<MeshPrimitive> p);
	virtual int OnAddResource();

	shared_ptr<Material> GetMaterial(int index);

	virtual int Allocate();

	BBox& GetBBox();
	BSphere& GetBSphere();

	vector<shared_ptr<MeshPrimitive> > mPrimitiveList;

private:

	map<int, shared_ptr<Material> > mMaterialList;

	list<BBox> mBBoxList;
	BBox mBBox;
	list<BSphere> mBSphereList;
	BSphere mBSphere;

	float max_left;
	float min_right;
	float max_up;
	float min_down;
	float max_far;
	float min_near;
};

#endif
