#ifndef MeshPrimitiveSkeletalH
#define MeshPrimitiveSkeletalH

#include "Scene/Vertex.h"
#include "Math/Quaternion.h"
#include "Resource/MeshPrimitive.h"

#include <vector>
#include <unordered_map>

using namespace std;

struct BoneAttribute
{
	BoneAttribute(float ids[4])
	{
		memcpy(mBoneIds, ids, sizeof(mBoneIds));
	}

	float mBoneIds[4];
};

struct TimedKey
{
	double mTime;

	TimedKey()
	{
		mTime = 0.0;
	}

	TimedKey(double t)
	{
		mTime = t;
	}
};

struct VectorKey: public TimedKey
{
	Vector3 mValue;
	
	VectorKey() : TimedKey()
	{
	}

	VectorKey(double time, const Vector3& value)
		: TimedKey(time)
		, mValue(value)
	{}

	bool operator == (const VectorKey& o) const {
		return o.mValue == this->mValue;
	}
	bool operator != (const VectorKey& o) const {
		return o.mValue != this->mValue;
	}

	bool operator < (const VectorKey& o) const {
		return mTime < o.mTime;
	}
	bool operator > (const VectorKey& o) const {
		return mTime > o.mTime;
	}
};

struct QuatKey : public TimedKey
{
	Quaternion mValue;

	QuatKey() {
	}

	QuatKey(double time, const Quaternion& value)
		: TimedKey(time)
		, mValue(value)
	{}

	typedef Quaternion elem_type;

	bool operator == (const QuatKey& o) const {
		return o.mValue == this->mValue;
	}
	bool operator != (const QuatKey& o) const {
		return o.mValue != this->mValue;
	}
	bool operator < (const QuatKey& o) const {
		return mTime < o.mTime;
	}
	bool operator > (const QuatKey& o) const {
		return mTime > o.mTime;
	}
};

struct VertexWeight
{
	unsigned int mVertexId;
	float mWeight;

	VertexWeight() { }
	VertexWeight(unsigned int pID, float pWeight)
		: mVertexId(pID), mWeight(pWeight)
	{
	}
};

class BoneWeights {
public:
	BoneWeights()
	{
		boneId = -1;
	}

	Matrix4 offsetMatrix;
	std::vector<VertexWeight> weights;

	unsigned int boneId;
};

class Channel {
public:
	Channel()
	{
		boneId = -1;
	}

	unsigned int boneId;

	std::vector<VectorKey> positions;
	std::vector<QuatKey> rotations;
	std::vector<VectorKey> scales;
};

class SkeletalAnimation
{
public:
	SkeletalAnimation()
	{
		duration = 0.0;
		ticksPerSecond = 1.0;
	}

	double duration;
	double ticksPerSecond;

	std::vector<Channel> channels;
};

class Bone {
public:
	Bone()
	{
		parentBoneId = -1;
		hasParentBoneId = false;
	}

	unsigned int parentBoneId;
	bool hasParentBoneId;
	Matrix4 transformation;
};

struct WeightAttribute
{
	WeightAttribute(float w[4])
	{
		memcpy(mWeights, w, sizeof(mWeights));
	}

	float mWeights[4];
};

class MeshPrimitiveSkeletal: public MeshPrimitive
{
public:

	static const  int MaxBoneCount;

	MeshPrimitiveSkeletal(string name, Buffer* buffer);
	
	virtual void Bind(int p, int n, int uv, int b, int w, unsigned int* deffuse, unsigned int* alpha);
	virtual double AnimationDur();

	vector<SkeletalAnimation> mAnimations;
	std::vector<Bone> bones;
	std::unordered_map<std::string, unsigned int> boneName2boneId;
	std::vector<BoneWeights> boneWeights;

	vector<BoneAttribute> mBones;
	vector<WeightAttribute> mWeights;

	float mBoneStride;
	float mWeightStride;

	int currentAnimation;

public:

	//void loadFromScene(const aiScene *scene, int sz);
	//unsigned int getBoneId(const aiNode* node);
	void getMeshFrame();
	void createFrame(double elapsed);
	virtual Matrix4* getBoneTransforms(double elapsed);
	virtual void loadFromStream(shared_ptr<FileStream> fs);
	virtual void loadAnimation(shared_ptr<FileStream> fs);
};

#endif
