#ifndef ShaderH
#define ShaderH

#include "Core/Resource.h"
#include "Core/SharedPtr.h"
#include "Math/Matrix4.h"

class MeshPrimitive;

class Shader: public Resource
{
public:
	Shader(string name);
	virtual int Allocate() = 0;

	void SetVertexSourceCode(string code);
	void SetFragmentSourceCode(string code);
	string GetVertexSourceCode();
	string GetFragmentSourceCode();

	virtual int UseProgram() = 0;

	virtual void SetMVMatrix(Matrix4 m) = 0;
	virtual void SetMVPMatrix(Matrix4 m) = 0;
	virtual void SetModelMatrix(Matrix4 m) = 0;
	virtual void SetProjectionMatrix(Matrix4 m) = 0;
	virtual void SetOrthoMatrix(Matrix4 m) = 0;
	virtual void SetViewMatrix(Matrix4 m) = 0;
	virtual void SetEyePosition(Vector3 v) = 0;
	virtual void SetLightPosition(Vector3 v) = 0;
	virtual void SetLightArray(Vector3* v, int lightCount) = 0;
	virtual int SetColor(Vector4 v) = 0;
	virtual int SetTime(float t) = 0;
	virtual int SetBoneTransforms(Matrix4* trans) = 0;

	virtual void RenderMeshPrimitive(shared_ptr<MeshPrimitive> p, float elapsedTime) = 0;

protected:
	string vertexSourceCode;
	string fragmentSourceCode;

};

#endif
