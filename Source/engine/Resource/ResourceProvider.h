#ifndef ResourceProviderH
#define ResourceProviderH

#include "Core/SharedPtr.h"

#include "Core/ActivityObject.h"
#include "Resource/Shader.h"
#include "Resource/ShaderManager.h"
#include "Resource/MeshManager.h"
#include "Resource/TextureManager.h"

class Scene;

class ResourceProvider
{
private:
	ResourceProvider() {}

public:
	static ResourceProvider& instance()
	{
		static ResourceProvider INSTANCE;
		return INSTANCE;
	}

	virtual int LoadResources();
	virtual int onSurfaceCreated();

	shared_ptr<Mesh> GetMesh(string name);
	shared_ptr<Shader> GetShader(string name);
	shared_ptr<Texture> GetTexture(string name);
	shared_ptr<Material> GetDefaultMaterial();

	map<string, shared_ptr<Resource>>* getMeshes();

private:
	MeshManager mMeshManager;
	ShaderManager mShaderManager;
	TextureManager mTextureManager;
};

#endif
