#ifndef MaterialH
#define MaterialH

#include <map>
#include "Core/SharedPtr.h"
#include "Resource/Texture.h"

using namespace std;

enum aiTextureTypeDef
{
	aiTextureTypeDef_NONE = 0x0,
    aiTextureTypeDef_DIFFUSE = 0x1,
    aiTextureTypeDef_SPECULAR = 0x2,
    aiTextureTypeDef_AMBIENT = 0x3,
    aiTextureTypeDef_EMISSIVE = 0x4,
    aiTextureTypeDef_HEIGHT = 0x5,
    aiTextureTypeDef_NORMALS = 0x6,
    aiTextureTypeDef_SHININESS = 0x7,
    aiTextureTypeDef_OPACITY = 0x8,
    aiTextureTypeDef_DISPLACEMENT = 0x9,
    aiTextureTypeDef_LIGHTMAP = 0xA,
    aiTextureTypeDef_REFLECTION = 0xB,
    aiTextureTypeDef_UNKNOWN = 0xC,
};

class Material: public Resource
{
public:
	Material(string name, shared_ptr<Texture> deffuse, float alpha, aiTextureTypeDef t = aiTextureTypeDef_DIFFUSE);
	int Bind(unsigned int* deffuse, unsigned int* alpha);
	virtual int Allocate();
	aiTextureTypeDef getType();

private:
	shared_ptr<Texture> mDeffuse;
	float mAlpha;
	aiTextureTypeDef type;
};

#endif
