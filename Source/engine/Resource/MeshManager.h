#ifndef MeshManagerH
#define MeshManagerH

#include <list>
#include <map>
#include <string>

#include "Core/SharedPtr.h"
#include "Core/FileStream.h"

#include "Math/Line.h"

#include "Resource/ResourceManager.h"
#include "Resource/Mesh.h"

using namespace std;

class Vertex;

class MeshManager: public ResourceManager
{
public:
	MeshManager();

	virtual int LoadFromStream(shared_ptr<FileStream> fs, string meshName);

private:
    shared_ptr<MeshPrimitive> parsePrimitive(shared_ptr<FileStream> fs, const char* meshName);
    shared_ptr<Material> parseMaterial(shared_ptr<FileStream> fs, const char* meshName);
    int parseHeader(shared_ptr<FileStream> fs);
    shared_ptr<Mesh> ParseMesh(shared_ptr<FileStream> fs, const char* meshName);

    shared_ptr<Material> defaultMaterial;
	vector<unsigned char> data;
};

#endif
