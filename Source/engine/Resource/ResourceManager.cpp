#include "ResourceManager.h"
#include "Platform\Platform.h"
#include "Core/ActivityObject.h"

ResourceManager::ResourceManager()
:Resource("ResourceManager")
{
}

int ResourceManager::GetResourceCount()
{
	return resourceTable.size();
}

int ResourceManager::AddResource(shared_ptr<Resource> resource)
{
	EXCEPTION_TRY

	resource->OnAddResource();
	resourceTable[resource->GetName()] = resource;

	EXCEPTION_CATCH

	return 0;
}

shared_ptr<Resource> ResourceManager::GetResource(string name)
{
	map<string, shared_ptr<Resource>>::iterator resourceTable_it = resourceTable.find(name);
	if(resourceTable_it != resourceTable.end())
	{
		return resourceTable_it->second;
	}

	return 0;
}

int ResourceManager::Allocate()
{
	map<string, shared_ptr<Resource>>::iterator resourceTable_it = resourceTable.begin();
	while(resourceTable_it != resourceTable.end())
	{
		if (resourceTable_it->second->Allocate() == -1)
		{
			resourceTable_it = resourceTable.erase(resourceTable_it);
		}
		else
		{
			resourceTable_it++;
		}
	}

	return 0;
}

map<string, shared_ptr<Resource>>* ResourceManager::getResources()
{
	return &resourceTable;
}
