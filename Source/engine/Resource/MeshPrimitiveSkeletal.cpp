#include "MeshPrimitiveSkeletal.h"
#include "Render/Buffer.h"
#include "Math/Quaternion.h"
#include "Core/FileStream.h"
#include "Platform\Platform.h"
#include "Core/ActivityObject.h"
#include <functional>

const int MeshPrimitiveSkeletal::MaxBoneCount = 60;

MeshPrimitiveSkeletal::MeshPrimitiveSkeletal(string name, Buffer* buffer):
MeshPrimitive(name, buffer)
,currentAnimation(0)
{
	mBoneTransforms.resize(MeshPrimitiveSkeletal::MaxBoneCount);

	mBoneStride = sizeof(Vector4);
	mBoneStride /= sizeof(float);

	mWeightStride = sizeof(Vector4);
	mWeightStride /= sizeof(float);

	mVertexStride = sizeof(VertexSkeletal);
}

void MeshPrimitiveSkeletal::Bind(int p, int n, int uv, int b, int w, unsigned int* deffuse, unsigned int* alpha)
{
	MeshPrimitive::Bind(p, n, uv, b, w, deffuse, alpha);

	if (mBuffer != NULL)
	{
		mBuffer->BindBones(b, mVertexStride, mPositionStride, mNormalStride, mUVStride, mBoneStride, &mBones);
		mBuffer->BindWeights(w, mVertexStride, mPositionStride, mNormalStride, mUVStride, mBoneStride, mWeightStride, &mWeights);
	}
}

double MeshPrimitiveSkeletal::AnimationDur()
{
	if (currentAnimation >= 0 && currentAnimation < mAnimations.size())
	{
		return mAnimations[currentAnimation].duration;
	}

	return 0.f;
}

void MeshPrimitiveSkeletal::createFrame(double elapsed) {
	if (currentAnimation >= 0 && currentAnimation < mAnimations.size()) {

		Channel& channel = mAnimations[currentAnimation].channels[0];

		int pframe = 0;
		for (int i = 0; i < channel.positions.size(); i++)
		{
			if (channel.positions[i].mTime > elapsed)
			{
				pframe = i;
				break;
			}
		}

		int sframe = 0;
		for (int i = 0; i < channel.scales.size(); i++)
		{
			if (channel.scales[i].mTime > elapsed)
			{
				sframe = i;
				break;
			}
		}

		int rframe = 0;
		for (int i = 0; i < channel.rotations.size(); i++)
		{
			if (channel.rotations[i].mTime > elapsed)
			{
				rframe = i;
				break;
			}
		}

		for (auto& channel : mAnimations[currentAnimation].channels) {

			Vector3 scale = channel.scales[sframe].mValue;
			Quaternion rotation = channel.rotations[rframe].mValue;
			Vector3 position = channel.positions[pframe].mValue;

			if (channel.boneId >= 0 && channel.boneId < bones.size())
			{
				bones[channel.boneId].transformation = Matrix4x4Compose(scale, &rotation, position);
			}
		}
	}
}

void MeshPrimitiveSkeletal::getMeshFrame() {
	for (unsigned int cb = 0; cb < boneWeights.size(); cb++) {
		const auto& boneWeight = boneWeights[cb];

		Matrix4 transformation;
		const std::function<void(unsigned int)> calculateBoneTransformation = [&](unsigned int boneId) {
			if (boneId >= 0 && boneId < bones.size())
			{
				if (bones[boneId].hasParentBoneId) {
					calculateBoneTransformation(bones[boneId].parentBoneId);
				}

				transformation = MM(bones[boneId].transformation, transformation);
			}
		};
		calculateBoneTransformation(boneWeight.boneId);
	
		Matrix4 m = boneWeight.offsetMatrix;
		transformation = MM(m, transformation);

		mBoneTransforms[cb] = transformation;
	}
}

Matrix4* MeshPrimitiveSkeletal::getBoneTransforms(double elapsed)
{
	createFrame(elapsed);

	getMeshFrame();

	return &mBoneTransforms[0];
}

void MeshPrimitiveSkeletal::loadAnimation(shared_ptr<FileStream> fs)
{
	EXCEPTION_TRY

	int bwn = fs->ReadInt();
	for (int i = 0; i < bwn; i++)
	{
		BoneWeights bw;
		bw.boneId = fs->ReadInt();
		fs->ReadMemory(&bw.offsetMatrix, sizeof(bw.offsetMatrix));

		int wn = fs->ReadInt();
		for (int j = 0; j < wn; j++)
		{
			VertexWeight w;
			w.mVertexId = fs->ReadInt();
			fs->ReadMemory(&w.mWeight, sizeof(w.mWeight));
			bw.weights.push_back(w);
		}

		boneWeights.push_back(bw);
	}

	int bn = fs->ReadInt();
	for (int i = 0; i < bn; i++)
	{
		Bone b;
		b.parentBoneId = fs->ReadInt();
		b.hasParentBoneId = b.parentBoneId != -1;
		fs->ReadMemory(&b.transformation, sizeof(b.transformation));
		bones.push_back(b);
	}

	int an = fs->ReadInt();

	for (int i = 0; i < an; i++)
	{
		SkeletalAnimation a;
		fs->ReadMemory(&a.duration, sizeof(a.duration));
		fs->ReadMemory(&a.ticksPerSecond, sizeof(a.ticksPerSecond));

		int chn = fs->ReadInt();
		for (int i = 0; i < chn; i++)
		{
			Channel ch;
			ch.boneId = fs->ReadInt();

			std::vector<VectorKey> positions;
			std::vector<QuatKey> rotations;
			std::vector<VectorKey> scales;

			int pn = fs->ReadInt();
			for (int j = 0; j < pn; j++)
			{
				VectorKey k;
				fs->ReadMemory(&k, sizeof(k));
				ch.positions.push_back(k);
			}

			int rn = fs->ReadInt();
			for (int j = 0; j < rn; j++)
			{
				QuatKey k;
				fs->ReadMemory(&k, sizeof(k));
				ch.rotations.push_back(k);
			}

			int sn = fs->ReadInt();
			for (int j = 0; j < sn; j++)
			{
				VectorKey k;
				fs->ReadMemory(&k, sizeof(k));
				ch.scales.push_back(k);
			}

			a.channels.push_back(ch);
		}

		mAnimations.push_back(a);
	}

	EXCEPTION_CATCH
}

void MeshPrimitiveSkeletal::loadFromStream(shared_ptr<FileStream> fs)
{
	EXCEPTION_TRY

		if (mBuffer != NULL)
		{
			int vertex_count = fs->ReadInt();

			if (mBuffer->getBufferTpe() == kVBOBT
				|| mBuffer->getBufferTpe() == kVAOBT)
			{
				mVertices.reset(new VertexArray<VertexSkeletal>);
				mVertices->setCount(vertex_count);
				fs->ReadMemory(mVertices->getVertices(), mVertices->getVertexSize()*vertex_count);

				loadAnimation(fs);
			}
			else if (mBuffer->getBufferTpe() == kArrayBT)
			{
				mPositions.reset(new vector<Vector3>);
				mNormals.reset(new vector<Vector3>);
				mUVs.reset(new vector<Vector2>);

				for (int i = 0; i < vertex_count; i++)
				{
					VertexSkeletal v;
					fs->ReadMemory(&v, sizeof(v));
					mPositions->push_back(v.position);
					mNormals->push_back(v.normal);
					mUVs->push_back(v.uv);
					mBones.push_back(BoneAttribute(v.mBoneIds));
					mWeights.push_back(WeightAttribute(v.mWeights));
				}

				loadAnimation(fs);
			}

			loadIndices(fs);

			mAABB = mBuffer->GenerateBBox(mVertices.get(), mPositions.get());
		}

	EXCEPTION_CATCH
}