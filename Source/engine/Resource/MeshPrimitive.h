#ifndef MeshPrimitiveH
#define MeshPrimitiveH

#include "Resource/Material.h"
#include "Scene/VertexArray.h"
#include "Math/BBox.h"

#include <vector>

using namespace std;

class Vector3;
class Vector2;
class Buffer;

class MeshPrimitive: public Resource
{
public:
	MeshPrimitive(string name, Buffer* b);

	virtual int Allocate();
	virtual void loadFromStream(shared_ptr<FileStream> fs);
	virtual void loadIndices(shared_ptr<FileStream> fs);

	virtual int SetMaterial(shared_ptr<Material> m);

	virtual void Bind(int p, int n, int uv, int b, int w, unsigned int* deffuse, unsigned int* alpha);
	virtual void Render();
	virtual Matrix4* getBoneTransforms(double elapsed) { return 0; }
	void generateBBox();

	int IndicesNum();

	int setHidden(bool h);
	bool hidden();
	virtual double AnimationDur() { return 0.0; };

	struct AABB
	{
		AABB():
			max_left(0.)
			, min_right(0.)
			, max_up(0.)
			, min_down(0.)
			, max_far(0.)
			, min_near(0.) {

		}

		float max_left;
		float min_right;
		float max_up;
		float min_down;
		float max_far;
		float min_near;
	};

	AABB mAABB;

protected:
	int mPositionStride;
	int mNormalStride;
	int mUVStride;
	int mVertexStride;

	shared_ptr<Material> mMaterial;

	bool mHidden;

	shared_ptr<IVertexArray> mVertices;
	shared_ptr<vector<Vector3>> mPositions;
	shared_ptr<vector<Vector3>> mNormals;
	shared_ptr<vector<Vector2>> mUVs;
	shared_ptr<vector<unsigned short>> mIndices;
	std::vector<Matrix4> mBoneTransforms;

	Buffer* mBuffer;
};

#endif
