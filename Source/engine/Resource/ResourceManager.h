#ifndef ResourceManagerH
#define ResourceManagerH

#include <list>
#include <map>
#include <string>
#include "Core/SharedPtr.h"
#include "Core/Resource.h"

using namespace std;

class FileStream;

class ResourceManager: public Resource
{
public:
	ResourceManager();

	virtual int LoadFromStream(shared_ptr<FileStream> fs, string meshName) = 0;

	int AddResource(shared_ptr<Resource> resource);
    virtual shared_ptr<Resource> GetResource(string name);
    int GetResourceCount();
	virtual int Allocate();

    map<string, shared_ptr<Resource>>* getResources();

protected:
    map<string, shared_ptr<Resource>> resourceTable;
};

#endif
