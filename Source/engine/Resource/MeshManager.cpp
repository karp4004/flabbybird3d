#include "MeshManager.h"

#include "Math/GEMath.h"
#include "Scene/Vertex.h"
#include "Render/GL/GLBufferObject.h"
#include "Render/GL/GLArrayObject.h"
#include "Render/GL/GLBufferArray.h"
#include "Resource/MeshPrimitiveSkeletal.h"
#include "ApplicationFrame.h"
#include "Platform/Platform.h"

#include <sstream>

#define VBO 0
#define VAO 1
#define ARRAY 2

#define  DEBUG_MESHMANAGER 1

MeshManager::MeshManager()
:ResourceManager()
{
	unsigned char d[16] =
	{
	  255,   0,   0, 100,// Red
		0, 255,   0, 100, // Green
		0,   0, 255, 100,// Blue
	  255, 255,   0,  100  // Yellow
	};
	data.resize(16);
	memcpy(&data[0], d, 16);

	EXCEPTION_TRY
	shared_ptr<Texture> deffuse(Platform::instance().RenderFactory().CreateTexture("defaultDeffuse", GE_RGBA, 2, 2, GE_RGBA, data, GE_TEXTURE0, GE_TEXTURE2D));
	defaultMaterial.reset(new Material("defaultMaterial", deffuse, 0.2));
	AddResource(defaultMaterial);
	EXCEPTION_CATCH
}

shared_ptr<MeshPrimitive> MeshManager::parsePrimitive(shared_ptr<FileStream> fs, const char* meshName)
{
	shared_ptr<MeshPrimitive> ret;

	EXCEPTION_TRY

		int skeletal = fs->ReadInt();
		if (skeletal == 0)
		{
			ret.reset(Platform::instance().RenderFactory().CreatePrimitive(meshName, new GLBufferObject()));
			ret->loadFromStream(fs);
		}
		else if (skeletal == 1)
		{
			ret.reset(Platform::instance().RenderFactory().CreatePrimitiveSkeletal(meshName, new GLBufferObject()));
			ret->loadFromStream(fs);
		}

	EXCEPTION_CATCH

	return ret;
}

shared_ptr<Material> MeshManager::parseMaterial(shared_ptr<FileStream> fs, const char* meshName)
{
	EXCEPTION_TRY

		aiTextureTypeDef textureType = (aiTextureTypeDef)fs->ReadInt();
		PixelFormat bpp = (PixelFormat)fs->ReadInt();
		int width = fs->ReadInt();
		int height = fs->ReadInt();
		PixelFormat format = (PixelFormat)fs->ReadInt();
		int data_size = fs->ReadInt();

		vector<unsigned char> data;
		data.resize(data_size);
		fs->ReadMemory(&data[0], data_size);

		ostringstream ostr;
		ostr << meshName << textureType;

		TextureUnit unit = GE_TEXTURE0;
		if(textureType == aiTextureTypeDef_NORMALS)
		{
			unit = GE_TEXTURE1;
		}
		else if(textureType == aiTextureTypeDef_SPECULAR)
		{
			unit = GE_TEXTURE2;
		}
		else if(textureType == aiTextureTypeDef_AMBIENT)
		{
			unit = GE_TEXTURE3;
		}

		shared_ptr<Texture> deffuse(Platform::instance().RenderFactory().CreateTexture(ostr.str().c_str(), bpp, width, height, format, data, unit, GE_TEXTURE2D));

		if(deffuse != NULL)
		{
			return shared_ptr<Material>(new Material(meshName, deffuse, 0.3, textureType));
		}

	EXCEPTION_CATCH

	return NULL;
}

int MeshManager::parseHeader(shared_ptr<FileStream> fs)
{
	char buffer[11];
	memset(buffer, 0, sizeof(buffer));
	fs->ReadMemory(buffer, sizeof(buffer));
	if(memcmp(buffer, "GollardMesh", sizeof(buffer)))
	{
		return -1;
	}

	return 0;
}

shared_ptr<Mesh> MeshManager::ParseMesh(shared_ptr<FileStream> fs, const char* meshName)
{
	EXCEPTION_TRY

		if(!parseHeader(fs))
		{		
			shared_ptr<Mesh> mesh(new Mesh(meshName));

			int texture_count = fs->ReadInt();

			for(int i=0;i<texture_count;i++)
			{
				int index = fs->ReadInt();

				if(index>-1)
				{
					ostringstream ostr;
					ostr << meshName << "_material" << "_" << index;
					shared_ptr<Material> m = parseMaterial(fs, ostr.str().c_str());
					if(m)
					{
						mesh->AddMaterial(index, m);
					}
				}
			}

			int pcount = fs->ReadInt();
			for(int i=0;i<pcount;i++)
			{
				ostringstream ostr;
				ostr << meshName << "_" << i;
				shared_ptr<MeshPrimitive> p = parsePrimitive(fs, ostr.str().c_str());
				if(p != NULL)
				{
					shared_ptr<Material> mat = mesh->GetMaterial(fs->ReadInt());
					if(!mat)
					{
						mat = defaultMaterial;
					}

					p->SetMaterial(mat);
					mesh->AddPrimitive(p);
				}
			}

			return mesh;
		}

	EXCEPTION_CATCH

	return NULL;
}

int MeshManager::LoadFromStream(shared_ptr<FileStream> fs, string meshName)
{
	if(GetResource(meshName) != NULL)
	{
		return -1;
	}

	shared_ptr<Mesh> mesh = ParseMesh(fs, meshName.c_str());
	if(mesh)
	{
		AddResource(mesh);
	}

	return 0;
}