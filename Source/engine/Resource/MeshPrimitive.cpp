#include "MeshPrimitive.h"
#include "Platform\Platform.h"
#include "Core/ActivityObject.h"
#include "Core/FileStream.h"
#include "Math/GEMath.h"
#include "Math/BBox.h"
#include "Render/Buffer.h"
#include "Scene/Vertex.h"
#include <sstream>      // std::ostringstream

#define  BUFFER_TYPE ARRAY //VBO VAO ARRAY

MeshPrimitive::MeshPrimitive(string name, Buffer* b)
:Resource(name)
,mHidden(false)
,mVertices(NULL)
,mPositions(NULL)
,mNormals(NULL)
,mUVs(NULL)
,mIndices(NULL)
,mBuffer(b)
{
	mPositionStride = sizeof(Vector3);
	mPositionStride /= sizeof(float);

	mNormalStride = sizeof(Vector3);
	mNormalStride /= sizeof(float);

	mUVStride = sizeof(Vector2);
	mUVStride /= sizeof(float);

	mVertexStride = sizeof(Vertex);
}

void MeshPrimitive::loadIndices(shared_ptr<FileStream> fs)
{
	EXCEPTION_TRY

	int indices_count = fs->ReadInt();
	if (indices_count>0)
	{
		mIndices.reset(new vector<unsigned short>());
		mIndices->resize(indices_count);
		fs->ReadMemory(&(*mIndices)[0], sizeof(unsigned short)*mIndices->size());
	}

	EXCEPTION_CATCH
}

void MeshPrimitive::loadFromStream(shared_ptr<FileStream> fs)
{
	EXCEPTION_TRY

		if (mBuffer != NULL)
		{
			int vertex_count = fs->ReadInt();
			if (mBuffer->getBufferTpe() == kVBOBT
				|| mBuffer->getBufferTpe() == kVAOBT)
			{
				mVertices.reset(new VertexArray<Vertex>);
				mVertices->setCount(vertex_count);
				fs->ReadMemory(mVertices->getVertices(), mVertices->getVertexSize()*vertex_count);
			}
			else if (mBuffer->getBufferTpe() == kArrayBT)
			{
				mPositions.reset(new vector<Vector3>);
				mNormals.reset(new vector<Vector3>);
				mUVs.reset(new vector<Vector2>);

				for (int i = 0; i < vertex_count; i++)
				{
					Vertex v;
					fs->ReadMemory(&v, sizeof(v));
					mPositions->push_back(v.position);
					mNormals->push_back(v.normal);
					mUVs->push_back(v.uv);
				}

			}

			loadIndices(fs);
		}

		mAABB = mBuffer->GenerateBBox(mVertices.get(), mPositions.get());

	EXCEPTION_CATCH
}

int MeshPrimitive::Allocate()
{
	Platform::instance().Logger().LogError(__FILE__, "%p %p", mVertices.get(), mIndices.get());

	if (mBuffer != NULL)
	{
		mBuffer->Allocate(mVertices.get(), mIndices.get());
	}

	return 0;
}

void MeshPrimitive::Bind(int p, int n, int uv, int b, int w, unsigned int* deffuse, unsigned int* alpha)
{
	if (mBuffer != NULL)
	{
		mBuffer->Bind();
		mBuffer->BindPositions(p, mVertexStride, mPositionStride, mPositions.get());
		mBuffer->BindNormals(n, mVertexStride, mPositionStride, mNormalStride, mNormals.get());
		mBuffer->BindUVs(uv, mVertexStride, mPositionStride, mNormalStride, mUVStride, mUVs.get());

		if (mMaterial)
		{
			mMaterial->Bind(deffuse, alpha);
		}
	}
}

void MeshPrimitive::Render()
{
	if (mBuffer != NULL)
	{
		mBuffer->Render(mIndices.get());
	}
}

int MeshPrimitive::SetMaterial(shared_ptr<Material> m)
{
	mMaterial = m;
	return 0;
}

int MeshPrimitive::IndicesNum()
{
	return mIndices->size();
}

int MeshPrimitive::setHidden(bool h)
{
	mHidden = h;

	return 0;
}

bool MeshPrimitive::hidden()
{
	return mHidden;
}

void MeshPrimitive::generateBBox()
{
	if (mBuffer != NULL)
	{
		mAABB = mBuffer->GenerateBBox(mVertices.get(), mPositions.get());
	}
}