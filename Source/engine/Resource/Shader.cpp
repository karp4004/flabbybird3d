#include "Shader.h"

Shader::Shader(string name)
:Resource(name)
{
}

void Shader::SetVertexSourceCode(string code)
{
	vertexSourceCode = code;
}

void Shader::SetFragmentSourceCode(string code)
{
	fragmentSourceCode = code;
}

string Shader::GetVertexSourceCode()
{
	return vertexSourceCode;
}

string Shader::GetFragmentSourceCode()
{
	return fragmentSourceCode;
}
