#include "Core/ActivityObject.h"
#include "Core/OpenGL.h"
#include "Scene/MeshNode.h"
#include "Scene/Camera.h"
#include "Scene/LightNode.h"

#if defined(_WIN32)
#define DLLEXPORT __declspec(dllexport)
#define STDCALL __stdcall
#else
#define DLLEXPORT
#define STDCALL
#endif

#ifdef __cplusplus
extern "C" {
#endif
	DLLEXPORT int STDCALL ReleaseScene();
	DLLEXPORT int STDCALL OnSurfaceCreated();
	DLLEXPORT int STDCALL OnSurfaceChanged(int width, int height);
	DLLEXPORT int STDCALL OnDrawFrame();

	DLLEXPORT int STDCALL GetFPS();
	DLLEXPORT int STDCALL GetFaces();
	DLLEXPORT void STDCALL OnTouchScreen();

	DLLEXPORT void STDCALL Zoom(Camera* m, int in);
	DLLEXPORT Vector3 STDCALL Unproject(Camera* cam, float x, float y);
	DLLEXPORT void STDCALL CameraSlide(Camera* node, Vector2 slide);
	DLLEXPORT void STDCALL MoveableMove(Moveable* m, float x, float y, float z);
	DLLEXPORT void STDCALL MoveableTranslate(Moveable* m, float x, float y, float z);
	DLLEXPORT void STDCALL MeshSetColor(MeshNode* n, Vector4 col);
	DLLEXPORT Vector3 STDCALL MeshProject(MeshNode* n);
	DLLEXPORT void STDCALL MoveableRotate(Moveable* n, Vector3 rot);
	DLLEXPORT Vector3 STDCALL MoveablePosition(Moveable* n);
	DLLEXPORT void STDCALL MeshSetShader(MeshNode* n, const char* shader);

#ifdef __cplusplus
}
#endif