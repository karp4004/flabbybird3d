#ifndef BufferH
#define BufferH

#include "Core/OpenGL.h"
#include "Scene/Vertex.h"
#include "Resource/MeshPrimitive.h"
#include "Resource/MeshPrimitiveSkeletal.h"

#include <string>

using namespace std;

enum BufferTpe
{
	kArrayBT,
	kVBOBT,
	kVAOBT
};

class Buffer
{
public:
	virtual BufferTpe getBufferTpe() = 0;

	virtual int Bind() = 0;
	virtual int Render(vector<unsigned short>* indices) = 0;
	virtual MeshPrimitive::AABB GenerateBBox(IVertexArray* vertices, vector<Vector3>* positions) = 0;
	virtual int Allocate(IVertexArray* vertices, vector<unsigned short>* indices) = 0;
	virtual int BindPositions(unsigned int indx, int vertex_stride, int position_stride, vector<Vector3>* positions) = 0;
	virtual int BindNormals(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, vector<Vector3>* normals) = 0;
	virtual int BindUVs(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, vector<Vector2>* uvs) = 0;
	virtual int BindBones(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, vector<BoneAttribute>* bones) = 0;
	virtual int BindWeights(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, int weight_stride, vector<WeightAttribute>* weights) = 0;
};

#endif
