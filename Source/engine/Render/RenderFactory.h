#ifndef RENDER_FACTORY
#define RENDER_FACTORY

#include "Resource/Texture.h"

class MeshPrimitive;
class MeshPrimitiveSkeletal; 
class Shader;
class Buffer;

using namespace std;

class SignedPlace;

class RenderFactory
{
public:
	virtual MeshPrimitive* CreatePrimitive(string name, Buffer* buffer) = 0;
	virtual MeshPrimitiveSkeletal* CreatePrimitiveSkeletal(string name, Buffer* buffer) = 0;
	virtual Shader* CreateShader(string name) = 0;
	virtual Texture* CreateTexture(string name, PixelFormat bpp, int width, int height, PixelFormat format, vector<unsigned char>& data, TextureUnit unit, TextureCap cap) = 0;
};

#endif
