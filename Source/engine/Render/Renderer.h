#ifndef RendererH
#define RendererH

#include <map>

#include <Math/GEMath.h>
#include <Math/Frustum.h>
#include <Math/Line.h>

#include "Core/SharedPtr.h"

#include "Resource/Shader.h"

using namespace std;

class MeshPrimitive;
class Renderer
{
public:
	Renderer()
	{ 
		memset(mViewport, 0, sizeof(mViewport)); 
		mFPS = 0;
		mTris = 0;
		mTrisCount = 0;
		frameCounter = 0;
		avgFPS = 0;
		allCounter = 0;
		secCounter = 0;
		_MeshRendered = 0;
		_MeshRenderedCount = 0;
	}

	virtual int setViewport(float left, float top, float w, float h) = 0;
	virtual int PreRender(float elapsedTime, float deltaTime) = 0;
	int SetShader(shared_ptr<Shader> s);
	int SetShader(shared_ptr<Shader> s, double t, Matrix4& model);
	shared_ptr<Shader> GetShader();

    void OffsetModelMatrix(Vector3& v);
    void SetProjectionMatrix(Matrix4& m);
    void SetOrthoMatrix(Matrix4& m);
    void SetViewMatrix(Matrix4& m);
    void SetEyePosition(Vector3& v);
    void SetLightPosition(Vector3& v);
    void UpdateLight(int l, Vector3& v);
	int SetColor(Vector4& v);
	virtual int SetTransparency(bool on) = 0;

	virtual Vector3 Unproject(Vector3& v, Matrix4& model) = 0;
	virtual Vector3 Project(Vector3& v, Matrix4& model) = 0;

    bool SphereInFrustum(BSphere& sphere);

	void addStatTris(int tris)
	{
		mTris += tris;
	}

	void addMesh()
	{
		_MeshRendered++;
	}

	int getFPS()
	{
		return mFPS;
	}

	int getFaces()
	{
		return mTrisCount;
	}

	int getMeshRenderedCount()
	{
		return _MeshRenderedCount;
	}

	int* getViewport()
	{
		return mViewport;
	}

protected:

    int ExtractFrustum(Matrix4& mvp);

    shared_ptr<Shader> activeShader;

	Matrix4 mMVMatrix;
	Matrix4 mMVPMatrix;

	Matrix4 mModelMatrix;
	Matrix4 mProjectionMatrix;
	Matrix4 mOrthoMatrix;
    Matrix4 mViewMatrix;
    Vector3 mEyePosition;
    Vector4 mColor;

    Vector3 mLightPosition;
    map<int, Vector3> mLightArray;
    Vector4 mLightEyeSpace;
    Vector3 mLightArrayView[10];

    shared_ptr<MeshPrimitive> mPrimitive;

    Frustum mFrustum;

	int mViewport[4];

private:
	float mFPS;
	int mTris;
	int mTrisCount;
	int frameCounter;
	float avgFPS;
	float allCounter;
	float secCounter;
	int _MeshRendered;
	int _MeshRenderedCount;
};

#endif
