#ifndef GLShaderH
#define GLShaderH

#include "Core/OpenGL.h"
#include "Resource/Shader.h"

class MeshPrimitive;
class GLShader: public Shader
{
public:
	GLShader(string name);

	virtual int Allocate();

	virtual int UseProgram();

	virtual void SetMVMatrix(Matrix4 m);
	virtual void SetMVPMatrix(Matrix4 m);
	virtual void SetModelMatrix(Matrix4 m);
	virtual void SetProjectionMatrix(Matrix4 m);
	virtual void SetOrthoMatrix(Matrix4 m);
	virtual void SetViewMatrix(Matrix4 m);
	virtual void SetEyePosition(Vector3 v);
	virtual void SetLightPosition(Vector3 v);
	virtual void SetLightArray(Vector3* v, int lightCount);	
	virtual int SetColor(Vector4 v);
	virtual int SetTime(float t);
	virtual int SetBoneTransforms(Matrix4* trans);

	virtual void RenderMeshPrimitive(shared_ptr<MeshPrimitive> p, float elapsedTime);

private:

	GLuint loadShader(GLenum shaderType, const char* pSource);
	int SetAttribute(GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr, string object_name);
	int SetUniformMatrix4(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value, string object_name);
	int SetUniform3(GLint location, GLsizei count, const GLfloat* v, string object_name);
	int SetUniform1(GLint location, GLsizei count, const GLfloat* v, string object_name);
	int SetUniform4(GLint location, GLsizei count, const GLfloat* v, string object_name);

	GLuint mProgram;

//uniforms
	GLint mMVLoc;
	GLint mMVPLoc;

	GLint mModelLoc;
	GLint mViewLoc;
	GLint mProjectionLoc;
	GLint mOrthoLoc;
	GLint mLightLoc;
	GLint mCameraLoc;
	GLint mLightArray;
	GLint mLightCount;
	GLint mColor;
	GLint mTime;
	GLint uBones;

//attributes
	GLuint mvPositionHandle;
	GLuint mNormalHandle;
	GLuint mvUVHandle;
	GLuint mvBoneHandle;
	GLuint mvWeightHandle;

//fragment
	GLint mSampler2D;
	GLint mBlendFactor;
};

#endif
