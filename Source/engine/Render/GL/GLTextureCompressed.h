#ifndef GL_TEXTURE_COMPRESSEDH
#define GL_TEXTURE_COMPRESSEDH

#include "Core/OpenGL.h"
#include <Resource/Texture.h>
#include "GLTexture.h"

class GLTextureCompressed: public GLTexture
{
public:
	GLTextureCompressed(string name, GLushort bpp, int width, int height, GLushort format, vector<unsigned char>& data, GLushort unit, GLushort cap);
	virtual int TexImage2D(int width, int height, unsigned char* data);
};

#endif
