#include "GLBufferObject.h"
#include "GLRenderer.h"

#include <algorithm>
#include "Platform/Platform.h"

GLBufferObject::~GLBufferObject()
{
	GLCHECK(glDeleteBuffers(1, &mVBO));
	GLCHECK(glDeleteBuffers(1, &mIBO));
}

MeshPrimitive::AABB GLBufferObject::GenerateBBox(IVertexArray* vertices, vector<Vector3>* positions)
{
	MeshPrimitive::AABB ret;

	if (vertices != NULL)
	{
		for (int i = 0; i < vertices->getCount(); i++)
		{
			Vector3 pos = vertices->getPosition(i);
			ret.max_left = max(ret.max_left, pos.x);
			ret.min_right = min(ret.min_right, pos.x);
			ret.max_up = max(ret.max_up, pos.y);
			ret.min_down = min(ret.min_down, pos.y);
			ret.max_far = max(ret.max_far, pos.z);
			ret.min_near = min(ret.min_near, pos.z);
		}
	}

	return ret;
}

int GLBufferObject::Allocate(IVertexArray* vertices, vector<unsigned short>* indices)
{
	GLCHECK(glGenBuffers(1, &mVBO));
	GLCHECK(glGenBuffers(1, &mIBO));
	if (mVBO > 0 && mIBO > 0) {
		GLCHECK(glBindBuffer(GL_ARRAY_BUFFER, mVBO));
		GLCHECK(glBufferData(GL_ARRAY_BUFFER, vertices->getVertexSize() * vertices->getCount(), vertices->getVertices(), GL_STATIC_DRAW));
		GLCHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBO));
		GLCHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * indices->size(), &(*indices)[0], GL_STATIC_DRAW));
		GLCHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
		GLCHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
	}
	else
	{
		Platform::instance().Logger().LogInfo(__FILE__, ":%d:mVertexCount:%d\n", __LINE__, "glGenBuffers error");
	}

	return 0;
}

int GLBufferObject::Bind()
{
	GLCHECK(glBindBuffer(GL_ARRAY_BUFFER, mVBO));

	return 0;
}

int GLBufferObject::BindPositions(unsigned int indx, int vertex_stride, int position_stride, vector<Vector3>* positions)
{
	GLCHECK(glVertexAttribPointer(indx, 3, GL_FLOAT, GL_FALSE, vertex_stride, 0));
	GLCHECK(glEnableVertexAttribArray(indx));

	return 0;
}

int GLBufferObject::BindNormals(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, vector<Vector3>* normals)
{
	GLCHECK(glVertexAttribPointer(indx, 3, GL_FLOAT, GL_FALSE, vertex_stride, (GLvoid*)(position_stride *sizeof(float))));
	GLCHECK(glEnableVertexAttribArray(indx));

	return 0;
}

int GLBufferObject::BindUVs(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, vector<Vector2>* uvs)
{
	GLCHECK(glVertexAttribPointer(indx, uv_stride, GL_FLOAT, GL_FALSE, vertex_stride, (GLvoid*)((position_stride + normal_stride)*sizeof(float))));
	GLCHECK(glEnableVertexAttribArray(indx));

	return 0;
}

int GLBufferObject::BindBones(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, vector<BoneAttribute>* bones)
{
	GLCHECK(glVertexAttribPointer(indx, bone_stride, GL_FLOAT, GL_FALSE, vertex_stride, (GLvoid*)((position_stride + normal_stride + uv_stride) * sizeof(float))));
	GLCHECK(glEnableVertexAttribArray(indx));

	return 0;
}

int GLBufferObject::BindWeights(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, int weight_stride, vector<WeightAttribute>* weights)
{
	GLCHECK(glVertexAttribPointer(indx, weight_stride, GL_FLOAT, GL_FALSE, vertex_stride, (GLvoid*)((position_stride + normal_stride + uv_stride + bone_stride) * sizeof(float))));
	GLCHECK(glEnableVertexAttribArray(indx));

	return 0;
}

int GLBufferObject::Render(vector<unsigned short>* indices)
{
	GLCHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBO));
	GLCHECK(glDrawElements(GL_TRIANGLES, indices->size(), GL_UNSIGNED_SHORT, 0));
	GLCHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
	GLCHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));

	return indices->size();
}
