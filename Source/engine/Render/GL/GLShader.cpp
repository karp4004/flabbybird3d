﻿#include "GLShader.h"
#include <sstream>

#include "Resource/MeshPrimitiveSkeletal.h"
#include "GLRenderer.h"
#include "Core/OpenGL.h"
#include "Platform/Platform.h"

GLShader::GLShader(string name)
:Shader(name)
{
	mProgram = 0;

	mMVLoc = -1;
	mMVPLoc = -1;

	mModelLoc = -1;
	mViewLoc = -1;
	mProjectionLoc = -1;
	mOrthoLoc = -1;
	mLightLoc = -1;
	mCameraLoc = -1;
	mLightArray = -1;
	mLightCount = 0;
	mColor = -1;
	mTime = -1;
	uBones = -1;
	mvPositionHandle = -1;
	mvUVHandle = -1;
	mNormalHandle = -1;
	mvBoneHandle = -1;
	mvWeightHandle = -1;
	mSampler2D = -1;
	mBlendFactor = -1;
}

GLuint GLShader::loadShader(GLenum shaderType, const char* pSource) {
    GLuint shader = GLCHECK(glCreateShader(shaderType));
    if (shader) {
		GLCHECK(glShaderSource(shader, 1, &pSource, NULL));
		GLCHECK(glCompileShader(shader));
        GLint compiled = 0;
		GLCHECK(glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled));
        if (!compiled) {
            GLint infoLen = 0;
			GLCHECK(glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen));
            if (infoLen) {
                char* buf = (char*) malloc(infoLen);
                if (buf) {
					GLCHECK(glGetShaderInfoLog(shader, infoLen, NULL, buf));
					Platform::instance().Logger().LogError(__FILE__, "Could not compile shader %x:\n%s\n", shaderType, buf);
                    free(buf);
                }
				GLCHECK(glDeleteShader(shader));
                shader = 0;
            }
        }
    }
    return shader;
}

int GLShader::Allocate()
{
	string vertex = GetVertexSourceCode();
	string fragment = GetFragmentSourceCode();

	GLuint vertexShader = loadShader(GL_VERTEX_SHADER, vertex.c_str());
	if (!vertexShader) {
		return -1;
	}

	GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, fragment.c_str());
	if (!pixelShader) {
		return -1;
	}


	mProgram = GLCHECK(glCreateProgram());
	if (mProgram) {
		GLCHECK(glAttachShader(mProgram, vertexShader));
		GLCHECK(glAttachShader(mProgram, pixelShader));
		GLCHECK(glLinkProgram(mProgram));
		GLint linkStatus = GL_FALSE;
		GLCHECK(glGetProgramiv(mProgram, GL_LINK_STATUS, &linkStatus));
		if (linkStatus != GL_TRUE) {
			GLint bufLength = 0;
			GLCHECK(glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &bufLength));
			if (bufLength) {
				char* buf = (char*) malloc(bufLength);
				if (buf) {
					GLCHECK(glGetProgramInfoLog(mProgram, bufLength, NULL, buf));
					Platform::instance().Logger().LogError(__FILE__, "Could not link mProgram:\n%s\n", buf);
					free(buf);
				}
			}
			GLCHECK(glDeleteProgram(mProgram));
			mProgram = 0;
		}
	}

	if(mProgram == 0)
	{
		return -1;
	}

	//attributes
	mvPositionHandle = GLCHECK(glGetAttribLocation(mProgram, "aPosition"));
	mNormalHandle = GLCHECK(glGetAttribLocation(mProgram, "aNormal"));
	mvUVHandle = GLCHECK(glGetAttribLocation(mProgram, "aUV"));
	mvBoneHandle = GLCHECK(glGetAttribLocation(mProgram, "aBones"));
	mvWeightHandle = GLCHECK(glGetAttribLocation(mProgram, "aWeights"));
	mMVLoc = GLCHECK(glGetUniformLocation(mProgram, "uMVMatrix"));
	mMVPLoc = GLCHECK(glGetUniformLocation(mProgram, "uMVPMatrix"));
	mProjectionLoc = GLCHECK(glGetUniformLocation(mProgram, "uProjectMatrix"));
	mOrthoLoc = GLCHECK(glGetUniformLocation(mProgram, "uOrthoMatrix"));
	mViewLoc = GLCHECK(glGetUniformLocation(mProgram, "uViewMatrix"));
	mModelLoc = GLCHECK(glGetUniformLocation(mProgram, "uModelMatrix"));
	mSampler2D = GLCHECK(glGetUniformLocation(mProgram, "uSampler2D"));
	mLightLoc = GLCHECK(glGetUniformLocation(mProgram, "uLightPos"));
	mLightArray = GLCHECK(glGetUniformLocation(mProgram, "uLightArray"));
	mLightCount = GLCHECK(glGetUniformLocation(mProgram, "uLightCount"));
	mCameraLoc = GLCHECK(glGetUniformLocation(mProgram, "uEyePos"));
	mBlendFactor = GLCHECK(glGetUniformLocation(mProgram, "uBlendFactor"));
	mColor = GLCHECK(glGetUniformLocation(mProgram, "uColor"));
	mTime = GLCHECK(glGetUniformLocation(mProgram, "uTime"));
	uBones = GLCHECK(glGetUniformLocation(mProgram, "uBones"));

	return 0;
}

int GLShader::UseProgram()
{
	if(mProgram != NULL)
	{
		GLCHECK(glUseProgram(mProgram));
		return 0;
	}

	return -1;
}

int GLShader::SetAttribute(GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr, string object_name)
{
	if(indx==-1)
	{
		return -1;
	}

	GLCHECK(glVertexAttribPointer(indx, size, type, normalized, stride, ptr));
	GLCHECK(glEnableVertexAttribArray(indx));

	return 0;
}

int GLShader::SetUniformMatrix4(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value, string object_name)
{
	if(location==-1)
	{
		return -1;
	}

	GLCHECK(glUniformMatrix4fv( location, count, transpose, value));

    return 0;
}

int GLShader::SetUniform3(GLint location, GLsizei count, const GLfloat* v, string object_name)
{
	if(location==-1)
	{
		return -1;
	}

	GLCHECK(glUniform3fv( location, count, v));

    return 0;
}

int GLShader::SetUniform4(GLint location, GLsizei count, const GLfloat* v, string object_name)
{
	if(location==-1)
	{
		return -1;
	}

	GLCHECK(glUniform4fv( location, count, v));

    return 0;
}

int GLShader::SetUniform1(GLint location, GLsizei count, const GLfloat* v, string object_name)
{
	if(location==-1)
	{
		return -1;
	}

	GLCHECK(glUniform1fv(location, count, v));

    return 0;
}

void GLShader::SetMVMatrix(Matrix4 m)
{
	SetUniformMatrix4( mMVLoc, 1, GL_FALSE, (GLfloat*) m.v, "mvMatrix" );
}

void GLShader::SetMVPMatrix(Matrix4 m)
{
	SetUniformMatrix4( mMVPLoc, 1, GL_FALSE, (GLfloat*) m.v, "mvpMatrix" );

}

void GLShader::SetModelMatrix(Matrix4 m)
{
	SetUniformMatrix4( mModelLoc, 1, GL_FALSE, (GLfloat*) m.v, "mModelMatrix" );
}

void GLShader::SetProjectionMatrix(Matrix4 m)
{
	SetUniformMatrix4( mProjectionLoc, 1, GL_FALSE, (GLfloat*) m.v, "mProjectionMatrix" );
}

void GLShader::SetOrthoMatrix(Matrix4 m)
{
	SetUniformMatrix4( mOrthoLoc, 1, GL_FALSE, (GLfloat*) m.v, "mOrthoMatrix" );
}

void GLShader::SetViewMatrix(Matrix4 m)
{
	SetUniformMatrix4( mViewLoc, 1, GL_FALSE, (GLfloat*) m.v, "mViewMatrix" );
}

void GLShader::SetEyePosition(Vector3 v)
{
	SetUniform3(mCameraLoc, 1, (GLfloat*)&v, "mEyePosition");
}

void GLShader::SetLightPosition(Vector3 v)
{
	SetUniform3(mLightLoc, 1, (GLfloat*)&v, "mLightPosition");
}

int GLShader::SetColor(Vector4 v)
{
	return SetUniform4(mColor, 1, (GLfloat*)&v, "mColor");
}

int GLShader::SetTime(float t)
{
	SetUniform1(mTime, 1, (GLfloat*)&t, "mTime");

	return 0;
}

int GLShader::SetBoneTransforms(Matrix4* trans)
{
	if(trans != NULL)
	SetUniformMatrix4(uBones, MeshPrimitiveSkeletal::MaxBoneCount, GL_FALSE, (GLfloat*)trans, "uBones");

	return 0;
}

void GLShader::SetLightArray(Vector3* v, int lightCount)
{
	float lCount = lightCount;
	SetUniform3(mLightArray, lightCount, (GLfloat*)v, "mLightArray");
	SetUniform1(mLightCount, 1, (GLfloat*)&lCount, "mLightCount");
}

void GLShader::RenderMeshPrimitive(shared_ptr<MeshPrimitive> p, float elapsedTime)
{
	p->Bind(mvPositionHandle, mNormalHandle, mvUVHandle, mvBoneHandle, mvWeightHandle, (GLuint*)&mSampler2D, (GLuint*)&mBlendFactor);
	SetBoneTransforms(p->getBoneTransforms(elapsedTime));
	p->Render();
}