#include "GLRenderer.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Resource/MeshPrimitive.h"
#include "GLShader.h"

#include "Platform/Platform.h"

void printGLString(const char *name, GLenum s) {
	const char *v = (const char *) glGetString(s);
	Platform::instance().Logger().LogError(__FILE__, "GL %s = %s\n", name, v);
}

int checkGlError(const char* op) {

	int res = 0;
    for (GLint error = glGetError(); error; error
            = glGetError()) {
        res  =1;
		Platform::instance().Logger().LogError(__FILE__, "after %s() glError (0x%x)\n", op, error);
    }

    return res;
}

int GLRenderer::setViewport(float left, float top, float w, float h) {
	
	mViewport[0] = left;
	mViewport[1] = top;
	mViewport[2] = w;
	mViewport[3] = h;

	GLCHECK(glViewport(left, top, w, h));

    return 0;
}

int GLRenderer::PreRender(float elapsedTime, float deltaTime)
{
	Renderer::PreRender(elapsedTime, deltaTime);

	GLCHECK(glEnable(GL_DEPTH_TEST));
	GLCHECK(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	GLCHECK(glClearColor(0.0f, 0.5f, 0.3f, 1.0f));
	GLCHECK(glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT));

    return 0;
}

Vector3 GLRenderer::Unproject(Vector3& sc, Matrix4& model)
{
	Matrix4 mv = matrixMultiply(mViewMatrix, model);

	sc.y = mViewport[3] - sc.y;

	float o[3];
	unproject(	sc.x,sc.y,sc.z,
		mv.v, mProjectionMatrix.v, mViewport,
			&o[0], &o[1], &o[2]);

	return GV(o[0], o[1], o[2]);
}

Vector3 GLRenderer::Project(Vector3& obj, Matrix4& model)
{
	Matrix4 mv = mViewMatrix;

	GLfloat coords[] = { obj.x, obj.y, obj.z };
	GLfloat sc[3];
	project(coords[0], coords[1], coords[2],
		mv.v, mProjectionMatrix.v, mViewport,
			&sc[0], &sc[1], &sc[2]);

	sc[1] = mViewport[3] - sc[1];

	return GV(sc[0], sc[1], sc[2]);
}

int GLRenderer::SetTransparency(bool on)
{
	if (on)
	{
		GLCHECK(glEnable(GL_BLEND));
		GLCHECK(glDisable(GL_CULL_FACE));

	}
	else
	{
		GLCHECK(glDisable(GL_BLEND));
		GLCHECK(glEnable(GL_CULL_FACE));

	}

	return 0;
}
