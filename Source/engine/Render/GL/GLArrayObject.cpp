#include "GLArrayObject.h"
#include "GLRenderer.h"
#include "Core/OpenGL.h"
#include "Platform/Platform.h"

GLArrayObject::~GLArrayObject()
{
	if (glDeleteVertexArrays != NULL)
	{
		GLCHECK(glDeleteVertexArrays(1, &mVAO));
	}
}

int GLArrayObject::Allocate(IVertexArray* vertices, vector<unsigned short>* indices)
{
	if (glGenVertexArrays != NULL)
	{
		GLCHECK(glGenVertexArrays(1, &mVAO));

		if (glBindVertexArray != NULL)
		{
			GLCHECK(glBindVertexArray(mVAO));

			GLBufferObject::Allocate(vertices, indices);

			GLCHECK(glBindVertexArray(0));

			return 0;
		}
	}

	return -1;
}

int GLArrayObject::Render(vector<unsigned short>* indices)
{
	if (glBindVertexArray != NULL)
	{
		GLCHECK(glBindVertexArray(mVAO));

		GLBufferObject::Render(indices);

		GLCHECK(glBindVertexArray(0));

		return indices->size();
	}

	return 0;
}
