#include "GLTexture.h"
#include "GLRenderer.h"

GLTexture::GLTexture(string name, GLushort bpp, int width, int height, GLushort format, vector<unsigned char>& data, GLushort unit, GLushort cap)
:Texture(name, width, height, data)
,mId(-1)
,mBpp(bpp)
,mFormat(format)
,mUnit(unit)
,mCap(cap)
{
}

int GLTexture::TexImage2D(int width, int height, unsigned char* data)
{
	GLCHECK(glTexImage2D ( GL_TEXTURE_2D, 0, mFormat, mWidth, mHeight, 0, mFormat, GL_UNSIGNED_BYTE, &mData[0]));

	return 0;
}

int GLTexture::Allocate()
{
	GLCHECK(glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 ));
	GLCHECK(glGenTextures ( 1, &mId ));
	GLCHECK(glBindTexture ( mCap, mId ));

	GLboolean isTex = GLCHECK(glIsTexture(mId));
	if(isTex)
	{
		TexImage2D(mWidth, mHeight, &mData[0]);

		GLCHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
		GLCHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	}

	return 0;
}

int GLTexture::Bind(unsigned int* indx, int glsl_index)
{
	GLCHECK(glActiveTexture(mUnit));
	GLCHECK(glBindTexture(mCap, mId));

	GLuint handle = (GLuint)*indx;
	if(handle==-1)
	{
		return -1;
	}

	GLCHECK(glUniform1i(handle, glsl_index));
	return 0;
}
