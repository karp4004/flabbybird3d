#ifndef GLRendererH
#define GLRendererH

#include "Core/OpenGL.h"
#include "Render/Renderer.h"

class MeshPrimitive;
class GLRenderer: public Renderer
{
private:
	GLRenderer() {};

public:
	static GLRenderer& instance()
	{
		static GLRenderer INSTANCE;
		return INSTANCE;
	}

	virtual int setViewport(float left, float top, float w, float h);
	virtual int PreRender(float elapsedTime, float deltaTime);

	Vector3 Unproject(Vector3& v, Matrix4& model);
	Vector3 Project(Vector3& v, Matrix4& model);

	virtual int SetTransparency(bool on);
};

void printGLString(const char *name, GLenum s);
int checkGlError(const char* op);

#endif
