#ifndef GLTextureH
#define GLTextureH

#include "Core/OpenGL.h"
#include <Resource/Texture.h>

class GLTexture: public Texture
{
public:
	GLTexture(string name, GLushort bpp, int width, int height, GLushort format, vector<unsigned char>& data, GLushort unit, GLushort cap);

	virtual int Allocate();
	virtual int Bind(unsigned int* indx, int glsl_index);

	virtual int TexImage2D(int width, int height, unsigned char* data);

protected:

	GLuint mId;
	GLushort mBpp;
	GLushort mFormat;
	GLushort mUnit;
	GLushort mCap;
};

#endif
