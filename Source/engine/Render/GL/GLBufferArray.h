#ifndef GLBufferArrayH
#define GLBufferArrayH

#include "Scene/Vertex.h"
#include "Resource/MeshPrimitive.h"
#include "Resource/MeshPrimitiveSkeletal.h"
#include "Render/Buffer.h"

#include <string>

using namespace std;

class GLBufferArray: public Buffer
{
public:

	virtual int Render(vector<unsigned short>* indices);
	virtual MeshPrimitive::AABB GenerateBBox(IVertexArray* vertices, vector<Vector3>* positions);
	virtual int BindPositions(unsigned int indx, int vertex_stride, int position_stride, vector<Vector3>* positions);
	virtual int BindNormals(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, vector<Vector3>* normals);
	virtual int BindUVs(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, vector<Vector2>* uvs);
	virtual int BindBones(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, vector<BoneAttribute>* bones);
	virtual int BindWeights(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, int weight_stride, vector<WeightAttribute>* weights);
	virtual BufferTpe getBufferTpe() { return kArrayBT; }
};

#endif
