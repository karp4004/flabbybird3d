#include "Render/GL/GLRenderFactory.h"

#include "Render/GL/GLShader.h"
#include "Render/GL/GLTexture.h"
#include "Render/GL/GLTextureCompressed.h"
#include "Resource/MeshPrimitiveSkeletal.h"
#include "Platform/Platform.h"

static GLushort GLPixelFormat[] =
{
		GL_RGB,
		GL_RGBA,
		GL_ALPHA,
		GE_COMPRESSED,
		GE_COMPRESSED_RGB_FXT1_3DFX
};

static GLushort GLUnits[] =
{
		GL_TEXTURE0,
		GL_TEXTURE1,
		GL_TEXTURE2,
		GL_TEXTURE3
};

static GLushort GLCaps[] =
{
		GL_TEXTURE_2D
};

MeshPrimitive* GLRenderFactory::CreatePrimitive(string name, Buffer* buffer)
{
	EXCEPTION_TRY
		return new MeshPrimitive(name, buffer);
	EXCEPTION_CATCH
		return NULL;
}

MeshPrimitiveSkeletal* GLRenderFactory::CreatePrimitiveSkeletal(string name, Buffer* buffer)
{	
	EXCEPTION_TRY
		return new MeshPrimitiveSkeletal(name, buffer);
	EXCEPTION_CATCH
		return NULL;
}

Shader* GLRenderFactory::CreateShader(string name)
{	
	EXCEPTION_TRY
		return new GLShader(name);
	EXCEPTION_CATCH
		return NULL;
}

Texture* GLRenderFactory::CreateTexture(string name, PixelFormat bpp, int width, int height, PixelFormat format, vector<unsigned char>& data, TextureUnit unit, TextureCap cap)
{
	if(format > GE_COMPRESSED)
	{
		EXCEPTION_TRY
			return new GLTextureCompressed(name, GLPixelFormat[bpp], width, height, GLPixelFormat[format], data, GLUnits[unit], GLCaps[cap]);
		EXCEPTION_CATCH
	}
	else
	{
		EXCEPTION_TRY
			return new GLTexture(name, GLPixelFormat[bpp], width, height, GLPixelFormat[format], data, GLUnits[unit], GLCaps[cap]);
		EXCEPTION_CATCH
	}

	return NULL;
}
