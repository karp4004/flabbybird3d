#ifndef GELS2_RENDER_FACTORY
#define GELS2_RENDER_FACTORY

#include "Core/OpenGL.h"
#include "Render/RenderFactory.h"

#include "Resource/MeshPrimitive.h"
#include "Resource/Shader.h"
#include "Resource/Texture.h"

using namespace std;

class SignedPlace;

class GLRenderFactory: public RenderFactory
{
private:
	GLRenderFactory(){}

public:
	static GLRenderFactory& instance()
	{
		static GLRenderFactory INSTANCE;
		return INSTANCE;
	}

public:
	MeshPrimitive* CreatePrimitive(string name, Buffer* buffer);
	MeshPrimitiveSkeletal* CreatePrimitiveSkeletal(string name, Buffer* buffer);
	Shader* CreateShader(string name);
	Texture* CreateTexture(string name, PixelFormat bpp, int width, int height, PixelFormat format, vector<unsigned char>& data, TextureUnit unit, TextureCap cap);
};

#endif
