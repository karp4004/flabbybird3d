#include "GLBufferArray.h"
#include "GLRenderer.h"

#include "Core/OpenGL.h"
#include <sstream>
#include "Platform/Platform.h"

#include <algorithm>

int GLBufferArray::Render(vector<unsigned short>* indices)
{
	GLCHECK(glDrawElements(GL_TRIANGLES, indices->size(), GL_UNSIGNED_SHORT, &(*indices)[0]))

	return indices->size();
}

MeshPrimitive::AABB GLBufferArray::GenerateBBox(IVertexArray* vertices, vector<Vector3>* positions)
{
	MeshPrimitive::AABB ret;

	if (positions != NULL)
	{
		for (int i = 0; i < positions->size(); i++)
		{
			ret.max_left = max(ret.max_left, (*positions)[i].x);
			ret.min_right = min(ret.min_right, (*positions)[i].x);
			ret.max_up = max(ret.max_up, (*positions)[i].y);
			ret.min_down = min(ret.min_down, (*positions)[i].y);
			ret.max_far = max(ret.max_far, (*positions)[i].z);
			ret.min_near = min(ret.min_near, (*positions)[i].z);
		}
	}

	return ret;
}

int GLBufferArray::BindPositions(unsigned int indx, int vertex_stride, int position_stride, vector<Vector3>* positions)
{
	if (positions != NULL)
	{
		GLCHECK(glVertexAttribPointer(indx, position_stride, GL_FLOAT, GL_FALSE, 0, &(*positions)[0]));
		GLCHECK(glEnableVertexAttribArray(indx));
	}

	return 0;
}

int GLBufferArray::BindNormals(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, vector<Vector3>* normals)
{
	if (normals != NULL)
	{
		GLCHECK(glVertexAttribPointer(indx, normal_stride, GL_FLOAT, GL_FALSE, 0, &(*normals)[0]));
		GLCHECK(glEnableVertexAttribArray(indx));
	}

	return 0;
}

int GLBufferArray::BindUVs(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, vector<Vector2>* uvs)
{
	if (uvs != NULL)
	{
		GLCHECK(glVertexAttribPointer(indx, uv_stride, GL_FLOAT, GL_FALSE, 0, &(*uvs)[0]));
		GLCHECK(glEnableVertexAttribArray(indx));
	}

	return 0;
}

int GLBufferArray::BindBones(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, vector<BoneAttribute>* bones)
{
	if (bones != NULL)
	{
		GLCHECK(glVertexAttribPointer(indx, bone_stride, GL_FLOAT, GL_FALSE, 0, &(*bones)[0]));
		GLCHECK(glEnableVertexAttribArray(indx));
	}

	return 0;
}

int GLBufferArray::BindWeights(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, int weight_stride, vector<WeightAttribute>* weights)
{
	if (weights != NULL)
	{
		GLCHECK(glVertexAttribPointer(indx, weight_stride, GL_FLOAT, GL_FALSE, 0, &(*weights)[0]));
		GLCHECK(glEnableVertexAttribArray(indx));
	}

	return 0;
}