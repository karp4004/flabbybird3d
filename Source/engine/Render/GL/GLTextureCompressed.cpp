#include "GLTextureCompressed.h"
#include "GLRenderer.h"

GLTextureCompressed::GLTextureCompressed(string name, GLushort bpp, int width, int height, GLushort format, vector<unsigned char>& data, GLushort unit, GLushort cap)
:GLTexture(name, bpp,  width, height, format, data, unit, cap)
{
}

int GLTextureCompressed::TexImage2D(int width, int height, unsigned char* data)
{
	GLuint uiSize = 8 * ((mWidth + 3) >> 2) * ((mHeight + 3) >> 2);
	GLCHECK(glCompressedTexImage2D(GL_TEXTURE_2D, 0, mFormat,
				   mWidth, mHeight, 0, uiSize, &mData[0]));

		return 0;
}
