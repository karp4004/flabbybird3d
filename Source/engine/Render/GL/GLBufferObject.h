#ifndef GLBufferObjectH
#define GLBufferObjectH

#include "Core/OpenGL.h"
#include "Scene/Vertex.h"
#include "Resource/MeshPrimitive.h"
#include "Resource/MeshPrimitiveSkeletal.h"
#include "Render/Buffer.h"

#include <string>

using namespace std;

class GLBufferObject: public Buffer
{
public:
	GLBufferObject() {
		mVBO = 0;
		mIBO = 0;
	}

	virtual ~GLBufferObject();

	virtual int Bind();
	virtual int Render(vector<unsigned short>* indices);
	virtual MeshPrimitive::AABB GenerateBBox(IVertexArray* vertices, vector<Vector3>* positions);
	virtual int Allocate(IVertexArray* vertices, vector<unsigned short>* indices);
	virtual int BindPositions(unsigned int indx, int vertex_stride, int position_stride, vector<Vector3>* positions);
	virtual int BindNormals(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, vector<Vector3>* normals);
	virtual int BindUVs(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, vector<Vector2>* uvs);
	virtual int BindBones(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, vector<BoneAttribute>* bones);
	virtual int BindWeights(unsigned int indx, int vertex_stride, int position_stride, int normal_stride, int uv_stride, int bone_stride, int weight_stride, vector<WeightAttribute>* weights);
	virtual BufferTpe getBufferTpe() { return kVBOBT; }

private:
	GLuint mVBO;
	GLuint mIBO;
};

#endif
