#ifndef GLArrayObjectH
#define GLArrayObjectH

//#define GL_GLEXT_PROTOTYPES

#include "Core/OpenGL.h"
#include "GLBufferObject.h"

class GLArrayObject: public GLBufferObject
{
public:
	GLArrayObject() { mVAO = 0; }
	~GLArrayObject();

	virtual int Allocate(IVertexArray* vertices, vector<unsigned short>* indices);
	virtual int Render(vector<unsigned short>* indices);
	virtual BufferTpe getBufferTpe() { return kVAOBT; }

private:
	GLuint mVAO;
};

#endif
