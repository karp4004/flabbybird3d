#include "Renderer.h"

#include "Resource/MeshPrimitive.h"
#include "Platform/Platform.h"

shared_ptr<Shader> Renderer::GetShader()
{
	return activeShader;
}

int Renderer::SetShader(shared_ptr<Shader> shader)
{
	if (shader != NULL)
	{
		activeShader = shader;
		activeShader->UseProgram();
		activeShader->SetProjectionMatrix(mProjectionMatrix);
		activeShader->SetOrthoMatrix(mOrthoMatrix);
		activeShader->SetViewMatrix(mViewMatrix);
		activeShader->SetLightPosition(GV(mLightEyeSpace.x, mLightEyeSpace.y, mLightEyeSpace.z));
		activeShader->SetLightArray(mLightArrayView, mLightArray.size());
	}

	return 0;
}

int Renderer::SetShader(shared_ptr<Shader> shader, double time, Matrix4& model)
{
	if (shader != NULL)
	{
		SetShader(shader);

		activeShader->SetTime(time);

		mModelMatrix = model;

		mMVMatrix = matrixMultiply(mViewMatrix, mModelMatrix);
		mMVPMatrix = matrixMultiply(mProjectionMatrix, mMVMatrix);

		ExtractFrustum(mMVPMatrix);

		activeShader->SetColor(mColor);
		activeShader->SetMVMatrix(mMVMatrix);
		activeShader->SetMVPMatrix(mMVPMatrix);
		activeShader->SetModelMatrix(mModelMatrix);
	}

	return 0;
}

void Renderer::OffsetModelMatrix(Vector3& v)
{
	mModelMatrix.v[12] += v.x;
	mModelMatrix.v[13] += v.y;
	mModelMatrix.v[14] += v.z;
}

void Renderer::SetProjectionMatrix(Matrix4& m)
{
	mProjectionMatrix = m;
}

void Renderer::SetOrthoMatrix(Matrix4& m)
{
	mOrthoMatrix = m;
}

void Renderer::SetViewMatrix(Matrix4& m)
{
	mViewMatrix = m;

	mLightEyeSpace = GV(mLightPosition.x, mLightPosition.y, mLightPosition.z, 1.0);
	mLightEyeSpace = vectorMultiply(mViewMatrix, mLightEyeSpace);

	map<int, Vector3>::iterator mLightArray_it = mLightArray.begin();
	int i=0;
	while(mLightArray_it != mLightArray.end())
	{
		Vector4 l = GV(mLightArray_it->second.x, mLightArray_it->second.y, mLightArray_it->second.z, 1.0);
		l = vectorMultiply(mViewMatrix, l);
		mLightArrayView[i] = GV(l.x, l.y, l.z);
		i++;
		mLightArray_it++;
	}
}

void Renderer::SetEyePosition(Vector3& v)
{
	mEyePosition = v;
}

void Renderer::SetLightPosition(Vector3& v)
{
	mLightPosition = v;
}

void Renderer::UpdateLight(int l, Vector3& v)
{
    mLightArray[l] = v;
}

int Renderer::SetColor(Vector4& v)
{
	mColor = v;

	return 0;
}

int Renderer::PreRender(float elapsedTime, float deltaTime)
{
	if (elapsedTime >= 1.0)
	{
		secCounter++;

		mFPS = frameCounter;
		avgFPS = allCounter / secCounter;
		Platform::instance().Logger().LogError(__FILE__, "FPS:%f AVG:%f TRIS:%d/%d deltaTime:%f meshes:%d", mFPS, avgFPS, mTris, mTris / 3, deltaTime, _MeshRenderedCount);
		frameCounter = 0;
	}

	mTrisCount = mTris;
	_MeshRenderedCount = _MeshRendered;
	mTris = 0;
	_MeshRendered = 0;

	frameCounter++;
	allCounter++;

	return 0;
}

int Renderer::ExtractFrustum(Matrix4& mvp)
{
   float   t;

   /* ������� A, B, C, D ��� ������ ��������� */
   mFrustum.left.A = mvp.v[ 3] - mvp.v[ 0];
   mFrustum.left.B = mvp.v[ 7] - mvp.v[ 4];
   mFrustum.left.C = mvp.v[11] - mvp.v[ 8];
   mFrustum.left.D = mvp.v[15] - mvp.v[12];

   /* �������� ��������� ��������� � ����������� ���� */
   t = sqrt( mFrustum.left.A * mFrustum.left.A + mFrustum.left.B * mFrustum.left.B + mFrustum.left.C * mFrustum.left.C );
   mFrustum.left.A /= t;
   mFrustum.left.B /= t;
   mFrustum.left.C /= t;
   mFrustum.left.D /= t;

   /* ������� A, B, C, D ��� ����� ��������� */
   mFrustum.right.A = mvp.v[ 3] + mvp.v[ 0];
   mFrustum.right.B = mvp.v[ 7] + mvp.v[ 4];
   mFrustum.right.C = mvp.v[11] + mvp.v[ 8];
   mFrustum.right.D = mvp.v[15] + mvp.v[12];

   /* �������� ��������� ��������� � ����������� ���� */
   t = sqrt( mFrustum.right.A * mFrustum.right.A + mFrustum.right.B * mFrustum.right.B + mFrustum.right.C * mFrustum.right.C );
   mFrustum.right.A /= t;
   mFrustum.right.B /= t;
   mFrustum.right.C /= t;
   mFrustum.right.D /= t;

   /* ������� A, B, C, D ��� ������ ��������� */
   mFrustum.bottom.A = mvp.v[ 3] + mvp.v[ 1];
   mFrustum.bottom.B = mvp.v[ 7] + mvp.v[ 5];
   mFrustum.bottom.C = mvp.v[11] + mvp.v[ 9];
   mFrustum.bottom.D = mvp.v[15] + mvp.v[13];

   /* �������� ��������� ��������� � ����������� */
   t = sqrt( mFrustum.bottom.A * mFrustum.bottom.A + mFrustum.bottom.B * mFrustum.bottom.B + mFrustum.bottom.C * mFrustum.bottom.C );
   mFrustum.bottom.A /= t;
   mFrustum.bottom.B /= t;
   mFrustum.bottom.C /= t;
   mFrustum.bottom.D /= t;

   /* ������� ��������� */
   mFrustum.top.A = mvp.v[ 3] - mvp.v[ 1];
   mFrustum.top.B = mvp.v[ 7] - mvp.v[ 5];
   mFrustum.top.C = mvp.v[11] - mvp.v[ 9];
   mFrustum.top.D = mvp.v[15] - mvp.v[13];

   /* ���������� ��� */
   t = sqrt( mFrustum.top.A * mFrustum.top.A + mFrustum.top.B * mFrustum.top.B + mFrustum.top.C * mFrustum.top.C );
   mFrustum.top.A /= t;
   mFrustum.top.B /= t;
   mFrustum.top.C /= t;
   mFrustum.top.D /= t;

   /* ������ ��������� */
   mFrustum.mNear.A = mvp.v[ 3] - mvp.v[ 2];
   mFrustum.mNear.B = mvp.v[ 7] - mvp.v[ 6];
   mFrustum.mNear.C = mvp.v[11] - mvp.v[10];
   mFrustum.mNear.D = mvp.v[15] - mvp.v[14];

   /* ���������� ��� */
   t = sqrt( mFrustum.mNear.A * mFrustum.mNear.A + mFrustum.mNear.B * mFrustum.mNear.B + mFrustum.mNear.C * mFrustum.mNear.C );
   mFrustum.mNear.A /= t;
   mFrustum.mNear.B /= t;
   mFrustum.mNear.C /= t;
   mFrustum.mNear.D /= t;

   /* �������� ��������� */
   mFrustum.mFar.A = mvp.v[ 3] + mvp.v[ 2];
   mFrustum.mFar.B = mvp.v[ 7] + mvp.v[ 6];
   mFrustum.mFar.C = mvp.v[11] + mvp.v[10];
   mFrustum.mFar.D = mvp.v[15] + mvp.v[14];

   /* ���������� ��� */
   t = sqrt( mFrustum.mFar.A * mFrustum.mFar.A + mFrustum.mFar.B * mFrustum.mFar.B + mFrustum.mFar.C * mFrustum.mFar.C );
   mFrustum.mFar.A /= t;
   mFrustum.mFar.B /= t;
   mFrustum.mFar.C /= t;
   mFrustum.mFar.D /= t;

	return 0;
}

bool Renderer::SphereInFrustum(BSphere& sphere)
{
//	return true;

	if( mFrustum.left.A * sphere.center.x + mFrustum.left.B * sphere.center.y
			+ mFrustum.left.C * sphere.center.z + mFrustum.left.D <= -sphere.radius )
	{
		return false;
	}

	if( mFrustum.right.A * sphere.center.x + mFrustum.right.B * sphere.center.y
			+ mFrustum.right.C * sphere.center.z + mFrustum.right.D <= -sphere.radius )
	{
		return false;
	}

	if( mFrustum.bottom.A * sphere.center.x + mFrustum.bottom.B * sphere.center.y
			+ mFrustum.bottom.C * sphere.center.z + mFrustum.bottom.D <= -sphere.radius )
	{
		return false;
	}

	if( mFrustum.top.A * sphere.center.x + mFrustum.top.B * sphere.center.y
			+ mFrustum.top.C * sphere.center.z + mFrustum.top.D <= -sphere.radius )
	{
		return false;
	}

	if( mFrustum.mNear.A * sphere.center.x + mFrustum.mNear.B * sphere.center.y
			+ mFrustum.mNear.C * sphere.center.z + mFrustum.mNear.D <= -sphere.radius )
	{
		return false;
	}

	if( mFrustum.mFar.A * sphere.center.x + mFrustum.mFar.B * sphere.center.y
			+ mFrustum.mFar.C * sphere.center.z + mFrustum.mFar.D <= -sphere.radius )
	{
		return false;
	}

	return true;
}