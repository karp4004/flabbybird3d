#ifndef APPLICATION_FRAME_H
#define APPLICATION_FRAME_H

#include <string>
#include <list>
#include "Core/SharedPtr.h"

#include "Core/ObjectInTime.h"
#include "Scene/Camera.h"
#include "Scene/LightNode.h"

using namespace std;

class LightNode;
class ShaderNode;
class MeshNode;
class SceneNode;

class ApplicationFrame: public ObjectInTime
{
public:
	virtual int onCreateScene();
	virtual int onReleaseScene();
	virtual int onSurfaceCreated();
	virtual int onSurfaceChanged(float l, float t, float w, float h);
	virtual int onDrawFrame(double deltaTime);

protected:
	shared_ptr<SceneNode> _Root;
};

#endif
