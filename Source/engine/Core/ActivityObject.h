#ifndef ActivityObjectH
#define ActivityObjectH

#define EXCEPTION_TRY try {
#define EXCEPTION_CATCH  \
} \
catch (std::bad_alloc& ba) \
{ Platform::instance().Logger().LogError(__FILE__, "std::bad_alloc:%s", ba.what()); \
} \
catch (std::exception e) \
{ Platform::instance().Logger().LogError(__FILE__, "std::exception:%s", e.what()); \
} \
catch (...) \
{ Platform::instance().Logger().LogError(__FILE__, "Exception:%s", "Unhandled"); \
}

class ActivityObject
{
public:
	virtual int onSurfaceCreated() = 0;
	virtual int onSurfaceChanged(float l, float t, float w, float h) = 0;
	virtual int onDrawFrame(double deltaTime) = 0;
};

#endif
