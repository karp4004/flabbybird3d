#ifndef ResourceH
#define ResourceH

#include "SharedPtr.h"
#include <string>
#include "Core/FileStream.h"

using namespace std;

class Resource
{
public:
	Resource(string name) {
		_Name = name;
	};

	virtual int Allocate() = 0;
	virtual int OnAddResource(){return 0;};
	virtual void loadFromStream(shared_ptr<FileStream> fs) {};
	string GetName() { return _Name; }

protected:
	string _Name;
};

#endif
