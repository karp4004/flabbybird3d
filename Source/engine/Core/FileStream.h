#ifndef FileStreamH
#define FileStreamH

#include <string>
#include <vector>

using namespace std;

class File;
class FileStream
{
public:
	FileStream();
	FileStream(File* f);
	FileStream(const char* buff, int length);

	string ReadString();
	int GetBufferLength();
	int ReadMemory(void* buffer, int length);
	char ReadByte();
	int ReadInt();
	double ReadDouble();

private:
	int mLength;
	std::vector<char> mBuffer;
	int mToken;
};

#endif
