#include "Timer.h"

Timer::Timer():
mElapsed(0.)
,mDelta(0.)
{
}

double Timer::GetElapsed()
{
	return mElapsed;
}

double Timer::GetDelta()
{
	return mDelta;
}

void Timer::CountElapsed(float t)
{
	mElapsed += t;
}

void Timer::ResetElapsed()
{
	mElapsed = 0.;
}

void Timer::SetDelta(float t)
{
	mDelta = t;
}

int Timer::Reset()
{
	mElapsed = 0.;
	mDelta = 0.;

	return 0;
}
