#ifndef FILE_H
#define FILE_H

#include <string>

using namespace std;

enum FileOperation
{
	kFileWrite,
	kFileRead
};

class File
{
public:
	File(FileOperation op){};
	virtual ~File(){};

	virtual int Length() = 0;
	virtual int Read(char* buf, int len) = 0;
	virtual int Write(char* buf, int len) = 0;
};

#endif
