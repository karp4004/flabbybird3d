#ifndef FILEDIR_H
#define FILEDIR_H

#include <string>

using namespace std;

class FileDir
{
public:
	FileDir(){};
	virtual ~FileDir(){}
	virtual const char* GetNextFileName() = 0;
};

#endif
