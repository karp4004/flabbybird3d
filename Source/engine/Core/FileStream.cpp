#include "Core/FileStream.h"
#include "Core/File.h"
#include "Platform\Platform.h"
#include "Core/ActivityObject.h"
#include <stdlib.h>

FileStream::FileStream():
mLength(0)
,mToken(0)
{
}

FileStream::FileStream(File* f):
mLength(0)
,mBuffer(0)
,mToken(0)
{
	EXCEPTION_TRY

	int capacity = f->Length() + 1;

	if(capacity>1)
	{
		
		mBuffer.resize(capacity);

		while(1)
		{
			int to_read = capacity - mLength;

			if(to_read>1)
			{
				int readed = f->Read(&mBuffer[mLength], to_read);
				mLength += readed;
			}
			else
			{
				break;
			}
		}
	}

	EXCEPTION_CATCH
}

FileStream::FileStream(const char* buff, int length)
{
	EXCEPTION_TRY

		mLength = length;
		mBuffer.resize(mLength);
		std::copy(buff, buff + mLength, mBuffer.begin());

	EXCEPTION_CATCH
}

int FileStream::ReadMemory(void* buffer, int length)
{
	memcpy(buffer, &mBuffer[mToken], length);
	mToken += length;
	return length;
}

char FileStream::ReadByte()
{
	char value = mBuffer[mToken];
	mToken += sizeof(char);
	return value;
}

int FileStream::ReadInt()
{
	int value;
	memcpy(&value, &mBuffer[mToken], sizeof(int));
	mToken += sizeof(int);
	return value;
}

double FileStream::ReadDouble()
{
	double value;
	memcpy(&value, &mBuffer[mToken], sizeof(double));
	mToken += sizeof(double);
	return value;
}

int FileStream::GetBufferLength()
{
	return mLength;
}

string FileStream::ReadString()
{
	string ret;
	ret += &mBuffer[0];
	return ret;
}

int WriteMemory(File* f, void* buffer, int length)
{
	return 0;
}
