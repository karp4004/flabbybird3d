#ifndef ObjectInTimeH
#define ObjectInTimeH

#include "ActivityObject.h"

class ObjectInTime: public ActivityObject
{
public:
	ObjectInTime() { elapsedTime = 0.f; }

protected:
	float elapsedTime;
};

#endif
