#include "Core/Logger.h"

int log_raw(char* buf, int len)
{
	for(int i=0;i<len;i++)
	{
		log_prefix ANDROID_LOG_INFO, LOG_NAME "0x%x ", (unsigned char)buf[i]);
	}

	log_prefix ANDROID_LOG_INFO, LOG_NAME "\n");

	return 0;
}
