#ifndef TimerH
#define TimerH

class Timer
{
public:
	Timer();

	double GetElapsed();
	double GetDelta();
	virtual int Update() = 0;
	virtual int Reset() = 0;
	void ResetElapsed();

protected:
	void CountElapsed(float t);
	void SetDelta(float t);

private:
	double mElapsed;
	double mDelta;
};

#endif
