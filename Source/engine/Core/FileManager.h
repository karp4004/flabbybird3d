#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include "Core/FileDir.h"
#include "Core/File.h"
#include "Core/SharedPtr.h"

#include <string>

using namespace std;

class AAssetManager;

class FileManager
{
public:
	FileManager(){};

#if defined(_WIN32)
	virtual void setPath(string uri) = 0;
#else
	virtual void setAssets(AAssetManager* m) = 0;
#endif	

	virtual shared_ptr<FileDir> OpenDir(string name) = 0;
	virtual shared_ptr<File> OpenFile(string uri, FileOperation op) = 0;
	virtual int OpenFileDescriptor(string uri, int &start, int &length) = 0;
};

#endif
