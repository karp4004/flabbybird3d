#ifndef SceneNodeH
#define SceneNodeH

#include "Core/SharedPtr.h"

#include "Math/GEMath.h"
#include "Math/Line.h"
#include "Core/ObjectInTime.h"

#include "Resource/Shader.h"
#include "Resource/Mesh.h"
#include "Resource/Texture.h"

using namespace std;

class Scene;

class Moveable;
class ShaderNode;
class Camera;
class LightNode;
class MeshNode;

class SceneNode: public ObjectInTime
{
public:
	SceneNode():mHidden(false), _Released(false){}

	int AddChild(shared_ptr<SceneNode> child);
	virtual int removeNode(shared_ptr<SceneNode> child);
	int ChildCount() { return mChildren.size(); }

	virtual int onSurfaceCreated();
	virtual int onSurfaceChanged(float l, float t, float w, float h);
	virtual int onDrawFrame(double deltaTime);
	virtual int onReleaseScene();

	shared_ptr<SceneNode> addNode();
	shared_ptr<Camera> addCamera(Vector3& pos, Vector3& eye, Vector3& up, float fp);
	shared_ptr<Camera> addCamera(Matrix4& m, float fp);
	shared_ptr<LightNode> addLight(int id, Matrix4& pos);
	shared_ptr<MeshNode> addMesh(string meshName, Matrix4& origin, string shader, Vector3& scale);

	virtual int setHidden(bool v);

	virtual int Reset(Matrix4& origin);

protected:
	int dLine;
	bool mHidden;
	bool _Released;

private:
	list<shared_ptr<SceneNode>> mChildren;
};

#endif
