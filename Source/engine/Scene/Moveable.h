#ifndef MoveableH
#define MoveableH

#include "SceneNode.h"

enum CameraMode
{
	kCamNo,
	kCamFirst,
	kCamThird,
	kCamTarget
};

string CamModeName(CameraMode st);

class Moveable: public SceneNode
{
public:
	Moveable(Matrix4 origin = matrixIdentity());

	virtual int onDrawFrame(double deltaTime);
	virtual int UpdateCamera(double deltaTime);

	Matrix4& GetModelMatrix();
	void SetModelMatrix(Matrix4& m);

	Vector3 GetPosition();
	virtual void SetPosition(Vector3& v);

	virtual Vector3 GetDirection();
	virtual Vector3 GetSide();
	virtual Vector3 GetUp();

	virtual int Move(Vector3& v);
	int Translate(Vector3& v);
	virtual int Rotate(Vector3& v);
	int Scale(Vector3& v);
	Vector3 GetScale();

	int Attach(shared_ptr<Moveable> to);
	int AttachFull(shared_ptr<Moveable> to);

	int setCameraMode(CameraMode m);
	int setTargetOffset(Vector4& offset);

	virtual void RoundedPathMoveStart() {};
	virtual void RoundedPathMoveFinish() {};

protected:
	Matrix4 mModelMatrix;
	Matrix4 mScale;

	shared_ptr<Moveable> mAttachedTo;
	shared_ptr<Moveable> mAttachedFull;

	CameraMode CamMode;

	float targetViewAngle;
	float targetViewAngleZ;

	bool stopUpdate;

	Vector4 _CamThirdOffset;
};

#endif
