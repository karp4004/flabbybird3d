#include "Camera.h"
#include "Platform/Platform.h"

#include <sstream>

using namespace std;

Camera::Camera(float fp):
farPlane(fp)
,zoomSpeed(0.)
,zoomAccel(0.)
{
}

int Camera::onSurfaceChanged(float l, float t, float w, float h)
{
	float aspect = (float)w/(float)h;

    mProjectMatrix = getPerspective(30, aspect, 1.0f, farPlane);

	float left = 0.0f;
	float right = 1.0f;
	float bottom = 0.0f;
	float top = 1.0f;
	float near3d = 0.0f;
	float far3d = 1.0f;
    mOrthoMatrix = getOrtho(left, right, bottom, top, near3d, far3d);

    Platform::instance().Renderer().SetProjectionMatrix(mProjectMatrix);
	Platform::instance().Renderer().SetOrthoMatrix(mOrthoMatrix);

    return Moveable::onSurfaceChanged(l, t, w, h);
}

int Camera::onDrawFrame(double dt)
{
	if ((zoomSpeed > 0 && zoomAccel < 0)
		|| (zoomSpeed < 0 && zoomAccel > 0))
	{
		zoomSpeed += zoomAccel*dt;
		float zoom = zoomSpeed*dt;
		Vector3 v = GV(0, 0, zoom);
		Move(v);
	}

	Vector3 eye = GV(0., 0., 0.);
	Platform::instance().Renderer().SetEyePosition(eye);

    return 	Moveable::onDrawFrame(dt);
}

int Camera::UpdateCamera(double dt)
{
	switch(CamMode)
	{
		case kCamNo:
			break;

		case kCamFirst:
		case kCamThird:
		case kCamTarget:
			Platform::instance().Renderer().SetViewMatrix(mModelMatrix);
			break;
	}

	return 0;
}

int Camera::LookAt(Vector3 eye, Vector3 center, Vector3 up)
{
	mModelMatrix = getLookAtM(eye, center, up);
	return 0;
}

int Camera::SetTarget(shared_ptr<Moveable> target)
{
	mTarget = target;
	return 0;
}

void Camera::Zoom(int in)
{
	zoomSpeed = 100 * in;
	zoomAccel = 150*-in;
}

