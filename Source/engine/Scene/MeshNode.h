#ifndef MeshNodeH
#define MeshNodeH

#include <vector>
#include "Resource/Mesh.h"
#include "Scene/Moveable.h"
#include "Resource/Shader.h"

enum CulllingType
{
	kSphereCull,
	kScreenCull
};

class MeshNode: public Moveable
{
public:
	MeshNode(shared_ptr<Mesh> mesh, shared_ptr<Shader> shader, Matrix4 origin = matrixIdentity());

	virtual int onDrawFrame(double deltaTime);

	Vector3 GetDimensions();

	void setColor(Vector4 c) { mColor = c; }
	void setShader(shared_ptr<Shader> s) { mShader = s; }
	void setCullingType(CulllingType t) { _CulllingType = t; }
	Vector3 getScrRightBorder();
	Vector3 getScrLeftBorder();
	virtual bool checkOutLeft();
	virtual bool checkOutRight();

protected:
	shared_ptr<Mesh> mMesh;
	shared_ptr<Shader> mShader;
	bool mPlaying;
	Vector4 mColor;
	CulllingType _CulllingType;
};

#endif
