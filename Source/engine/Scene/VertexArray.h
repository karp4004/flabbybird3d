#ifndef VertexArrayH
#define VertexArrayH

#include "Math/GEMath.h"

#include <vector>

using namespace std;

#define WEIGHT_COUNT 4

class IVertexArray
{
public:
	virtual void setCount(int c) = 0;
	virtual int getCount() = 0;
	virtual void* getVertices() = 0;
	virtual Vector3 getPosition(int idx) = 0;
	virtual void setBone(int idx, int bidx, float b) = 0;
	virtual void setWeight(int idx, int widx, float w) = 0;
	virtual int getVertexSize() = 0;
};

template<class V>
class VertexArray: public IVertexArray
{
public:
	virtual void setCount(int c) { mVertices.resize(c); }
	virtual int getCount() { return mVertices.size(); }
	virtual void* getVertices() { return &mVertices[0]; }
	virtual Vector3 getPosition(int idx) { return mVertices[idx].position; }
	virtual void setBone(int idx, int bidx, float b) { mVertices[idx].setBone(bidx, b); }
	virtual void setWeight(int idx, int widx, float w) { mVertices[idx].setWeight(widx, w); }
	virtual void setVertices(vector<V>* verts) { mVertices = *verts; }
	virtual int getVertexSize() { return sizeof(V); }

	vector<V> mVertices;
};

#endif
