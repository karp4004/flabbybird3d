#include "Moveable.h"
#include "Platform/Platform.h"

#include <sstream>

using namespace std;

static string modeNmaes[] =
{
		"kCamNo",
		"kCamFirst",
		"kCamThird",
		"kCamTarget"
};

string CamModeName(CameraMode m)
{
	return modeNmaes[m];
}

Moveable::Moveable(Matrix4 origin)
:mAttachedTo(0)
,targetViewAngle(0.)
,targetViewAngleZ(0.)
,stopUpdate(false)
,CamMode(kCamNo)
, _CamThirdOffset(GV(0., -100., 100., 1.0))
{
	mModelMatrix = origin;

	mScale = MI();
}

int Moveable::onDrawFrame(double deltaTime)
{
	if(mAttachedFull)
	{
		SetModelMatrix(mAttachedFull->GetModelMatrix());
	}
	else if(mAttachedTo)
	{
		Vector3 p = mAttachedTo->GetPosition();
		SetPosition(p);
	}

	UpdateCamera(deltaTime);

	return SceneNode::onDrawFrame(deltaTime);
}

int Moveable::UpdateCamera(double deltaTime)
{
	switch(CamMode)
	{
		case kCamNo:
			break;

		case kCamFirst:
			{
				Vector3 eye = GetPosition();
				Vector3 center = GetPosition() + GetDirection();
				Vector3 up = GetUp();
				Matrix4 view = getLookAtM(eye, center, up);
				Platform::instance().Renderer().SetViewMatrix(view);
			}
			break;

		case kCamThird:
			{
				Vector3 eye = GetPosition() - GetDirection()*8 + GetUp()*4;
				Vector3 center = GetPosition();
				Vector3 up = GetUp();
				Matrix4 view = getLookAtM(eye, center, up);
				Platform::instance().Renderer().SetViewMatrix(view);
			}
			break;

		case kCamTarget:
			{
				Matrix4 vam = matrixRotateY(targetViewAngle);
				Matrix4 vamz = matrixRotateX(targetViewAngleZ);
				vam = matrixMultiply(vam, vamz);

				Vector4 off = vectorMultiply(vam, _CamThirdOffset);

				Vector3 eye = GetPosition() - GV(off.x, off.y, off.z);
				Vector3 center = GetPosition();
				Vector3 up = GetUp();
				Matrix4 view = getLookAtM(eye, center, up);

				Platform::instance().Renderer().SetViewMatrix(view);
			}
			break;
	}

	return 0;
}

Matrix4& Moveable::GetModelMatrix()
{
	return mModelMatrix;
}

void Moveable::SetModelMatrix(Matrix4& m)
{
	mModelMatrix = m;
}

Vector3 Moveable::GetPosition()
{
	Vector3 position;

	position.x = mModelMatrix.v[12];
	position.y = mModelMatrix.v[13];
	position.z = mModelMatrix.v[14];

	return position;
}

void Moveable::SetPosition(Vector3& v)
{
	mModelMatrix.v[12] = v.x;
	mModelMatrix.v[13] = v.y;
	mModelMatrix.v[14] = v.z;
}

Vector3 Moveable::GetDirection()
{
	Vector3 dir;

	dir.x = mModelMatrix.v[2];
	dir.y = mModelMatrix.v[6];
	dir.z = mModelMatrix.v[10];

	dir = normalize(dir);

	return dir;
}

Vector3 Moveable::GetSide()
{
	Vector3 side;

	side.x = mModelMatrix.v[0];
	side.y = mModelMatrix.v[4];
	side.z = mModelMatrix.v[8];

	side = normalize(side);

	return side;
}

Vector3 Moveable::GetUp()
{
	Vector3 up;

	up.x = mModelMatrix.v[1];
	up.y = mModelMatrix.v[5];
	up.z = mModelMatrix.v[9];

	up = normalize(up);

	return up;
}

int Moveable::Scale(Vector3& v)
{
	mScale.v[0] *= v.x;
	mScale.v[5] *= v.y;
	mScale.v[10] *= v.z;

	return 0;
}

Vector3 Moveable::GetScale()
{
	return GV(mScale.v[0], mScale.v[5], mScale.v[10]);
}

int Moveable::Move(Vector3& v)
{
	mModelMatrix.v[12] += v.x;
	mModelMatrix.v[13] += v.y;
	mModelMatrix.v[14] += v.z;

	return 0;
}

int Moveable::Translate(Vector3& v)
{
	mModelMatrix = translateM(mModelMatrix, v.x, v.y, v.z);

	return 0;
}

int Moveable::Rotate(Vector3& v)
{
	Matrix4 ox = matrixRotateX(v.x);
	Matrix4 oy = matrixRotateY(v.y);
	Matrix4 oz = matrixRotateZ(v.z);

	mModelMatrix = matrixMultiply(mModelMatrix, ox);
	mModelMatrix = matrixMultiply(mModelMatrix, oy);
	mModelMatrix = matrixMultiply(mModelMatrix, oz);

	return 0;
}

int Moveable::Attach(shared_ptr<Moveable> to)
{
	if(!to)
	{
		return -1;
	}

	mAttachedTo = to;
	return 0;
}

int Moveable::AttachFull(shared_ptr<Moveable> to)
{
	mAttachedFull = to;

	return 0;
}

int Moveable::setCameraMode(CameraMode m)
{
	CamMode = m;

	return 0;
}

int Moveable::setTargetOffset(Vector4& offset)
{
	_CamThirdOffset = offset;

	return 0;
}