#include "Scene/LightNode.h"

#include "Platform/Platform.h"

LightNode::LightNode(int id, Matrix4 origin, LightType type, float attenuation)
:Moveable(origin)
,lightType(type)
,mAttenuation(attenuation)
, _Id(id)
{
}

int LightNode::onDrawFrame(double deltaTime)
{
	Moveable::onDrawFrame(deltaTime);

	Matrix4 mLightModelMatrix = GetModelMatrix();
	Vector3 lightPos = GV(mLightModelMatrix.v[12], mLightModelMatrix.v[13], mLightModelMatrix.v[14]);
	Platform::instance().Renderer().SetLightPosition(lightPos);
	Platform::instance().Renderer().UpdateLight(_Id, lightPos);

	return 0;
}
