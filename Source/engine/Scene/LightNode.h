#ifndef LightNodeH
#define LightNodeH

#include "Scene/Moveable.h"

enum LightType
{
	lIGHT_SPOT,
	LIGHT_DIRECTIONAL
};

class LightNode: public Moveable
{
public:
	LightNode(int id, Matrix4 origin, LightType type = lIGHT_SPOT, float attenuation = 5.);
	virtual int onDrawFrame(double deltaTime);

private:
	LightType lightType;
	float mAttenuation;
	int _Id;
};

#endif
