#include "SceneNode.h"

#include "ApplicationFrame.h"

#include "Scene/Camera.h"
#include "Scene/LightNode.h"
#include "Scene/MeshNode.h"
#include "Resource/Texture.h"
#include "Resource/MeshPrimitive.h"
#include "Platform/Platform.h"

int SceneNode::Reset(Matrix4& origin)
{
	return 0;
}

int SceneNode::removeNode(shared_ptr<SceneNode> child)
{
	EXCEPTION_TRY

		list<shared_ptr<SceneNode>>::iterator mChildren_it = mChildren.begin();
		while (mChildren_it != mChildren.end())
		{
			if ((*mChildren_it) == child)
			{
				mChildren_it = mChildren.erase(mChildren_it);
			}

			mChildren_it++;
		}

	EXCEPTION_CATCH

	return 0;
}

int SceneNode::setHidden(bool v)
{
	mHidden = v;
	return 0;
}

int SceneNode::AddChild(shared_ptr<SceneNode> child)
{
	EXCEPTION_TRY

		mChildren.push_back(child);

	EXCEPTION_CATCH

	return 0;
}

int SceneNode::onSurfaceCreated()
{
	list<shared_ptr<SceneNode>>::iterator mChildren_it = mChildren.begin();
	while(mChildren_it != mChildren.end())
	{
		(*mChildren_it)->onSurfaceCreated();
		mChildren_it++;
	}

	return 0;
}

int SceneNode::onSurfaceChanged(float l, float t, float w, float h)
{
	list<shared_ptr<SceneNode>>::iterator mChildren_it = mChildren.begin();
	while(mChildren_it != mChildren.end())
	{
		(*mChildren_it)->onSurfaceChanged(l, t, w, h);
		mChildren_it++;
	}

	return 0;
}

int SceneNode::onDrawFrame(double deltaTime)
{
	if(!mHidden && !_Released)
	{		
		list<shared_ptr<SceneNode>>::iterator mChildren_it = mChildren.begin();
		while(mChildren_it != mChildren.end())
		{
			if (_Released)
			{
				break;
			}

			(*mChildren_it)->onDrawFrame(deltaTime);
			mChildren_it++;
		}
	}

	return 0;
}

int SceneNode::onReleaseScene()
{
	Platform::instance().Logger().LogInfo(__FILE__, "Released:%p", this);

	_Released = true;

	list<shared_ptr<SceneNode>>::iterator mChildren_it = mChildren.begin();
	while (mChildren_it != mChildren.end())
	{
		(*mChildren_it)->onReleaseScene();
		mChildren_it++;
	}

	mChildren.clear();

	return 0;
}

shared_ptr<SceneNode> SceneNode::addNode()
{
	EXCEPTION_TRY
		shared_ptr<SceneNode> child(new SceneNode());
		mChildren.push_back(child);
		return child;
	EXCEPTION_CATCH

		return NULL;
}

shared_ptr<Camera> SceneNode::addCamera(Vector3& pos, Vector3& eye, Vector3& up, float fp)
{
	EXCEPTION_TRY
		shared_ptr<Camera> node(new Camera(fp));
		node->LookAt(pos, eye, up);
		mChildren.push_back(node);
		return node;
	EXCEPTION_CATCH

		return NULL;
}

shared_ptr<Camera> SceneNode::addCamera(Matrix4& m, float fp)
{
	EXCEPTION_TRY
		shared_ptr<Camera> node(new Camera(fp));
		node->SetModelMatrix(m);
		mChildren.push_back(node);
		return node;
	EXCEPTION_CATCH

		return NULL;
}

shared_ptr<LightNode> SceneNode::addLight(int id, Matrix4& pos)
{
	EXCEPTION_TRY
		shared_ptr<LightNode> node(new LightNode(id, pos));
		mChildren.push_back(node);
		return node;
	EXCEPTION_CATCH

		return NULL;
}

shared_ptr<MeshNode> SceneNode::addMesh(string meshName, Matrix4& origin, string shader, Vector3& scale)
{
	EXCEPTION_TRY
		shared_ptr<Mesh> mesh = Platform::instance().ResourceProvider().GetMesh(meshName);
		if(mesh)
		{
			shared_ptr<Shader> sh = Platform::instance().ResourceProvider().GetShader(shader);
			if (sh != NULL)
			{
				shared_ptr<MeshNode> meshNode(new MeshNode(mesh, sh, origin));
				meshNode->Scale(scale);
				mChildren.push_back(meshNode);
				return meshNode;
			}
		}
		else
		{
			Platform::instance().Logger().LogInfo(__FILE__, "%s:no mesh", meshName.c_str());
		}	
	
	EXCEPTION_CATCH

	return 0;
}