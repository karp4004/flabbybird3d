#include "MeshNode.h"
#include "ApplicationFrame.h"
#include "Platform/Platform.h"
#include "Scene/Camera.h"
#include <sstream>
#include <algorithm>

#if defined(_WIN32)
#include <windows.h>
#endif

MeshNode::MeshNode(shared_ptr<Mesh> mesh, shared_ptr<Shader> shader, Matrix4 origin)
:Moveable(origin)
, mMesh(mesh)
, mColor(GV(1, 1, 1, 1))
, mPlaying(true)
, _CulllingType(kSphereCull)
{
	mShader = shader;
}

Vector3 MeshNode::getScrRightBorder()
{
	Vector3 pos = GetPosition();
	pos.x += GetDimensions().x / 2;
	Matrix4 m;
	return Platform::instance().Renderer().Project(pos, m);
}

Vector3 MeshNode::getScrLeftBorder()
{
	Vector3 pos = GetPosition();
	pos.x -= GetDimensions().x / 2;
	Matrix4 m;
	return Platform::instance().Renderer().Project(pos, m);
}

bool MeshNode::checkOutLeft()
{
	EXCEPTION_TRY

	int* vp = Platform::instance().Renderer().getViewport();
	if (vp != NULL)
	{
		Vector3 src = getScrRightBorder();
		if (src.x <= vp[0])
		{
			return true;
		}
	}

	EXCEPTION_CATCH

	return false;
}

bool MeshNode::checkOutRight()
{
	EXCEPTION_TRY

		int* vp = Platform::instance().Renderer().getViewport();
	if (vp != NULL)
	{
		Vector3 src = getScrLeftBorder();
		if (src.x >= vp[2])
		{
			return true;
		}
	}

	EXCEPTION_CATCH

		return false;
}

int MeshNode::onDrawFrame(double dt)
{
	if(!mHidden)
	{
		elapsedTime += dt;

		Moveable::onDrawFrame(dt);

		Matrix4 m = matrixMultiply(mModelMatrix, mScale);
		Platform::instance().Renderer().SetShader(mShader, elapsedTime, m);
		mShader->SetColor(mColor);

		BSphere& bs = mMesh->GetBSphere();
		bool notcull = true;
		if (_CulllingType == kSphereCull)
		{
			notcull = Platform::instance().Renderer().SphereInFrustum(bs);
		}
		else if (_CulllingType == kScreenCull)
		{
			notcull = !checkOutLeft() && !checkOutRight();
		}

		if(notcull)
		{
			Platform::instance().Renderer().addMesh();

			vector<shared_ptr<MeshPrimitive> >::iterator mPrimitiveList_it = mMesh->mPrimitiveList.begin();
			while (mPrimitiveList_it != mMesh->mPrimitiveList.end())
			{
				Platform::instance().Renderer().addStatTris((*mPrimitiveList_it)->IndicesNum());

				if (elapsedTime > mPrimitiveList_it->get()->AnimationDur() && mPlaying)
				{
					elapsedTime = 0.;
				}

				mShader->RenderMeshPrimitive((*mPrimitiveList_it), elapsedTime);

				mPrimitiveList_it++;
			}
		}
	}

	return 0;
}

Vector3 MeshNode::GetDimensions()
{
	BBox bbox = mMesh->GetBBox();

	return GV(bbox.width,bbox.height,bbox.length);
}