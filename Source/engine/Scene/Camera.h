#ifndef CameraH
#define CameraH

#include "Moveable.h"
#include "Core/SharedPtr.h"
#include "Math/Vector3.h"

class Camera: public Moveable
{
public:
	Camera(float fp);

	int LookAt(Vector3 eye, Vector3 center, Vector3 up);
	int SetTarget(shared_ptr<Moveable> target);

	virtual int onSurfaceChanged(float l, float t, float w, float h);
	virtual int onDrawFrame(double deltaTime);

	virtual int UpdateCamera(double deltaTime);

	void Zoom(int in);

private:
	Matrix4 mProjectMatrix;
	Matrix4 mOrthoMatrix;

	shared_ptr<Moveable> mTarget;

	float farPlane;
	float zoomSpeed;
	float zoomAccel;
};

#endif
