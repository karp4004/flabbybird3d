#ifndef VertexH
#define VertexH

#include "Math/GEMath.h"

#define WEIGHT_COUNT 4

class Vertex
{
public:

	void setBone(int bidx, float b) {}
	void setWeight(int widx, float w) {}

	Vector3 position;
	Vector3 normal;
	Vector2 uv;
};

class VertexSkeletal
{
public:

	VertexSkeletal()
	{
		memset(mBoneIds, 0, sizeof(mBoneIds));
		memset(mWeights, 0, sizeof(mWeights));
	}

	void setBone(int bidx, float b) { mBoneIds[bidx] = b; }
	void setWeight(int widx, float w) { mWeights[widx] = w; }

	Vector3 position;
	Vector3 normal;
	Vector2 uv;
	float mBoneIds[4];
	float mWeights[4];
};

#endif
