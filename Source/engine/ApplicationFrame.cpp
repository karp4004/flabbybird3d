#include "ApplicationFrame.h"
#include "Platform/Platform.h"

#include <sstream>

#include "Scene/LightNode.h"
#include "Scene/MeshNode.h"
#include "Scene/SceneNode.h"

#include "Core/OpenGL.h"
#include "Core/ObjectInTime.h"

int ApplicationFrame::onReleaseScene()
{
	if (_Root != NULL)
	{
		_Root->onReleaseScene();
	}

	return 0;
}

int ApplicationFrame::onCreateScene()
{
	EXCEPTION_TRY
		_Root.reset(new SceneNode());
	EXCEPTION_CATCH

	return 0;
}

int ApplicationFrame::onSurfaceCreated()
{
	Platform::instance().Timer().Reset();
	Platform::instance().ResourceProvider().onSurfaceCreated();
	_Root->onSurfaceCreated();

	return 0;
}

int ApplicationFrame::onSurfaceChanged(float l, float t, float w, float h)
{
	if (_Root != NULL)
	{
		_Root->onSurfaceChanged(l, t, w, h);
	}

    Platform::instance().Renderer().setViewport(l, t, w, h);

	return 0;
}


int ApplicationFrame::onDrawFrame(double dt)
{
	elapsedTime += dt;

	Platform::instance().Renderer().PreRender(elapsedTime, dt);

	if (_Root != NULL)
	{
		_Root->onDrawFrame(dt);
	}

	if (elapsedTime >= 1.0)
	{
		elapsedTime = 0.;
	}

    return 0;
}