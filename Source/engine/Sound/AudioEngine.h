#ifndef AudioEngineH
#define AudioEngineH

#include <string>
#include "Core/SharedPtr.h"

using namespace std;

struct AudioPlayer
{
	virtual int setVolume(int millibel) = 0;
	virtual int setPlayingAssetAudioPlayer(bool isPlaying) = 0;
};

class AudioEngine
{
public:
	virtual int CreateEngine() = 0;
	virtual int createBufferQueueAudioPlayer() = 0;
	virtual shared_ptr<AudioPlayer> createAssetAudioPlayer(string filename, bool loop) = 0;
};

#endif
