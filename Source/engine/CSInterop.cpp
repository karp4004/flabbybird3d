#include "CSInterop.h"
#include "ApplicationFrame.h"
#include "Platform/Platform.h"
#include "FlabbyBird3D.h"

#ifdef __cplusplus
extern "C" {
#endif

	DLLEXPORT int STDCALL ReleaseScene()
	{
		return FlabbyBird3D::instance().onReleaseScene();
	}

	DLLEXPORT int STDCALL OnSurfaceCreated()
	{
		return FlabbyBird3D::instance().onSurfaceCreated();
	}

	DLLEXPORT int STDCALL OnSurfaceChanged(int width, int height)
	{
		return  FlabbyBird3D::instance().onSurfaceChanged(0, 0, width, height);
	}

	DLLEXPORT int STDCALL OnDrawFrame()
	{
		Platform::instance().Timer().Update();
		double deltaTime = Platform::instance().Timer().GetDelta();
		return FlabbyBird3D::instance().onDrawFrame(deltaTime);
	}

	DLLEXPORT int STDCALL GetFPS()
	{
		return Platform::instance().Renderer().getFPS();
	}

	DLLEXPORT int STDCALL GetFaces()
	{
		return Platform::instance().Renderer().getFaces();
	}

	DLLEXPORT void STDCALL Zoom(Camera* m, int in)
	{
		m->Zoom(in);
	}

	DLLEXPORT Vector3 STDCALL Unproject(Camera* cam, float x, float y)
	{
		Matrix4 m = MI();// cam->GetModelMatrix();

		Platform::instance().Logger().LogInfo(__FILE__, "p:%f/%f", x, y);

		Platform::instance().Logger().LogInfo(__FILE__, "m:%f/%f/%f/%f/%f/%f/%f/%f/%f/%f/%f/%f/%f/%f/%f/%f",
			m.v[0], m.v[1], m.v[0], m.v[3], m.v[4], m.v[5], m.v[6], m.v[7],
			m.v[8], m.v[9], m.v[10], m.v[11], m.v[12], m.v[13], m.v[14], m.v[15]);


		Vector3 nearsc = GV(x, y, 0.0);
		Vector3 farsc = GV(x, y, 1.0);
		Vector3 near3d = Platform::instance().Renderer().Unproject(nearsc, m);
		Vector3 far3d = Platform::instance().Renderer().Unproject(farsc, m);

		Platform::instance().Logger().LogInfo(__FILE__, "near:%f/%f/%f", near3d.x, near3d.y, near3d.z);
		Platform::instance().Logger().LogInfo(__FILE__, "far:%f/%f/%f", far3d.x, far3d.y, far3d.z);

		Vector3 ret;
		//linePlaneIntersection(ret, ray, near, GV(0,1,0), GV(100, 0, 100));

		Vector3 p1 = GV(0, 1, 0);
		Vector3 p2 = GV(100, 0, 100);
		Plane plane(p1, p2);
		Ray ray(near3d, far3d);

		intersectPlane(plane, ray, ret);

		Platform::instance().Logger().LogInfo(__FILE__, "ret:%f/%f/%f", ret.x, ret.y, ret.z);

		return ret;
	}

	DLLEXPORT void STDCALL CameraSlide(Camera* node, Vector2 slide)
	{
		Vector3 dir = node->GetDirection();
		dir.y = 0;
		dir = normalize(dir)*slide.y;
		node->Translate(dir);

		Vector3 v = GV(slide.x, 0, 0);
		node->Move(v);
	}

	DLLEXPORT void STDCALL MoveableMove(Moveable* m, float x, float y, float z)
	{
		Vector3 v = GV(x, y, z);
		m->Move(v);
	}

	DLLEXPORT void STDCALL MoveableTranslate(Moveable* m, float x, float y, float z)
	{
		Vector3 v = GV(x, y, z);
		m->Translate(v);
	}

	DLLEXPORT void STDCALL MeshSetColor(MeshNode* n, Vector4 col)
	{
		n->setColor(col);
	}

	DLLEXPORT void STDCALL MoveableRotate(Moveable* n, Vector3 rot)
	{
		n->Rotate(rot);
	}

	DLLEXPORT Vector3 STDCALL MoveablePosition(Moveable* n)
	{
		return n->GetPosition();
	}

	DLLEXPORT void STDCALL MeshSetShader(MeshNode* n, const char* shader)
	{
		n->setShader(Platform::instance().ResourceProvider().GetShader(shader));
	}

	DLLEXPORT void STDCALL OnTouchScreen()
	{
		return FlabbyBird3D::instance().onTouchScreen();
	}

#ifdef __cplusplus
}
#endif