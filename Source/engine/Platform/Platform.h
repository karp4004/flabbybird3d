#ifndef PlatformH
#define PlatformH

#include "Render\GL\GLRenderer.h"
#include "Render\GL\GLRenderFactory.h"
#include "Resource\ResourceProvider.h"

#if defined(_WIN32)
#include "WinAudioEngine.h"
#include "WinTimer.h"
#include "WinFileManager.h"
#include "WinInterop.h"
#include "WinLogger.h"
#else
#include "AndroidAudioEngine.h"
#include "PosixTimer.h"
#include "AndroidFileManager.h"
#include "AndroidInterop.h"
#include "AndroidLogger.h"
#endif

class Renderer;

enum GraphicAPI
{
	kOpenGL
};

class Platform
{
private:
	Platform()
	{
		_api = kOpenGL;
	}

public:
	static Platform& instance()
	{
		static Platform INSTANCE;
		return INSTANCE;
	}

	Renderer& Renderer()
	{
		switch (_api)
		{
			case kOpenGL:
				return GLRenderer::instance();

			default:
				return GLRenderer::instance();
		}		
	}

	RenderFactory& RenderFactory()
	{
		switch (_api)
		{
		case kOpenGL:
			return GLRenderFactory::instance();

		default:
			return GLRenderFactory::instance();
		}
	}

	ResourceProvider& ResourceProvider()
	{
		return ResourceProvider::instance();
	}

	AudioEngine& AudioEngine()
	{
		#if defined(_WIN32)
		return WinAudioEngine::instance();
		#else
		return AndroidAudioEngine::instance();
		#endif
	}

	Timer& Timer()
	{
#if defined(_WIN32)
		return WinTimer::instance();
#else
		return PosixTimer::instance();
#endif
	}

	FileManager& FileManager()
	{
#if defined(_WIN32)
		return WinFileManager::instance();
#else
		return AndroidFileManager::instance();
#endif
	}

	Interop& Interop()
	{
#if defined(_WIN32)
		return WinInterop::instance();
#else
		return AndroidInterop::instance();
#endif
	}

	Logger& Logger()
	{
#if defined(_WIN32)
		return WinLogger::instance();
#else
		return AndroidLogger::instance();
#endif
	}

private:
	GraphicAPI _api;
};

#endif