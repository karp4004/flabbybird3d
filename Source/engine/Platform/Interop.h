#ifndef InteropH
#define InteropH

class Interop
{
public:
	virtual void ResourcesLoaded() = 0;
	virtual void CollisionBird() = 0;
	virtual void SceneCreated() = 0;
};

#endif