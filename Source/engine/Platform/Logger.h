#ifndef LoggerH
#define LoggerH

enum LogLevel
{
	kINFO = 0x1,
	kDEBUG = 0x2,
	kERROR = 0x4
};

enum LogPrio
{
	kPRIOINFO = 0x4,
	kPRIODEBUG = 0x3,
	kPRIOERROR = 0x6
};

class Logger
{
public:
	Logger(int level)
	{
		_Level = level;
	}

	virtual int LogInfo(const char *tag, const char *fmt, ...) = 0;
	virtual int LogDebug(const char *tag, const char *fmt, ...) = 0;
	virtual int LogError(const char *tag, const char *fmt, ...) = 0;

protected:
	int _Level;
};

#endif