#ifndef AndroidAudioEngineH
#define AndroidAudioEngineH

#include <assert.h>
#include <string>
#include <map>

// for native audio
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

#include "Sound/AudioEngine.h"

using namespace std;

struct AudioSample
{
	int fd;
	int start;
	int length;
};

struct AndroidAudioPlayer: public AudioPlayer
{
	AndroidAudioPlayer()
	{
		fdPlayerObject = NULL;
		fdPlayerPlay = NULL;
		fdPlayerSeek = NULL;
		fdPlayerMuteSolo = NULL;
		fdPlayerVolume = NULL;
	}

	virtual ~AndroidAudioPlayer();

	SLObjectItf fdPlayerObject;
	SLPlayItf fdPlayerPlay;
	SLSeekItf fdPlayerSeek;
	SLMuteSoloItf fdPlayerMuteSolo;
	SLVolumeItf fdPlayerVolume;

	int setVolume(int millibel);
	int setPlayingAssetAudioPlayer(bool isPlaying);
};

class AndroidAudioEngine: public AudioEngine
{
private:
	AndroidAudioEngine() {
		engineObject = NULL;
		outputMixObject = NULL;
		outputMixEnvironmentalReverb = NULL;
		bqPlayerObject = NULL;
		bqPlayerPlay = NULL;
		bqPlayerBufferQueue = NULL;
		bqPlayerEffectSend = NULL;
		bqPlayerMuteSolo = NULL;
		bqPlayerVolume = NULL;
		engineEngine = NULL;

		// pointer and size of the next player buffer to enqueue, and number of remaining buffers
		nextBuffer = 0;
		nextSize = 0;
		nextCount = 0;
	}

public:
	static AndroidAudioEngine& instance()
	{
		static AndroidAudioEngine INSTANCE;
		return INSTANCE;
	}

public:
	virtual ~AndroidAudioEngine();

	int CreateEngine();
	int createBufferQueueAudioPlayer();
	shared_ptr<AudioPlayer> createAssetAudioPlayer(string filename, bool loop);
	void bqPlayerCB(SLAndroidSimpleBufferQueueItf bq);

private:

	SLObjectItf engineObject;
	SLObjectItf outputMixObject;
	SLEnvironmentalReverbItf outputMixEnvironmentalReverb;
	SLObjectItf bqPlayerObject;
	SLPlayItf bqPlayerPlay;
	SLAndroidSimpleBufferQueueItf bqPlayerBufferQueue;
	SLEffectSendItf bqPlayerEffectSend;
	SLMuteSoloItf bqPlayerMuteSolo;
	SLVolumeItf bqPlayerVolume;
	SLEngineItf engineEngine;

	// pointer and size of the next player buffer to enqueue, and number of remaining buffers
	short *nextBuffer;
	unsigned nextSize;
	int nextCount;

	map<string, AudioSample> mSamples;
};

#endif
