#include "AndroidFileManager.h"
#include "Platform\Platform.h"
#include "Core/ActivityObject.h"
#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include "Core/FileDir.h"
#include "Core/File.h"
#include "AndroidFileDir.h"
#include "AndroidFile.h"

void AndroidFileManager::setAssets(AAssetManager* m)
{
	mAssetManager = m;
}

shared_ptr<FileDir> AndroidFileManager::OpenDir(string name)
{
	EXCEPTION_TRY
		AAssetDir* dir = AAssetManager_openDir(mAssetManager, name.c_str());
		shared_ptr<AndroidFileDir> ret(new AndroidFileDir(dir));
		return ret;
	EXCEPTION_CATCH
		return NULL;
}

shared_ptr<File> AndroidFileManager::OpenFile(string uri, FileOperation op)
{
	EXCEPTION_TRY
		AAsset* asset = AAssetManager_open(mAssetManager, uri.c_str(), AASSET_MODE_BUFFER);
		shared_ptr<AndroidFile> ret(new AndroidFile(uri, op, asset));
		return ret;
	EXCEPTION_CATCH
		return NULL;
}

int AndroidFileManager::OpenFileDescriptor(string uri, int &start, int &length)
{
	EXCEPTION_TRY

    AAsset* asset = AAssetManager_open(mAssetManager, uri.c_str(), AASSET_MODE_UNKNOWN);
    if (NULL != asset) {
		off_t s, l;
		int fd = AAsset_openFileDescriptor(asset, &s, &l);
		if(fd > 0)
		{
    		AAsset_close(asset);
    		start = s;
    		length = l;
			return fd;
		}    
	}
	EXCEPTION_CATCH
		return -1;
}
