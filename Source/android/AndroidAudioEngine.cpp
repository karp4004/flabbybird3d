#include "AndroidAudioEngine.h"
#include "Platform/Platform.h"

// aux effect on the output mix, used by the buffer queue player
static const SLEnvironmentalReverbSettings reverbSettings =
    SL_I3DL2_ENVIRONMENT_PRESET_STONECORRIDOR;

// this callback handler is called every time a buffer finishes playing
void bqPlayerCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
	EXCEPTION_TRY

	AndroidAudioEngine* e = (AndroidAudioEngine*)context;
	if (e != NULL)
	{
		e->bqPlayerCB(bq);
	}

	EXCEPTION_CATCH
}

void AndroidAudioEngine::bqPlayerCB(SLAndroidSimpleBufferQueueItf bq)
{
	EXCEPTION_TRY

	if (bq == bqPlayerBufferQueue)
	{
		if (--nextCount > 0 && NULL != nextBuffer && 0 != nextSize) {
			(*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, nextBuffer, nextSize);
		}
	}

	EXCEPTION_CATCH
}

AndroidAudioEngine::~AndroidAudioEngine()
{
	EXCEPTION_TRY

    // destroy buffer queue audio player object, and invalidate all associated interfaces
    if (bqPlayerObject != NULL) {
        (*bqPlayerObject)->Destroy(bqPlayerObject);
        bqPlayerObject = NULL;
        bqPlayerPlay = NULL;
        bqPlayerBufferQueue = NULL;
        bqPlayerEffectSend = NULL;
        bqPlayerMuteSolo = NULL;
        bqPlayerVolume = NULL;
    }

    // destroy output mix object, and invalidate all associated interfaces
    if (outputMixObject != NULL) {
        (*outputMixObject)->Destroy(outputMixObject);
        outputMixObject = NULL;
        outputMixEnvironmentalReverb = NULL;
    }

    // destroy engine object, and invalidate all associated interfaces
    if (engineObject != NULL) {
        (*engineObject)->Destroy(engineObject);
        engineObject = NULL;
        engineEngine = NULL;
    }

	EXCEPTION_CATCH
}

int AndroidAudioEngine::CreateEngine()
{
	SLresult result = SL_RESULT_SUCCESS;

	EXCEPTION_TRY

	// create engine
	result = slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
	if (SL_RESULT_SUCCESS == result)
	{
		result = (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
		if (SL_RESULT_SUCCESS == result);
		{
			result = (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);
			if (SL_RESULT_SUCCESS == result);
			{
				const SLInterfaceID ids[1] = { SL_IID_ENVIRONMENTALREVERB };
				const SLboolean req[1] = { SL_BOOLEAN_FALSE };
				result = (*engineEngine)->CreateOutputMix(engineEngine, &outputMixObject, 1, ids, req);
				if (SL_RESULT_SUCCESS == result);
				{
					result = (*outputMixObject)->Realize(outputMixObject, SL_BOOLEAN_FALSE);
					if (SL_RESULT_SUCCESS == result);
					{
						result = (*outputMixObject)->GetInterface(outputMixObject, SL_IID_ENVIRONMENTALREVERB,
							&outputMixEnvironmentalReverb);
						if (SL_RESULT_SUCCESS == result) {
							result = (*outputMixEnvironmentalReverb)->SetEnvironmentalReverbProperties(
								outputMixEnvironmentalReverb, &reverbSettings);
							
							AudioSample s;
							s.fd = Platform::instance().FileManager().OpenFileDescriptor("background.mp3", s.start, s.length);
							if (s.fd < 0)
							{
								Platform::instance().Logger().LogError(__FILE__, "cant open:%s", "background.mp3");
							}
							else
							{
								mSamples["background.mp3"] = s;
							}

							s.fd = Platform::instance().FileManager().OpenFileDescriptor("engine.mp3", s.start, s.length);
							if (s.fd < 0)
							{
								Platform::instance().Logger().LogError(__FILE__, "cant open:%s", "engine.mp3");
							}
							else
							{
								mSamples["engine.mp3"] = s;
							}

							s.fd = Platform::instance().FileManager().OpenFileDescriptor("bird.mp3", s.start, s.length);
							if (s.fd < 0)
							{
								Platform::instance().Logger().LogError(__FILE__, "cant open:%s", "bird.mp3");
							}
							else
							{
								mSamples["bird.mp3"] = s;
							}

							s.fd = Platform::instance().FileManager().OpenFileDescriptor("birds001.wav", s.start, s.length);
							if (s.fd < 0)
							{
								Platform::instance().Logger().LogError(__FILE__, "cant open:%s", "engine.mp3");
							}
							else
							{
								mSamples["birds001.wav"] = s;
							}
						}
					}
				}
			}
		}
	}

	EXCEPTION_CATCH

	return result;
}

int AndroidAudioEngine::createBufferQueueAudioPlayer()
{
	   SLresult result;
	EXCEPTION_TRY
	    // configure audio source
	    SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
	    SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, 1, SL_SAMPLINGRATE_8,
	        SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
	        SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN};
	    SLDataSource audioSrc = {&loc_bufq, &format_pcm};

	    // configure audio sink
	    SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMixObject};
	    SLDataSink audioSnk = {&loc_outmix, NULL};

	    // create audio player
	    const SLInterfaceID ids[3] = {SL_IID_BUFFERQUEUE, SL_IID_EFFECTSEND,
	            /*SL_IID_MUTESOLO,*/ SL_IID_VOLUME};
	    const SLboolean req[3] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE,
	            /*SL_BOOLEAN_TRUE,*/ SL_BOOLEAN_TRUE};
	    result = (*engineEngine)->CreateAudioPlayer(engineEngine, &bqPlayerObject, &audioSrc, &audioSnk,
	            3, ids, req);
	    if(SL_RESULT_SUCCESS == result);
		{
			result = (*bqPlayerObject)->Realize(bqPlayerObject, SL_BOOLEAN_FALSE);
			if (SL_RESULT_SUCCESS == result);
			{
				result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_PLAY, &bqPlayerPlay);
				if (SL_RESULT_SUCCESS == result);
				{
					result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_BUFFERQUEUE,
						&bqPlayerBufferQueue);
					if (SL_RESULT_SUCCESS == result);
					{
						result = (*bqPlayerBufferQueue)->RegisterCallback(bqPlayerBufferQueue, bqPlayerCallback, this);
						if (SL_RESULT_SUCCESS == result);
						{
							result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_EFFECTSEND,
								&bqPlayerEffectSend);
							if (SL_RESULT_SUCCESS == result);
							{
								result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_VOLUME, &bqPlayerVolume);
								if (SL_RESULT_SUCCESS == result);
								{
									result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
								}
							}
						}
					}
				}
			}
		}
		

	EXCEPTION_CATCH

	    return result;
}

shared_ptr<AudioPlayer> AndroidAudioEngine::createAssetAudioPlayer(string filename, bool loop)
{
	SLresult result;
	EXCEPTION_TRY

		map<string, AudioSample>::iterator it = mSamples.find(filename);
	if (it == mSamples.end())
	{
		Platform::instance().Logger().LogError(__FILE__, "no sample:%s", filename.c_str());

		return 0;
	}

	shared_ptr<AndroidAudioPlayer> p(new AndroidAudioPlayer);

	// configure audio source
	SLDataLocator_AndroidFD loc_fd = { SL_DATALOCATOR_ANDROIDFD, it->second.fd, it->second.start, it->second.length };
	SLDataFormat_MIME format_mime = { SL_DATAFORMAT_MIME, NULL, SL_CONTAINERTYPE_UNSPECIFIED };
	SLDataSource audioSrc = { &loc_fd, &format_mime };

	// configure audio sink
	SLDataLocator_OutputMix loc_outmix = { SL_DATALOCATOR_OUTPUTMIX, outputMixObject };
	SLDataSink audioSnk = { &loc_outmix, NULL };

	// create audio player
	const SLInterfaceID ids[3] = { SL_IID_SEEK, SL_IID_MUTESOLO, SL_IID_VOLUME };
	const SLboolean req[3] = { SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE };
	result = (*engineEngine)->CreateAudioPlayer(engineEngine, &(p->fdPlayerObject), &audioSrc, &audioSnk,
		3, ids, req);
	if (SL_RESULT_SUCCESS == result);
	{
		result = (*(p->fdPlayerObject))->Realize(p->fdPlayerObject, SL_BOOLEAN_FALSE);
		if (SL_RESULT_SUCCESS == result);
		{
			result = (*(p->fdPlayerObject))->GetInterface(p->fdPlayerObject, SL_IID_PLAY, &(p->fdPlayerPlay));
			if (SL_RESULT_SUCCESS == result);
			{
				result = (*(p->fdPlayerObject))->GetInterface(p->fdPlayerObject, SL_IID_SEEK, &(p->fdPlayerSeek));
				if (SL_RESULT_SUCCESS == result);
				{
					result = (*(p->fdPlayerObject))->GetInterface(p->fdPlayerObject, SL_IID_MUTESOLO, &(p->fdPlayerMuteSolo));
					if (SL_RESULT_SUCCESS == result);
					{
						result = (*(p->fdPlayerObject))->GetInterface(p->fdPlayerObject, SL_IID_VOLUME, &(p->fdPlayerVolume));
						if (SL_RESULT_SUCCESS == result);
						{
							result = (*(p->fdPlayerSeek))->SetLoop(p->fdPlayerSeek, loop, 0, SL_TIME_UNKNOWN);
							if (SL_RESULT_SUCCESS == result);
							{
								return p;
							}
						}
					}
				}
			}
		}
	}


	EXCEPTION_CATCH

    return NULL;
}

int AndroidAudioPlayer::setVolume(int millibel)
{
    SLresult result;
	
	EXCEPTION_TRY
	
	SLVolumeItf volumeItf = fdPlayerVolume;
    if (NULL != volumeItf) {
        result = (*volumeItf)->SetVolumeLevel(volumeItf, millibel);
    }

	EXCEPTION_CATCH

    return result;
}

AndroidAudioPlayer::~AndroidAudioPlayer()
{
	if (fdPlayerObject != NULL) {
		(*fdPlayerObject)->Destroy(fdPlayerObject);
		fdPlayerObject = NULL;
	}
}

int AndroidAudioPlayer::setPlayingAssetAudioPlayer(bool isPlaying)
{
    SLresult result; 
	EXCEPTION_TRY

    // make sure the asset audio player was created
    if (NULL != fdPlayerPlay) {

        // set the player's state
        result = (*fdPlayerPlay)->SetPlayState(fdPlayerPlay, isPlaying ?
            SL_PLAYSTATE_PLAYING : SL_PLAYSTATE_PAUSED);
    }

	EXCEPTION_CATCH

	return result;
}
