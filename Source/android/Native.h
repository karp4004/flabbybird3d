/*
 *  native.h
 *  
 *
 *  Created by Sergey Seitov on 8/24/10.
 *  Copyright 2010 Progorod Ltd. All rights reserved.
 *
 */

#ifndef __NATIVE_H__
#define __NATIVE_H__

#if defined _WIN32 || defined __CYGWIN__
  #ifdef BUILDING_DLL
    #ifdef __GNUC__
      #define DLL_PUBLIC __attribute__((dllexport))
    #else
      #define DLL_PUBLIC __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
    #endif
  #else
    #ifdef __GNUC__
      #define DLL_PUBLIC __attribute__((dllimport))
    #else
      #define DLL_PUBLIC __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
    #endif
  #endif
  #define DLL_LOCAL
#else
  #if __GNUC__ >= 4
    #define DLL_PUBLIC __attribute__ ((visibility("default")))
    #define DLL_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define DLL_PUBLIC
    #define DLL_LOCAL
  #endif
#endif

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif
DLL_PUBLIC JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved);
extern JavaVM* gVM;
#ifdef __cplusplus
}
#endif

#endif /* __NATIVE_H__ */
