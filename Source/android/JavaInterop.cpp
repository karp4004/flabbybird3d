#include <jni.h>

#include <assert.h>

#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include "FlabbyBird3D.h"
#include "Native.h"
#include "Platform\Platform.h"
#include "JavaInterop.h"

#define TAG "GollardEngineNative"

using namespace std;

PFNGLDELETEVERTEXARRAYSOESPROC glDeleteVertexArrays = NULL;
PFNGLGENVERTEXARRAYSOESPROC glGenVertexArrays = NULL;
PFNGLBINDVERTEXARRAYOESPROC glBindVertexArray = NULL;

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT int loadResources(JNIEnv * env, jobject obj,  jobject assetManager)
{
	glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSOESPROC)eglGetProcAddress("glDeleteVertexArraysOES");
	glGenVertexArrays = (PFNGLGENVERTEXARRAYSOESPROC)eglGetProcAddress("glGenVertexArraysOES");
	glBindVertexArray = (PFNGLBINDVERTEXARRAYOESPROC)eglGetProcAddress("glBindVertexArrayOES");

    AAssetManager* mgr = AAssetManager_fromJava(env, assetManager);
	if (mgr != NULL)
	{
		Platform::instance().FileManager().setAssets(mgr);
		Platform::instance().AudioEngine().CreateEngine();
		return Platform::instance().ResourceProvider().LoadResources();
	}

	return -1;
}

JNIEXPORT int createScene(JNIEnv * env, jobject obj)
{
    return FlabbyBird3D::instance().onCreateScene();
}

JNIEXPORT int releaseScene(JNIEnv * env, jobject obj)
{
	return FlabbyBird3D::instance().onReleaseScene();
}

JNIEXPORT int onSurfaceCreated(JNIEnv * env, jobject obj)
{
	return FlabbyBird3D::instance().onSurfaceCreated();
}

JNIEXPORT int onSurfaceChanged(JNIEnv * env, jobject obj,  jint width, jint height)
{
#if TEST_INPUT
	scr.onSurfaceChanged(0, 0, width, height,0);
#endif

	return  FlabbyBird3D::instance().onSurfaceChanged(0, 0, width, height);
}

JNIEXPORT int onDrawFrame(JNIEnv * env, jobject obj)
{
//#if GOLLARDENGINEJNI_DEBUG
//	LOGI( "%s", __FILE__);
//#endif
	Platform::instance().Timer().Update();
	double deltaTime = Platform::instance().Timer().GetDelta();
	return FlabbyBird3D::instance().onDrawFrame(deltaTime);
}

JNIEXPORT void onTouchScreen(JNIEnv * env, jobject obj)
{
	return FlabbyBird3D::instance().onTouchScreen();
}

JNIEXPORT jfloat getScores(JNIEnv * env, jobject obj)
{
	return FlabbyBird3D::instance().getScores();
}

JNIEXPORT void onDestroy(JNIEnv * env, jobject obj)
{
	exit(0);
}

#ifdef __cplusplus
}
#endif
