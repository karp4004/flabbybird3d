#include <jni.h>

#include <assert.h>

#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include "Core/ActivityObject.h"

#include "Native.h"
#include "Platform\Platform.h"
#include "JavaInterop.h"


class AttachThread
{
public:
	AttachThread(JavaVM* javaVM) : javaVM_(javaVM), env_(NULL), attached_(false)
	{
		if (javaVM_->GetEnv((void**)&env_, JNI_VERSION_1_4) == JNI_EDETACHED)
		{
			javaVM_->AttachCurrentThread(&env_, NULL);
			attached_ = true;
		}
	}

	~AttachThread()
	{
		if (attached_)
			javaVM_->DetachCurrentThread();
	}

	JNIEnv* operator->() const
	{
		return env_;
	}
	JNIEnv* env() const
	{
		return env_;
	}

private:

	JavaVM* javaVM_;
	JNIEnv* env_;
	bool attached_;
};

void AndroidInterop::ResourcesLoaded()
{
	AttachThread env(gVM);
	jclass classNative = env->FindClass("gollardengine/Native");
	jmethodID ResourcesLoaded = env->GetStaticMethodID(classNative,
		"ResourcesLoaded", "()V");
	env->CallStaticVoidMethod(classNative, ResourcesLoaded);
}

void AndroidInterop::CollisionBird()
{
	Platform::instance().Logger().LogInfo(__FILE__, "gVM:%p", gVM);

	AttachThread env(gVM);
	jclass classNative = env->FindClass("gollardengine/Native");
	jmethodID CollisionBird = env->GetStaticMethodID(classNative,
		"CollisionBird", "()V");
	env->CallStaticVoidMethod(classNative, CollisionBird);
}

void AndroidInterop::SceneCreated()
{
	AttachThread env(gVM);
	jclass classNative = env->FindClass("gollardengine/Native");
	jmethodID SceneCreated = env->GetStaticMethodID(classNative,
		"SceneCreated", "()V");
	env->CallStaticVoidMethod(classNative, SceneCreated);
}