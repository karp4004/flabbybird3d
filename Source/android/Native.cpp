#include "Native.h"

#include <jni.h>
#include "JavaInterop.h"

#ifndef NULL
#define NULL 0
#endif

#define JAVA_NATIVE_CLASS "gollardengine/Native"

/************************End of Native class methods implementation *****************************/

static JNINativeMethod methods[] =
{
	 {"loadResources","(Landroid/content/res/AssetManager;)I",(void*)loadResources }
	 ,{"createScene","()I",(void*)createScene }
	 ,{"releaseScene","()I",(void*)releaseScene }
	 ,{"onSurfaceCreated","()I",(void*)onSurfaceCreated}
	 ,{"onSurfaceChanged","(II)I",(void*)onSurfaceChanged}
	 ,{"onDrawFrame","()I",(void*)onDrawFrame}
	 ,{"onTouchScreen","()V",(void*)onTouchScreen }
	,{ "getScores","()F",(void*)getScores }
	,{ "onDestroy","()V",(void*)onDestroy }
};

JavaVM* gVM = NULL;

DLL_PUBLIC JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
	JNIEnv *env = NULL;
	if( vm->GetEnv((void **)&env, JNI_VERSION_1_4) != JNI_OK)
	{
		return -1;
	}

	jclass classNative = env->FindClass(JAVA_NATIVE_CLASS);
	if ( classNative == NULL )
	{
		return -1;
	}

	if ( env->RegisterNatives(classNative, methods, sizeof(methods)/sizeof(methods[0])) < 0)
	{
		return -1;
	}

	gVM = vm;

	return JNI_VERSION_1_4;
}
