#ifndef AndroidInteropH
#define AndroidInteropH

#include "Platform/Interop.h"

class AndroidInterop: public Interop
{
private:
	AndroidInterop() {}

public:
	static AndroidInterop& instance()
	{
		static AndroidInterop INSTANCE;
		return INSTANCE;
	}

	virtual void ResourcesLoaded();
	virtual void CollisionBird();
	virtual void SceneCreated();
};

#endif