#include <jni.h>
#include "EGL/egl.h"
#include "Core/OpenGL.h"
#include "Scene/MeshNode.h"
#include "Scene/Camera.h"
#include "Scene/LightNode.h"

#ifdef __cplusplus
extern "C" {
#endif
    JNIEXPORT int loadResources(JNIEnv * env, jobject obj,  jobject assetManager);
	JNIEXPORT int createScene(JNIEnv * env, jobject obj);
	JNIEXPORT int releaseScene(JNIEnv * env, jobject obj);
	JNIEXPORT int onSurfaceCreated(JNIEnv * env, jobject obj);
    JNIEXPORT int onSurfaceChanged(JNIEnv * env, jobject obj,  jint width, jint height);
    JNIEXPORT int onDrawFrame(JNIEnv * env, jobject obj);
	JNIEXPORT void onTouchScreen(JNIEnv * env, jobject obj);
	JNIEXPORT jfloat getScores(JNIEnv * env, jobject obj);
	JNIEXPORT void onDestroy(JNIEnv * env, jobject obj);

#ifdef __cplusplus
}
#endif

