#include <jni.h>

#include <assert.h>

#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include "AndroidFileManager.h"
#include "PosixTimer.h"
#include "AndroidCursor.h"
#include "AndroidAudioEngine.h"

#include "app/CustomApplication.h"
#include "Test.h"
#include "test/ModulesTest.h"
#include "test/NodesTest.h"
#include "Render/GL/GLRenderer.h"
#include "Render/GL/GLRenderFactory.h"
#include "Resource/ResourceProvider.h"
#include "Native.h"
#include "GollardEngineNative.h"

#define TAG "GollardEngineNative"

#define INFO 0
#define DEBUG 0
#define TRACE 0
#include "Core/Logger.h"

using namespace std;

#define DUMMY_SCENE 0
#define NODES_TEST 0
#define TEST_INPUT 0
#define SNIPER 1
#define EDITOR 0
#define CUSTOM 0

class AttachThread
{
public:
	AttachThread(JavaVM* javaVM) : javaVM_(javaVM), env_(NULL), attached_(false)
	{
		if (javaVM_->GetEnv((void**)&env_, JNI_VERSION_1_4) == JNI_EDETACHED)
		{
			javaVM_->AttachCurrentThread(&env_, NULL);
			attached_ = true;
		}
	}

	~AttachThread()
	{
		if (attached_)
			javaVM_->DetachCurrentThread();
	}

	JNIEnv* operator->() const
	{
		return env_;
	}
	JNIEnv* env() const
	{
		return env_;
	}

private:

	JavaVM* javaVM_;
	JNIEnv* env_;
	bool attached_;
};

GLRenderFactory GLES2Factory("GLES2RenderFactory");
GLRenderer Render("GLES2Renderer");
PosixTimer timer("PosixTimer");
AndroidCursor cursor;
AndroidAudioEngine* aengine = NULL;

PFNGLDELETEVERTEXARRAYSOESPROC glDeleteVertexArrays;
PFNGLGENVERTEXARRAYSOESPROC glGenVertexArrays;
PFNGLBINDVERTEXARRAYOESPROC glBindVertexArray;

int LoadProcs()
{
	glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSOESPROC)eglGetProcAddress("glDeleteVertexArraysOES");
	glGenVertexArrays = (PFNGLGENVERTEXARRAYSOESPROC)eglGetProcAddress("glGenVertexArraysOES");
	glBindVertexArray = (PFNGLBINDVERTEXARRAYOESPROC)eglGetProcAddress("glBindVertexArrayOES");

	return 0;
}

Cursor* getCursor()
{
	return &cursor;
}

RenderFactory* getRF()
{
	return &GLES2Factory;
}

Renderer* getRenderer()
{
	return &Render;
}

#if GOLLARDENGINEJNI_TEST
Test app;
#elif DUMMY_SCENE
#define app 0
#else
ApplicationFrame* app;
#endif

#if TEST_INPUT
Screen scr("src", 0);
#endif

ResourceProvider* getRP()
{
	return app->getRP();
}

shared_ptr<TextList> getDebugger()
{
	return app->getDebugger();
}

AudioEngine* getAE()
{
	return app->getAE();
}

ApplicationFrame* getAF()
{
	return app;
}

#ifdef __cplusplus
extern "C" {
#endif

void LibraryResourcesLoaded()
{
	LOGI("%s:%p", TAG " LibraryResourcesLoaded", gVM);

	AttachThread env(gVM);

	jclass classNative = env->FindClass("gollardengine/Native");

	LOGI("%s:%p", TAG "classNative", classNative);

	jmethodID resourcesLoaded = env->GetStaticMethodID(classNative,
			"resourcesLoaded", "()V");

	LOGI("%s:%p", TAG "resourcesLoaded", resourcesLoaded);

	env->CallStaticVoidMethod(classNative, resourcesLoaded);
}

JNIEXPORT int onLoadResources(JNIEnv * env, jobject obj,  jobject assetManager)
{
#if TEST_MODULES
    app = new ModulesTest("ModulesTest", &GLES2Factory);
#elif NODES_TEST
    app = new NodesTest ("NodesTest", &GLES2Factory, &Render, &timer);
#elif SNIPER
	if (aengine == NULL)
	{
		aengine = new AndroidAudioEngine();
	}

    app = new ApplicationFrame ("Sniper", &GLES2Factory, &Render, &timer, aengine);
#elif CUSTOM
    app = new CustomApplication("CustomApplication", &GLES2Factory, &Render, &timer, &aengine);
#endif

	LOGI(TAG "%s", "onLoadResources");

    AAssetManager* mgr = AAssetManager_fromJava(env, assetManager);
    assert(NULL != mgr);

	LOGI(TAG "%p", "mgr");

    AndroidFileManager m("AndroidFileManager", mgr);

	int ret = app->onLoadResources(&m);

	LibraryResourcesLoaded();

	return ret;
}

JNIEXPORT int onCreate(JNIEnv * env, jobject obj,  jobject assetManager)
{
	LOGT("", "");

    AAssetManager* mgr = AAssetManager_fromJava(env, assetManager);
    assert(NULL != mgr);

    AndroidFileManager m("AndroidFileManager", mgr);
//    app = new CustomApplication("CustomApplication", &GLES2Factory, &Render, &timer);

    return app->onCreate(&m);
}

JNIEXPORT int onPause(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int onResume(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int onRestart(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int onStart(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int onStop(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int onDestroy(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	//app->onDestroy();

	//delete app;

	//exit(0);

	return 0;
}

JNIEXPORT int onBackPressed(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	return app->onDestroy();
}

JNIEXPORT int onSurfaceCreated(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	return app->onSurfaceCreated();
}

JNIEXPORT int onSurfaceChanged(JNIEnv * env, jobject obj,  jint width, jint height)
{
	LOGT("", "");

#if TEST_INPUT
	scr.onSurfaceChanged(0, 0, width, height,0);
#endif

	return  app->onSurfaceChanged(0, 0, width, height);
}

JNIEXPORT int onDrawFrame(JNIEnv * env, jobject obj)
{
//#if GOLLARDENGINEJNI_DEBUG
//	LOGI( "%s", __FILE__);
//#endif

	return app->onDrawFrame();
}

JNIEXPORT int onTouchMove(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jid)
{
	LOGT("", "");

	cursor.id = jid;
	cursor.x = x;
	cursor.y = y;
	cursor.state = kScreenDown;

	return app->onTouchMove(&cursor);
}

JNIEXPORT int onTouchUp(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jid)
{
	LOGT("", "");

	cursor.id = jid;
	cursor.x = x;
	cursor.y = y;
	cursor.mMoveHistory.erase(cursor.id);

	return app->onTouchUp(&cursor);
}

JNIEXPORT int onTouchDown(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jid)
{
	LOGT("", "");

	cursor.id = jid;
	cursor.x = x;
	cursor.y = y;
	cursor.mMoveHistory[cursor.id] = GV(x,y);

	return app->onTouchDown(&cursor);
}

DLLEXPORT int STDCALL getFPS()
{
	return Render.mFPS;
}

DLLEXPORT int STDCALL getFaces()
{
	return Render.mTrisCount;
}

DLLEXPORT int STDCALL getFaces2()
{
	return Render.mTrisCount;
}

VehiclePlayer* createCar(Scene* s, CarDescriptor d)
{
	return app->createCar(s, d);
}

RigidNode* addRigidMesh(Scene* s, RigidNodeDescriptor d)
{
	return app->addRigidMesh(s, d);
}

MeshNode* addMesh(Scene* s, MeshDescriptor d)
{
	return app->addMesh(s, d);
}

Camera* addCamera(Scene* s, CameraDescriptor d)
{
	return app->addCamera(s, d);
}

LightNode* addLight(Scene* s, MoveableDescriptor d)
{
	return app->addLight(s, d);
}

PhysicScene* addPhysicScene(MoveableDescriptor d)
{
	return app->addPhysicScene(d);
}

int load_library()
{
	return 0;
}

DLLEXPORT MeshNode* STDCALL selectMesh(float scx, float scy)
{
	return app->selectMesh(scx, scy);
}

DLLEXPORT void STDCALL setSelectable(MeshNode* node, const char* name)
{
	app->setSelectable(node, name);
}

DLLEXPORT int STDCALL onDestroyXamarin()
{
	LOGT("", "");

	app->onDestroy();

	//delete app;

	//exit(0);

	return 0;
}

typedef void (callback)(wchar_t*  str);
void caller(wchar_t * input, int count, callback call)
{
	call(input);
}

#ifdef __cplusplus
}
#endif
