#ifndef PosixTimerH
#define PosixTimerH

#include <time.h>
#include "Core/Timer.h"
#include <string>

using namespace std;

class PosixTimer: public Timer
{
private:
	PosixTimer();

public:
	static PosixTimer& instance()
	{
		static PosixTimer INSTANCE;
		return INSTANCE;
	}

	virtual int Update();
	virtual int Reset();

private:
	timespec old_t;
};

#endif
