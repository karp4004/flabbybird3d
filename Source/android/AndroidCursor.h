#ifndef AndroidCursor_H
#define AndroidCursor_H

#include "Input/Cursor.h"

class AndroidCursor: public Cursor
{
public:
	AndroidCursor();
	virtual Vector2 Update(bool grab);
	virtual int onUp();
	virtual int onDown();
	virtual int onReshape(int x, int y);
	virtual int Grab();
	virtual int setHWND(void *hwnd){ return 0;};
	int Log(string, string);
};


#endif
