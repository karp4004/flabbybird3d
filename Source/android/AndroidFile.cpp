#include "AndroidFile.h"
#include "Platform\Platform.h"
#include "Core/ActivityObject.h"
#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

AndroidFile::AndroidFile(string name, FileOperation op, AAsset* a):
File(op)
,file(a)
{
}

AndroidFile::~AndroidFile()
{
	EXCEPTION_TRY

		AAsset_close(file);

	EXCEPTION_CATCH
}

int AndroidFile::Length()
{
	EXCEPTION_TRY

		return AAsset_getLength(file);

	EXCEPTION_CATCH

		return 0;
}

int AndroidFile::Read(char* buf, int len)
{
	EXCEPTION_TRY
	
		return AAsset_read(file, buf, len);
	
	EXCEPTION_CATCH
		
		return 0;
}

int AndroidFile::Write(char* buf, int len)
{
	return 0;
}
