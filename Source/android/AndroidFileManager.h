#ifndef ANDROID_FILE_MANAGER_H
#define ANDROID_FILE_MANAGER_H

#include "Core/SharedPtr.h"
#include "Core/FileManager.h"

#include <string>

using namespace std;

class AAssetManager;
class FileDir;
class File;

class AndroidFileManager: public FileManager
{
private:
	AndroidFileManager() { mAssetManager = NULL; }

public:
	static AndroidFileManager& instance()
	{
		static AndroidFileManager INSTANCE;
		return INSTANCE;
	}

	void setAssets(AAssetManager* m);
	virtual shared_ptr<FileDir> OpenDir(string name);
	virtual shared_ptr<File> OpenFile(string uri, FileOperation op);
	virtual int OpenFileDescriptor(string uri, int &start, int &length);

	int Log(string module, string name){};

private:
	AAssetManager* mAssetManager;
};

#endif
