#include "AndroidLogger.h"

#include <android/log.h>
#include <stdio.h>
#include <stdarg.h>

int AndroidLogger::LogInfo(const char *tag, const char *fmt, ...)
{
	if (_Level | kINFO)
	{
		const int logSize = 1024;
		va_list ap;
		char buf[logSize];

		va_start(ap, fmt);
		vsnprintf(buf, logSize, fmt, ap);
		va_end(ap);

		return __android_log_write(kPRIOINFO, tag, buf);
	}

	return -1;
}

int AndroidLogger::LogDebug(const char *tag, const char *fmt, ...)
{
	if (_Level | kDEBUG)
	{
		const int logSize = 1024;
		va_list ap;
		char buf[logSize];

		va_start(ap, fmt);
		vsnprintf(buf, logSize, fmt, ap);
		va_end(ap);

		return __android_log_write(kPRIODEBUG, tag, buf);
	}

	return -1;
}

int AndroidLogger::LogError(const char *tag, const char *fmt, ...)
{
	if (_Level | kERROR)
	{
		const int logSize = 1024;
		va_list ap;
		char buf[logSize];

		va_start(ap, fmt);
		vsnprintf(buf, logSize, fmt, ap);
		va_end(ap);

		return __android_log_write(kPRIOERROR, tag, buf);
	}

	return -1;
}
