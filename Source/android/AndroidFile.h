#ifndef ANDROID_FILE_H
#define ANDROID_FILE_H

#include "Core/File.h"

#include <string>

using namespace std;

class AAsset;

class AndroidFile: public File
{
public:
	AndroidFile(string name, FileOperation op, AAsset* a);
	virtual ~AndroidFile();

	virtual int Length();
	virtual int Read(char* buf, int len);
	virtual int Write(char* buf, int len);

	int Log(string module, string name){};

private:
	AAsset* file;
};

#endif
