#include "PosixTimer.h"
#include "stdlib.h"
#include "errno.h"

#include "Platform/Platform.h"

#define NANO_SEC 999999999
#define POSIXTIMER_DEBUG 0

PosixTimer::PosixTimer()
{
	timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	old_t = t;
}

int PosixTimer::Reset()
{
	Timer::Reset();
	timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	old_t = t;

	 return 0;
}

int PosixTimer::Update()
{
	timespec t;
	memset(&t, 0, sizeof(timespec));

	int ret = clock_gettime(CLOCK_MONOTONIC, &t);
	long delta_t = t.tv_nsec;

	if(delta_t < old_t.tv_nsec)
	{
		delta_t += (NANO_SEC - old_t.tv_nsec);
	}
	else
	{
		delta_t -= old_t.tv_nsec;
	}

	old_t = t;

	double fdelta = delta_t;
	fdelta /= 1000000000.;
	SetDelta(fdelta);
	CountElapsed(fdelta);

	Platform::instance().Logger().LogInfo(__FILE__, "t:%ld:%ld", t.tv_nsec, delta_t);
	return 0;
}
