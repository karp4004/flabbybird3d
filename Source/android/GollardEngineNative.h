#include <jni.h>
#include "Input/VehiclePlayer.h"
#include "EGL/egl.h"
#include "Core/OpenGL.h"
#include "Input/VehiclePlayer.h"
#include "Physics/RigidNode.h"
#include "Scene/MeshNode.h"
#include "Scene/Camera.h"
#include "Scene/LightNode.h"
#include "Physics/PhysicScene.h"

#ifdef __cplusplus
extern "C" {
#endif
    JNIEXPORT int onLoadResources(JNIEnv * env, jobject obj,  jobject assetManager);
	JNIEXPORT int onCreate(JNIEnv * env, jobject obj,  jobject assetManager);
	JNIEXPORT int onPause(JNIEnv * env, jobject obj);
	JNIEXPORT int onResume(JNIEnv * env, jobject obj);
	JNIEXPORT int onRestart(JNIEnv * env, jobject obj);
	JNIEXPORT int onStart(JNIEnv * env, jobject obj);
	JNIEXPORT int onStop(JNIEnv * env, jobject obj);
	JNIEXPORT int onDestroy(JNIEnv * env, jobject obj);
	JNIEXPORT int onBackPressed(JNIEnv * env, jobject obj);
	JNIEXPORT int onSurfaceCreated(JNIEnv * env, jobject obj);
    JNIEXPORT int onSurfaceChanged(JNIEnv * env, jobject obj,  jint width, jint height);
    JNIEXPORT int onDrawFrame(JNIEnv * env, jobject obj);
    JNIEXPORT int onTouchMove(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jid);
    JNIEXPORT int onTouchUp(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jidx);
    JNIEXPORT int onTouchDown(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jidx);

    int load_library();

	DLLEXPORT int STDCALL getFPS();
	DLLEXPORT int STDCALL getFaces();
	DLLEXPORT int STDCALL getFaces2();
	DLLEXPORT int STDCALL onDestroyXamarin();
	DLLEXPORT VehiclePlayer* STDCALL createCar(Scene* s, CarDescriptor d);
	DLLEXPORT RigidNode* STDCALL addRigidMesh(Scene* s, RigidNodeDescriptor d);
	DLLEXPORT MeshNode* STDCALL addMesh(Scene* s, MeshDescriptor d);
	DLLEXPORT Camera* STDCALL addCamera(Scene* s, CameraDescriptor d);
	DLLEXPORT LightNode* STDCALL addLight(Scene* s, MoveableDescriptor d);
	DLLEXPORT PhysicScene* STDCALL addPhysicScene(MoveableDescriptor d);
	DLLEXPORT MeshNode* STDCALL selectMesh(float scx, float scy);
	DLLEXPORT void STDCALL setSelectable(MeshNode* node, const char* name);

	extern PFNGLDELETEVERTEXARRAYSOESPROC glDeleteVertexArrays;
	extern PFNGLGENVERTEXARRAYSOESPROC glGenVertexArrays;
	extern PFNGLBINDVERTEXARRAYOESPROC glBindVertexArray;

#ifdef __cplusplus
}
#endif

