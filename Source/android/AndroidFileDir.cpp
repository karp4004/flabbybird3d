#include "AndroidFileDir.h"
#include "Platform\Platform.h"
#include "Core/ActivityObject.h"
#include <sys/types.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

AndroidFileDir::AndroidFileDir(AAssetDir* d):
FileDir()
,dir(d)
{
}

const char* AndroidFileDir::GetNextFileName()
{
	EXCEPTION_TRY
		return AAssetDir_getNextFileName(dir);
	EXCEPTION_CATCH
		return NULL;
}
