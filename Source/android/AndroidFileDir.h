#ifndef ANDROID_FILE_DIR
#define ANDROID_FILE_DIR

#include "Core/FileDir.h"

#include <string>

using namespace std;

class AAssetDir;

class AndroidFileDir: public FileDir
{
public:
	AndroidFileDir(AAssetDir* d);
	virtual const char* GetNextFileName();

	int Log(string module, string name){};

private:
	AAssetDir* dir;
};

#endif
