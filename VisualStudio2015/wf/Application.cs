﻿using System.Runtime.InteropServices;

namespace GEScript
{
	public class Application
    {
        public delegate void Callback(string str);

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int LoadLibrary(int a, int b);

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern void Caller(string str, int count, Callback call);

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern int LoadResources(string path);

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int CreateScene();

        [DllImport("libgelibrary.so")]
        public static extern int ReleaseScene();

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int OnSurfaceCreated();

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int OnSurfaceChanged(int w, int h);

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int OnDrawFrame();

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int CreateContext(int w);

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int OnDrawContext(int w);

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int ReleaseContext(int w);

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int GetFPS();

        [DllImport("libgelibrary.so", CallingConvention = CallingConvention.StdCall)]
        public static extern int GetFaces();

        [DllImport("libgelibrary.so")]
        public static extern void OnTouchScreen();
    }
}
