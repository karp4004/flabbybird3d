﻿using System;
using System.Windows.Forms;
using GEScript;

namespace WinForms
{
    public partial class FlabbyBird3D : Form
    {
        public FlabbyBird3D()
        {
            InitializeComponent();

            Paint += OnPaintEvent;
            Resize += OnResize;
            HandleDestroyed += OnFormClosed;
            MouseMove += MouseEventHandler;
            MouseDown += MouseEventDown;
            MouseWheel += MouseEventWheel;

            GEScript.Application.CreateContext(Handle.ToInt32());
            GEScript.Application.LoadResources("../../../VisualStudio2015/android/assets/");
            GEScript.Application.CreateScene();
        }

        public void MouseEventDown(object sender, MouseEventArgs e)
        {
            GEScript.Application.OnTouchScreen();
        }

        public void MouseEventWheel(object sender, MouseEventArgs e)
        {
        }

		public void MouseEventHandler(object sender, MouseEventArgs e)
		{
		}

        public void OnTimer(object sender, EventArgs e)
        {
            label1.Text = GEScript.Application.GetFPS().ToString();
            label2.Text = GEScript.Application.GetFaces().ToString();

            int faces = GEScript.Application.GetFaces();
            Console.Write("fps:" + GEScript.Application.GetFPS().ToString());
            Console.Write("faces:" + faces.ToString() + "/" + (faces / 3).ToString());
        }

		public void Form1_Load(object s, EventArgs ev)
        {
            GEScript.Application.OnSurfaceCreated();

            Width += 1;
        }

        public void OnResize(object sender, EventArgs e)
        {
            GEScript.Application.OnSurfaceChanged(Width, Height);

            label1.Text = GEScript.Application.GetFPS().ToString();

            int faces = GEScript.Application.GetFaces();
	        label2.Text = faces + "/" + (faces / 3);
        }

        public void OnPaintEvent(object sender, PaintEventArgs e)
        {
            Refresh();
        }

        public void OnFormClosed(object sender, EventArgs e)
        {
            GEScript.Application.ReleaseContext(Handle.ToInt32());
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x000F)
            {
                GEScript.Application.OnDrawContext(Handle.ToInt32());
                
                if (_sec != DateTime.Now.Second)
                {
                    int faces = GEScript.Application.GetFaces();
                    Console.WriteLine("fps:" + GEScript.Application.GetFPS().ToString());
                    Console.WriteLine("faces:" + faces.ToString() + "/" + (faces / 3).ToString());

                    _sec = DateTime.Now.Second;
                }
            }
            else if (m.Msg == 0x14)
	        {
		        m.Result = (IntPtr)1;
	        }
	        else
	        {
		        base.WndProc(ref m);
	        }
        }

		private int _sec = 0;
	}
} 
