﻿using System.Reflection;

[assembly: AssemblyTitle("GEngine Module")]
[assembly: AssemblyCompany("Kuzovov & Karpov Company.")]
[assembly: AssemblyProduct("GEngine")]
[assembly: AssemblyCopyright("© 2016-2017 Kuzovov & Karpov Company. All Rights Reserved.")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]